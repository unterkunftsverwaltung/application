/**
 * Login Service
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @extends {socket}
 */

var uvUser, uvRights, uvVersion;

/**
 * SOCKET ON: loginscreen
 *
 * @description Einblenden der Loginmaske
 * @author Rafal Welk <rafal@project.hatua.de>
 */
socket.on('loginscreen', function(err) {
	$('#login').show();
	$('html').addClass('login');
	$('.content-inner input[type="text"], .content-inner input[type="number"], .content-inner textarea').val('');
	$('#loginscreen').show().addClass("fadeIn");
	$('#login input[value=""]:first').focus();

	setTimeout(function() {
    	$('#loginscreen').removeClass('fadeIn');
    	document.title = 'Unterkunftsverwaltung';
    	window.history.replaceState(null, '', "#/");
   	}, 800);
});

/**
 * SOCKET ON: appscreen
 * 
 *
 * @description Benutzeroberfläche anzeigen und Hintergrundbild ausblenden
 *              Wenn der Benutzer angemeldet ist und die Webseite neu lädt oder 
 *              in einem weiteren Tab aufruft
 * @author Rafal Welk <rafal@project.hatua.de>
 */
socket.on('appscreen', function(err) {
	$('#wrapper').show();
	$('#login').hide();
	$('html').removeClass('login');
});

/**
 * SOCKET ON: login-ok
 * 
 *
 * @description Benutzername und Passwort sind korrekt, Benutzeroberfläche zur Verfügung stellen 
 *              und Hintergrundbild ausblenden. Sowíe die Benutzerdaten zwischenspeichern.
 * @author Rafal Welk <rafal@project.hatua.de>
 */
socket.on('login-ok', function(err) {
	notifyClose();
	$('#wrapper').show();
	$('#login').addClass('fadeOutUp');
	$('#login-username').val('');
	$('#login-password').val('');
	// Index + Dokumententitel anpassen
	document.title = 'Unterkunftsverwaltung';
	routeProvider.navigateTo('indexView');

	setTimeout(function() {
    	$('#login').hide().removeClass('fadeOutUp');
    	$('#loginscreen').hide().removeClass('fadeIn');
    	$('html').removeClass('login');
   	}, 800);
});

/**
 * jQuery Selector: #login-submit
 *
 * @description Überprüfen ob Benutzername und Passwort ausgefüllt sind
 *              Senden der Zugangsdaten zu überprüfen an den Server
 * @author Rafal Welk <rafal@project.hatua.de>
 */
$('#login').on('click', '#login-submit', function(event) {
	event.preventDefault();

	var userData = {
		username: 	$('#login-username').val().trim(),
		password: 	$('#login-password').val()
	}

	$('#loginscreen').removeClass('fadeIn');

	if ( (!userData.username) || (!userData.password) ) {
		$('#loginscreen').addClass('shake').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
			$(this).removeClass('shake');
		});
		notifyWarning('Bitte Benutzernamen und Passwort eintragen', '');
		if (!userData.username) {
			$('#login-username').focus();
		} else {
			$('#login-password').focus();
		}
		
		return;
	}

	socket.emit('login', userData, function(result) {
		if (result === false) {
			// Benutzername und / oder Passwort falsch
			$('#loginscreen').addClass('shake').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
				$(this).removeClass('shake');
			});
			notifyWarning('Benutzernamen und/oder Passwort sind fehlerhaft', '');
			$('#login-password').val('').focus();
		} 
	});
});

/**
 * jQuery Selector: #logout
 *
 * @description Abmeldung des Benutzers und sperren der Benutzeroberfläche
 * @author Rafal Welk <rafal@project.hatua.de>
 */
$('#logout').click(function(event) {
	event.preventDefault();
	socket.emit('logout');
});

/**
 * SOCKET ON: userpermissions
 * 
 * @description Benutzerinformationen und Berechtigungen
 * @author Rafal Welk <rafal@project.hatua.de>
 */
socket.on('userpermissions', function(user, right, version) {
	uvUser = user;
	uvRights = right;
	uvVersion = version;
	
	$('#logout').attr('data-original-title', 'Angemeldeter Benutzer').attr('data-content', '<span style="display: inline-block;"><p><strong>' + (uvUser.username).toUpperCase() + '</strong></p><p>' + uvUser.forename + ' ' + uvUser.surname + '<br><small>' + uvUser.rank + '</small></span></p>').popover({html: true});
	$('#group').attr('data-content', '<p><small>Hörsaal 09 - Projektteam</small><br><em>EIGHT BITS FOR PROJECT</em></p><small>Nadine <strong>Eickelbaum</strong><br>Alexander <strong>Faust</strong><br>Dennis <strong>Freitag</strong><br>Carsten <strong>Müller</strong><br>Jessica <strong>Ottilie</strong><br>Elisabeth <strong>Schöneberg</strong><br>Patrick <strong>Sturies</strong><br>Rafal <strong>Welk</strong></small><hr><span class="version">Version ' + uvVersion.number + ' - <em>' + uvVersion.name + '</em></span>').popover({html: true});
});


/**
 * SOCKET ON: logout
 * 
 * @description Anweisung die Session zu beenden und zu sperren
 * @author Rafal Welk <rafal@project.hatua.de>
 */
socket.on('logout', function() {
	notifyClose();
	$('.content-inner input[type="text"], .content-inner input[type="number"], .content-inner textarea').val('');
	$('#login').show().addClass('fadeInDown');
	$('html').addClass('login');
	$('#logout').popover('destroy');
	$('#group').popover('destroy');
	document.title = 'Unterkunftsverwaltung';
	routeProvider.navigateTo('indexView');

	uvUser = null;
	uvRights = null;

	setTimeout(function() {
    	$('#loginscreen').show().addClass("fadeIn");
    	$('#login-username').focus();
    	$('#wrapper').hide();
   	}, 500);

   	window.history.replaceState(null, '', "#/");
});

/**
 * SOCKET ON: reloadpermissions
 * 
 * @description Berechtigungen aktualisieren und das Sidemenü neu aufbauen
 * @author Rafal Welk <rafal@project.hatua.de>
 */
socket.on('reloadpermissions', function() {
	socket.emit('getpermissions', function(callback) {});	
});