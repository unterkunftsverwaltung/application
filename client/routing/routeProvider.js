/**
 * RouteProvider
 * @author Rafal Welk <rafal@project.hatua.de>
 * 
 * @description Aufbau des gewählten Contents über die Adresse oder aufgrund von Funktionsanweisung
 */

/**
 * global variables
 */
var actURL,
	routing,
	backview = null,
	lastview = null;

/**
 * Ermittlen der aktuellen Adresse
 * Aufbau des Arrays
 * [0]: Adresse mit Port
 * [1]: Raute
 * [2]: Selected View
 * [3]: Subfunction
 * [4]: Transfered ID
 * [5]: Für den Adressabgleich
 */
function getURL() {
 	actURL = window.location.href;
	actURL = actURL.substring(7).split("/");

	actURL[5] = (actURL[1] ? actURL[1] + "/" : '') + (actURL[2] ? actURL[2] : '') + (actURL[3] ? "/" + actURL[3] : '');
}

/**
 * Routing vom Server laden und gem. aktueller Adresse verfahren
 */
socket.emit('routing', function(data) {
	getURL();
	routing = data;

	for (var view in routing) {
		if ("#/" + routing[view].url === actURL[5]) {
			routeProvider.navigateTo(view, (actURL[4] === undefined ? '' : actURL[4]) );
			return;
		}
	}

	// If nothing was found - show Startpage
	routeProvider.navigateTo('indexView');
});

/**
 * RouteProvider Wechsel des Contents und überprüfen der Berechtigungen
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 */
function RouteProvider() {}
RouteProvider.prototype.navigateTo = function(view, transfer) {
	transfer = transfer || '';

	if ( (view !== '') && (routing[view] !== undefined) && (uvRights !== undefined) ) {
		
        var splitpermissions = (routing[view].permissions).split("|"),
        	found = 0;
                
        // Überprüfen ob min. 1x Berechtigung zutrifft                                        
        for (var i = 0; i < splitpermissions.length; i++) {

            if (uvRights[splitpermissions[i]]) { found++; }
            if (uvRights[transfer + '-' + splitpermissions[i]]) { found++; }
        }

        if (found > 0) {
        	// Speichern der letzten View
        	if (lastview !== view) {
        		backview = lastview;
        		lastview = view;
        	}
        	// Webadresse anpassen
			window.history.replaceState(null, '', '#/' + routing[view].url + (transfer !== '' ? '/' + transfer : '') );
			// Content laden
			$('#content').load('client/view/' + routing[view].route, function() {
				// Beschriftung des Zurück-Buttons
				$('#btn-back')
				.attr('data-toggle', 'popover')
				.attr('data-trigger', 'hover')
				.attr('data-placement', 'left')
				.attr('title', 'Zurück zur Übersicht: ')
				.attr('data-content', '<small>' + routing[view].header + '</small>')
				.popover({html: true});
				
				if (backview !== null) $('#btn-back').attr('data-content', '<small>' + routing[backview].header + '</small>');

				// Suchfunktion de- /aktivieren
				if (routing[view].search) {
					$('#searchBox').removeClass('hidden');
					$('#header-search-field').val('');
					$('#searchClear').addClass('notvisible');
					$('.searchIcon .glyphicon').removeClass('active');
					$('#noSearchResult').hide();
				} else {
					$('#searchBox').addClass('hidden');
				}

				// Autolabel
				autolabel();

				// verbleibende Zeichen
				remainingCharacters();

				// Fokus: Erstes Eingabefeld
				setTimeout(function() {
					$('#content input:first').focus();
				}, 100);
			});
			// Überschrift anpassen
			$('#contentHeader').text(routing[view].header);
			// Dokumententitel anpassen
			document.title = (routing[view].name !== 'Unterkunftsverwaltung' ? routing[view].name + ' | ' + 'Unterkunftsverwaltung' : routing[view].name);
			// Webadresse aktualisieren
			changeActiveClass();
		} else {
			startPage();
			// Fehlermeldung ausgeben
			notifyError('Sie besitzten nicht die benötigten Rechte um diese Funktion zu nutzen');
		}
	} else {
		startPage();
	}
};

/**
 * RouteProvider Springen zur letzten Seite
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 */
RouteProvider.prototype.navigateBack = function(view) {
	if (backview !== null) {
		routeProvider.navigateTo(backview);
	} else {
		routeProvider.navigateTo(view);
	}
};


var routeProvider = new RouteProvider();

/**
 * startPage Anzeigen der Startseite
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 */
function startPage() {
	// Startseite Content laden
	$('#content').html('').load('client/view/' + routing['indexView'].route);
	// Überschrift anpassen
	$('#contentHeader').text(routing['indexView'].header);
	// Dokumententitel anpassen
	document.title = 'Unterkunftsverwaltung';
	// Webadresse anpassen
	window.history.replaceState(null, '', '#/');
	// Webadresse aktualisieren
	changeActiveClass();
}

/**
 * Aufbau der Navigation
 */
socket.on('loadSidebar', function(data) {
	accesskey = 0;
	$('.sidebar-nav').html('');

	for (var element in data) {
		accesskey += 1;
		var entry = '<li class="link">';

		if (data[element].collapse) {
			// Verschachteltes Menü
			
			entry += 
			  '<a href="#' + data[element].url + '" data-toggle="collapse" aria-controls="' + data[element].url + '" accesskey="' + accesskey + '">'
			+ '<span class="fa fa-' + data[element].icon + '" aria-hidden="true"></span>'
            + '<span>' + data[element].name + '</span>'
            + '</a>'
            + '<ul class="collapse collapseable in" id="settings">';

            for(var sub in data[element].collapse) {
            	accesskey += 1;
            	entry += '<li><a id="' + sub + '" href="#/' + data[element].collapse[sub].url + '" accesskey="' + accesskey + '">' + data[element].collapse[sub].name + '</a>';
            }

     		entry += '</ul>';
			
		} else {
			// Normaler Eintrag
			
			entry +=
				'<a href="#/' + data[element].url + '" id="' + element + '" accesskey="' + accesskey + '">'
				+ '<span class="fa fa-' + data[element].icon + '" aria-hidden="true"></span>'
				+ '<span>' + data[element].name + '</span>'
				+ '</a>';
			
		}
 
		entry += '</li>';

		$('.sidebar-nav').append(entry);
		changeActiveClass();
	}
});

/**
 * Sidebar Navigation
 */

$('.sidebar-nav').on('click', 'a', function(e) {
	if ($(this).attr('href').substring(1, 2) === '/') {
		routeProvider.navigateTo( $(this).attr('id') );
	}
});

$('#startseite').click(function(e) {
	routeProvider.navigateTo('indexView');
});

function changeActiveClass() {
	getURL();
	$('.sidebar-nav a').removeClass("active");
	$('.sidebar-nav a[href="#/' + actURL[2] + '"]').addClass("active");

	if ( $('.sidebar-nav a[href="#/' + actURL[2] + '"]').parent().parent('ul').hasClass('collapse') ) {
		$('.sidebar-nav a[aria-controls="' + $('.sidebar-nav a[href="#/' + actURL[2] + '"]').parent().parent('ul').attr('id') + '"]').addClass('active');
	}
}