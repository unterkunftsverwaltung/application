/**
 * Remaining Characters 
 * Verbleibende Zeichen
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @description Ermittlen und einblenden der verbleibenden Zeichen
 */

function remainingCharacters() {
	var maxlength = 0;
	var characters = 0;

	// Verbleibende Zeichen einblenden
	$(".content-inner").on('focusin', 'input[type="text"], input[type="number"], textarea', function() {
		maxlength = $(this).attr('maxlength');
		characters = $(this).val().length;

		$(this).parent().prepend('<span class="characters"><span class="hidden-xs">Verbleibende Zeichen: <strong>' + (maxlength - characters) + '</strong></span><span class="hidden-sm hidden-md hidden-lg"><small>verb. Zeichen: <strong>' + (maxlength - characters) + '</strong></small></span></span>');
		$('.characters').fadeIn();
	});

	// Zeichenanzahl ermitteln
	$(".content-inner").on('keypress', 'input, textarea', charactersCount);
	$(".content-inner").on('keydown', 'input, textarea', charactersCount);
	$(".content-inner").on('keyup', 'input, textarea', charactersCount);
	$(".content-inner").on('change', 'input, textarea', charactersCount);

	// Verbleibende Zeichen entfernen
	$(".content-inner").on('focusout', 'input, textarea', function() {
		$('.characters').remove();
	});
}

/**
 * charactersCount Ermitteln der aktuell verbleibenden Zeichen
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 */
function charactersCount() {
	maxlength = $(this).attr('maxlength');
	characters = $(this).val().length + ($(this).val().length - $(this).val().replace(/\n/g, '').length);

	$('.characters strong').text(maxlength - characters);
}