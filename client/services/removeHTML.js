/**
 * removeHTML Entfernen das HTML Befehle in den Daten eingetragen werden
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @return {bool} HTML gefunden?
 */
function removeHTML() {
    var error = false;

    $("input, textarea").each(function() {
        if ( $(this).val() !== $(this).val().replace(/<(?:.|\n)*?>/gm, '').replace(/["'<>]/g, "") ) {
            error = true;
            $(this).val( $(this).val().replace(/<(?:.|\n)*?>/gm, '').replace(/["'<>]/g, "") );
            $(this).parent().addClass("has-error");

            if ($(this).attr('type') === 'password') {
                $(this).val('');
            }
        }
    });

    if (error) {
        notifyError('Ungültige Zeichen wurden in den rot markierten Feldern entfernt');
    }
    return error;
}