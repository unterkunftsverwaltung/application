/* 
 Created on : 28.02.2016, 18:10:44
 Author     : Dennis
 */

/** ********************************************************
 * checkEntry
 * 
 * @author Rafal Welk
 * @param {jQueryElement} e1
 * @param {jQueryElement} e2
 * @return {boolean} gibt Ergebnis zurück
 * ********************************************************/
function checkEntry(e1, e2) {
    var error = false;

    $(".has-error").removeClass("has-error");

    if (e1.val().trim() === '') {
        error = true;
        e1.parent().addClass("has-error");
    }

    if (e2.val().trim() === '') {
        error = true;
        e2.parent().addClass("has-error");
    }

    if (error) {
        notifyError('Bitte die rot markierten Felder ausfüllen');
    }

    return error;
}