/**
 * optimalNotes Zeichen im Popover begrenzen
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @description Bindestrich bei zu langen Wörtern hinzufügen, neue Zeilen umwandeln, unnötige Leerzeilen verhindern
 * @param  {string} notes Bemerkungen die Ausgegeben werden sollen
 * @return {string} Optimierte Ausgabe ober leerer String
 */
function optimalNotes(notes) {
    var counter = 0, 
        zeichen = '',
        zeilen = '', 
        optimiert = '';

    if (notes !== '') {

        // Bindestrich bei langen Wörtern hinzufügen
        for (var i = 0; i < notes.length; i++) {
            zeichen = notes.charAt(i);

            if (zeichen === ' ' || zeichen === '\n') {
                counter = 0;
            } else {
                counter += 1;
            }

            optimiert += zeichen;

            if (counter >= 30) {
                counter = 0;
                optimiert += '-';
            }
        }

        // Umwandeln von neuen Zeilen in <br>
        optimiert = optimiert.replace(/\n/g,"<br>");

        // Unnötige Leerzeichen je Zeile entfernen
        zeilen = optimiert.split('<');
        optimiert = '';
        zeilen.forEach(function (zeile) {
            if (zeilen.length >= 10) {
                optimiert += ' ' + zeile.trim().replace('br>', '');
            } else {
                optimiert += '<' + zeile.trim();
            }
        });
        optimiert = optimiert.substring(1);

        // Überprüfen das keine doppelten Leerzeilen vorkommen
        while(optimiert.indexOf('<br><br>') > 0) {
            optimiert = optimiert.replace('<br><br>','<br>');
        }
        
        return optimiert;
    } else {
        return '';
    }
}