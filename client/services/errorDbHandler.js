/* 
 Created on : 28.02.2016, 16:10:35
 Author     : Dennis Freitag
 */

/* global socket */

socket.on('errorHandler', function(err) {
    notifyError('Fehler von der Datenbank: ' + err.code);
});

socket.on('errorHandlerDB', function(errors) {
	var msg = '';

	for (var i = 0; i < errors.length; i++) {
		msg += '<strong style="color: black;">' + errors[i].code + '</strong>: ' + errors[i].query + '<br>';
	}
    notifyErrorDB('Datenbank-Fehler', msg);
});

socket.on('errorHandlerClose', function(msg) {
	notifyErrorClose('Anwendung beendet', msg);

	if ($('html').hasClass('login') === false) {
		$('#login').show().addClass('fadeInDown');
		$('html').addClass('login');
		$('#logout').popover('destroy');
		$('#group').popover('destroy');
		document.title = 'Unterkunftsverwaltung';
		routeProvider.navigateTo('indexView');
		window.history.replaceState(null, '', "#/");

		uvUser = null;
		uvRights = null;
	}

	$('#loginscreen').hide();
	$('#wrapper').hide();
});