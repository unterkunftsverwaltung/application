/**
 * Überschriften ein- und ausblenden 
 * @author Rafal Welk <rafal@project.hatua.de>
 */

/**
 * autolabel Ein- und Ausblenden der Überschriften 
 *           Binden der jQuery Selectoren
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 */

function autolabel() {

  // Input Forms
  $(".content-inner").on('focusin', '.autolabel input, .autolabel textarea', function() {
    if ($(this).val().trim() === '') {
      $(this).prev("label").removeClass("lblhide").addClass("animated slideInUp").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
        $(this).removeClass('animated slideInUp');
      });
    }
  });

  $(".content-inner").on('focusout', '.autolabel input, .autolabel textarea', function() {
    if ($(this).val().trim() === '') {
      $(this).prev("label").addClass("animated slideOutDown").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
        $(this).removeClass('animated slideOutDown').addClass("lblhide").hide().fadeIn();
      });
    }
  });

  // Bootstrap Selectpicker
  $('.content-inner').on('show.bs.select', '.autolabel .bootstrap-select', function(e) {
    if ($(this).children("select").val() === '' && $(this).prev('label').hasClass('lblhide') === true ) {
      $(this).prev("label").removeClass("lblhide").addClass("animated slideInUp").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
        $(this).removeClass('animated slideInUp');
      });
    }
  });

  $('.content-inner').on('hide.bs.select', '.autolabel .bootstrap-select', function(e) {
    if ($(this).children("select").val() === '') {
      $(this).prev("label").addClass("animated slideOutDown").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
        $(this).removeClass('animated slideOutDown').addClass("lblhide").hide().fadeIn();
      });
    }
  });

  // Bootstrap Treeview
  $('.content-inner').on('nodeSelected', '.autolabel #tree', function(e, data) {
    nodeSelected();
  });

  $('.content-inner').on('nodeUnselected', '.autolabel #tree', function(e, data) {
    nodeSelected();
  });
  
  // Bootstrap datepicker
  $(".content-inner").on('show', '.autolabel .input-group.date', function(index) {
    if (index.dates.length <= 0 && $(this).prev('label').hasClass('lblhide') === true ) {
      $(this).prev("label").removeClass("lblhide").addClass("animated slideInUp").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
        $(this).removeClass('animated slideInUp');
      });
    }
  });

  $(".content-inner").on('hide', '.autolabel .input-group.date', function(index) {
    if (index.dates.length <= 0) {
      $(this).prev("label").addClass("animated slideOutDown").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
        $(this).removeClass('animated slideOutDown').addClass("lblhide").hide().fadeIn();
      });
    }
  });

}

function nodeSelected() {

  setTimeout(function() {
    if ($(".autolabel #tree .node-selected").length === 0) {

      $('.autolabel #tree').prev("label").addClass("animated slideOutDown").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
        $(this).removeClass('animated slideOutDown').addClass("lblhide").hide().fadeIn();
      });

    } else {
      if ($('.autolabel #tree').prev('label').hasClass('lblhide') === true) {

        $('.autolabel #tree').prev("label").removeClass("lblhide").addClass("animated slideInUp").one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function() {
          $(this).removeClass('animated slideInUp');
        });

      }
    }

  }, 200);
  
}

/**
 * autolabelClean Alle Überschriften ausblenden
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 */
function autolabelClean() {
  $('.autolabel label').each(function(index) {
    $(this).addClass('lblhide');
  });
}

/**
 * autolabelSet Ermitteln ob Daten eingetragen sind und anzeigen der Überschriften falls ja
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 */
function autolabelSet() {
    $('.autolabel input, .autolabel textarea').each(function(index) {
    if ($(this).val().trim() !== '') {
      $(this).prev('label').removeClass("lblhide");
    } else {
      $(this).prev('label').addClass("lblhide");
    }
  });

  $('.autolabel select').each(function(index) {
    if ($(this).val() !== '') {
      $(this).parent('.bootstrap-select').prev('label').removeClass("lblhide");
    } else {
      $(this).parent('.bootstrap-select').prev('label').addClass("lblhide");
    }
  });

  if ($(".autolabel #tree .node-selected").length > 0) {
    $('.autolabel #tree').prev('label').removeClass("lblhide");
  } else {
    $('.autolabel #tree').prev('label').addClass("lblhide");
  }
}
