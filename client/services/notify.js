/**
 * Bootstrap Notify
 * Erfolgs-, Fehlermeldungen
 */

var notify;

/* Default Configuration */
$.notifyDefaults({
    placement: {
      from: "top",
      align: "center"
    },
    allow_dismiss: true,
    newest_on_top: true,
    animate:{
      enter: "animated bounceIn",
      exit: "animated bounceOut"
    },
    offset: {
    	y: 10
    },
    type: 'danger',
    timer: 2000
});


function notifyError(msg) {

	notifyClose();

	$.notify({
		icon: 'glyphicon glyphicon-warning-sign',
        title: "<strong>Fehler</strong><br>",
        message: msg
	});

	$('.has-error input:first').focus();
}

function notifyErrorDB(title, msg) {

  notifyClose();

  $.notify({
    icon: 'glyphicon glyphicon-hdd',
    title: "<strong>" + title + "</strong><br>",
    message: msg
  });
}

function notifyErrorClose(title, msg) {

  notifyClose();

  $.notify({
    title: title + "<br>",
    message: msg
  });
}

function notifySuccess(title, msg) {

	notifyClose();

	$.notify({
		icon: 'glyphicon glyphicon-ok',
        title: "<strong>" + title + "</strong><br>",
        message: msg
	},{
		type: 'success'
	});
}

function notifyWarning(title, msg) {

  notifyClose();

  $.notify({
    icon: 'glyphicon glyphicon-warning-sign',
        title: "<strong>" + title + "</strong><br>",
        message: msg
  },{
    type: 'warning'
  });
}

function notifyClose() {
  $('*[data-notify="container"]').hide();
}