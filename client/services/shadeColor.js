/**
 * shadeColor Funktion um die Einheiten auch optisch voneinander zu trennen
 *   durch die übergabe einer Farbe und um wieviel Prozent diese heller werden
 *   soll
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param  {string} color   Farbe im Hex-Format (#ffffff)
 * @param  {int} percent Prozentzahl um wieviel die Farbe heller werden soll
 * @return {string} Rückgabe der helleren Farbe im Hex-Format
 */
function shadeColor(color, percent) {

    var R = parseInt(color.substring(1,3),16);
    var G = parseInt(color.substring(3,5),16);
    var B = parseInt(color.substring(5,7),16);

    R = parseInt(R * (100 + percent) / 100);
    G = parseInt(G * (100 + percent) / 100);
    B = parseInt(B * (100 + percent) / 100);

    R = (R<255)?R:255;  
    G = (G<255)?G:255;  
    B = (B<255)?B:255;  

    var RR = ((R.toString(16).length==1)?"0"+R.toString(16):R.toString(16));
    var GG = ((G.toString(16).length==1)?"0"+G.toString(16):G.toString(16));
    var BB = ((B.toString(16).length==1)?"0"+B.toString(16):B.toString(16));

    return "#"+RR+GG+BB;
}

/**
 * shadeColorLinear Funktion um die Einheiten auch optisch voneinander zu trennen
 *   durch die übergabe einer Farbe und um wieviel Prozent diese heller werden
 *   soll
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param  {string} color   Farbe im Hex-Format (#ffffff)
 * @param  {int} percent Prozentzahl um wieviel die Farbe heller werden soll
 * @return {string} Rückgabe der helleren Farbe als liniearer Verlauf
 */
function shadeColorLinear(color, percent) {

    var R = parseInt(color.substring(1,3),16);
    var G = parseInt(color.substring(3,5),16);
    var B = parseInt(color.substring(5,7),16);

    R = parseInt(R * (100 + percent) / 100);
    G = parseInt(G * (100 + percent) / 100);
    B = parseInt(B * (100 + percent) / 100);

    R = (R<255)?R:255;  
    G = (G<255)?G:255;  
    B = (B<255)?B:255;  

    return 'background: linear-gradient(90deg, rgba('+R+','+G+','+B+',1) 0%, rgba('+R+','+G+','+B+',0.85) 100%);';
}