/**
 * Search 
 * Suche
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @description Einblenden der Suchfunktion bei zu kleiner Darstellung und reagieren auf Eingaben 
 */

// Suche bei kleinen Auflösungen ein- und ausblenden
$('#search').click(function(e) {
	e.preventDefault();

	$('#searchBox').toggleClass('mobilesearch');
	$('#header-search-field').focus();
});

// Bei kleinen Auflösungen automatisch ausblenden wenn das Textfeld verlassen wird
$('#nav-header').on('focusout', '.mobilesearch #header-search-field', function() {
	setTimeout(function() {
		$('#searchBox').removeClass('mobilesearch');
	}, 100);
});

// Suche starten nach Tastendruck
$('#nav-header').on('keypress', '#header-search-field', searchheader);
$('#nav-header').on('keyup', '#header-search-field', searchheader);
$('#nav-header').on('keydown', '#header-search-field', searchheader);
$('#nav-header').on('change', '#header-search-field', searchheader);

// Suche zurücksetzen
$('#searchClear').click(function(e) {
	e.preventDefault();
	$('#header-search-field').val('');
	$('#searchClear').addClass('notvisible');
	$('.searchIcon .glyphicon').removeClass('active');
	$('tr[data-search]').fadeIn(100);
	$('table[data-unitid]').fadeIn(100);
	$('table[data-buildingid]').fadeIn(100);
});

// Suchen ob Übereinstimmungen auftreten
// Suchen die mit einem doppelten Anführungzeichen versehen sind werden nicht gesplittet
function searchheader() {
	var searchFor = $('#header-search-field').val().trim().toLowerCase();
	var searchArray = searchFor.match(/(".*?"|[^"\s]+)+(?=\s*|\s*$)/g);

	if (searchFor !== '' && searchArray !== null) {

		// Anzeigen das die Suche aktiv ist und Löschen Button anzeigen
		$('#searchClear').removeClass('notvisible');
		$('.searchIcon .glyphicon').addClass('active');

		// Überprüfen jedes einzelnen Beitrages auf die Suchbegriffe
		$('tr[data-search]').each(function() {
			var rowSearch = $(this).attr('data-search').replace(new RegExp('°', 'g'), '"').toLowerCase();
			var found = 0;

			searchArray.forEach(function(element) {
				if (rowSearch.search(element) > -1) {
					found += 1;
				}
			});

			if (searchArray.length === found) {
				$(this).fadeIn(100);
				$(this).parent('tbody').parent('table[data-unitid]').fadeIn(100);
				$(this).parent('tbody').parent('table[data-buildingid]').fadeIn(100);
			} else {
				$(this).fadeOut(100);
			}
		});

		setTimeout(function() {
			$('table[data-unitid] thead').each(function() {
				if ($(this).siblings('tbody').children('tr[data-search]:visible').length <= 0) {
					$(this).parent('table[data-unitid]').hide();
				} else {
					$(this).parent('table[data-unitid]').fadeIn(300);
				}
			});

			$('table[data-buildingid] thead').each(function() {
				if ($(this).siblings('tbody').children('tr[data-search]:visible').length <= 0) {
					$(this).parent('table[data-buildingid]').hide();
				} else {
					$(this).parent('table[data-buildingid]').fadeIn(300);
				}
			});

			if ($('tbody tr[data-search]:visible').length <= 0) {
				$('#noSearchResult:hidden').fadeIn(300);
			} else {
				$('#noSearchResult:visible').hide();
			} 
		}, 120);
		
	} else {
		$('#searchClear').addClass('notvisible');
		$('.searchIcon .glyphicon').removeClass('active');
		$('tr[data-search]').fadeIn(100);
		$('table[data-unitid]').fadeIn(100);
		$('table[data-buildingid]').fadeIn(100);
	}
}