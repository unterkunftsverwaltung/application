/* 
 Created on : 20.02.2016, 13:20:24
 Author     : Dennis
 */

function transferFunction() {
}

transferFunction.prototype.ranks = function() {
    this.editRank = null;
};

transferFunction.prototype.units = function() {
    this.editUnit = null;
    this.selectedNode = null;
};

transferFunction.prototype.users = function() {
    this.editUser = null;
};

transferFunction.prototype.roles = function() {
    this.editRole = null;
};

transferFunction.prototype.building = function() {
    this.editBuilding = null;
};

transferFunction.prototype.room = function() {
    this.addRoom = null;
    this.editRoom = null;
    this.selectedNode = null;
};

transferFunction.prototype.roomBook = function() {
    this.roomID = null;
    this.residentID = null;
};

transferFunction.prototype.residents = function() {
    this.residentID = null;
    this.selectedNode = null;
};

transferFunction.prototype.accommodations = function() {
    this.residentID = null;
};

var transfer = new transferFunction();