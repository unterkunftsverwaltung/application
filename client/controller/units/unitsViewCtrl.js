/**
 * unitsViewCtrl
 * Bewohner nach Einheiten anzeigen
 */

// Deklaration
var $ci 			= '.content-inner',
	$tu 			= '#tableUnits',
	$list 			= '#list', 

	parents 		= [],
	units 			= [],
	unitsSearch		= {},
	residentString  = [];

$(document).ready(function() {

	// Ermitteln der benötigten Einheiten in der richtigen Reihenfolge
	// und erstellen der Tabellen
	socket.emit('loadUnitsWithoutParent', function(parentUnits) {
	    // laden aller Units
	    socket.emit('loadUnitData', function(units) {
	    	// Laden der Bewohner (auf die der Benutzer zugreifen darf)
	    	socket.emit('loadResidents', 'unit', function(data) {


	    		if (uvRights.view === true) {
					$('.content-inner div.hidden').removeClass('hidden').html('<br><br><strong>Es wurde noch keine Einheiten hinzugefügt</strong><br><br>');
					$('#searchBox').addClass('hidden');
				} else if (data.residents.length <= 0) {
					$('.content-inner div.hidden').removeClass('hidden').html('<br><br><strong>Es wurden keine Bewohner gefunden für die Ausgabe</strong><br><br>');
					$('#searchBox').addClass('hidden');
		     	} else {

		     		// Einheiten ermitteln
					$.each(data.units, function(index, element) {
						unitsSearch[element.unitID] = {
							id: element.unitID,
							name: element.name,
							short: element.short,
							order: '',
							search: '',
							member: element.member,
							level: -1
						}
					});

					// Struktur der Einheiten für die Darstellung einbinden
					$.each(unitsSearch, function(index, element) {
						var next = unitsSearch[element.id].id;
							do {
								if (element.order === '') {
									element.order = '<strong>' + unitsSearch[next].name + '</strong>';
									element.search = '°' + unitsSearch[next].name + '° °' + unitsSearch[next].short + '°';
								} else {
									element.order = unitsSearch[next].name + '\n' + element.order;
									element.search = unitsSearch[next].name + ' ' + unitsSearch[next].short + ' ' + element.search;
								}
								element.level += 1;
								next = unitsSearch[next].member;
							} while (next > 0);
					});

		     		// Bewohner zusammenstellen
					$.each(data.residents, function(index, resident) {
						
						// Räume des Bewohners ermitteln
						var residentrooms = [];
						$.each(data.rooms, function(index, room) {
							if (room.residentID == resident.residentID) {
								residentrooms.push(room);
							}
						});

						if (! residentString[resident.unitID]) residentString[resident.unitID] = "";
						residentString[resident.unitID] += buildResidents(resident, residentrooms, units);
					});

		     		// Einheiten ermitteln
		     		var childSearch = new Array();

		            // Übernehmen der Einheiten die an oberste Stelle stehen [member IS NULL]
		            $.each(parentUnits, function(index, parentElement) {
		                
		                // Hinzufügen zu 'parents' und vorher level auf 0 setzen
		                // Erstellen des Eintrags mit der Funktion unitTemplate
		                parentElement.level = 0;
		                $($list).append(unitTemplate(parentElement));
		                parents.push(parentElement);
		                childSearch.push(parentElement);
		            });

		            // Ermitteln der untergeordneten Einheiten und erstellen des Eintrags unter der übergeordneten Einheit
		            do {
		                var nextSearch = new Array();
		                childSearch.forEach(function(above) {
		                    var nextUnits = new Array();

		                    units.forEach(function(below) {

		                        if (above.unitid === below.member) {

		                            // Als möglicher Elternteil für den nächsten Suchlauf eintragen
		                            nextSearch.push(below);

		                            // Level und ParentName anfügen
		                            below.level = above.level + 1;                 
		                            below.parent = above.name;
		                            below.parentID = above.unitid;

		                            // Eintrag hinzufügen
		                            $('#' + above.unitid).append(unitTemplate(below));
		                        } else {
		                            // Da kein Elternteil muss es sich um eine untergeordnete Einheit handeln
		                            // diese für den nächsten Durchgang eintragen
		                            nextUnits.push(below);
		                        }
		                    });

		                    units = nextUnits;
		                });

		                childSearch = nextSearch;
		            } while (nextSearch.length > 0);

		            // jQuery Funktion reverse
		            jQuery.fn.reverse = [].reverse;

		            // Umbau der Liste auf eine Ebene
		            $($list + ' li').reverse().each(function() {
		                $(this).prependTo($list);
		            });

		            // Markieren der benötigten Einheiten
		            var rightView = (uvRights.view + ",").split(',');
		            rightView.forEach(function(unit) {
		            	if (unit !== '') $($list + ' li#' + unit).attr('data-copy', true);
		           	});

		           	// Erstellen der benötigten Tabellen
		           	$('li[data-copy]').each(function() {
		           		var unit = {
		           			short: 		$(this).attr('data-short'),
		           			name: 		$(this).attr('data-name'),
		           			unitid: 	$(this).attr('id'),
		           			parentid: 	$(this).attr('data-parentid'),
		           			parent: 	$(this).attr('data-parent'),
		           			level: 		$(this).attr('data-level')
		           		}

		           		units.push(unit);
		           	});

		           	$($list).remove();

		           	// Erstellen der Tabellen
					units.forEach(function(unit) {
						$($tu).append(tableTemplate(unit, residentString));
					});

					// Wechsel der Bewohner ermöglichen
					$($tu).on('click', '.residentClick', function() {
						routeProvider.navigateTo('accomResidentView', $(this).attr('data-residentid'));
					});
	     		}

	     		// Popover aktivieren
				$('.label-additional, .label-residentroom, .residentnotes').popover({html: true});
	     	});
	    });
	});
});

/**
 * unitTemplate Funktion um ein Template zusammenzubauen für die HTML Einträge
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param  {object} unit Einheit aus der die Daten ermittelt werden
 * @return {string} Rückgabe des HTML Einträgs für die Einheit
 */
function unitTemplate(unit) {
    var $listtemplate = 
    '<li id="' + unit.unitid + '" data-level="' + unit.level + '" data-short="' + unit.short + '" data-parentid="' + (unit.parentID ? unit.parentID : '') + '" data-parent="' + (unit.parent ? unit.parent : '') + '" data-name="' + unit.name + '">' + unit.name + '</li>';

    return $listtemplate;
}

function tableTemplate(unit, residentString) {
	if (residentString[unit.unitid]) {
		var contentString = 
		'<table id="unit-' + unit.unitid + '" data-unitid="' + unit.unitid + '"class="table table-hover simpletable">' + 
			'<thead>' + 
				'<tr>' + 
					'<th colspan="2">' +
					
					'<span class="label pull-right" style="background-color: ' + shadeColor('#c06c1c', unit.level*15) + '; ' + (unit.level > 4 ? 'color: #303030;' : '') + '">' + unit.short + '</span> ' + 
					'<span class="pull-left">' +
					(unit.parent ? '<small>' + unit.parent + '</small><br>' : '<small>&nbsp;</small><br>') +
					unit.name + 
					'</span>' + 
					'</th>' + 
				'</tr>' + 
			'</thead>' + 
	        '<tbody>' + residentString[unit.unitid] + '</tbody>' +
	    '</table>';

	    return contentString;
    } else {
    	return '';
    }
}

function buildResidents(resident, rooms, units) {
	var contentString = '';

	contentString =
	'<tr id="' + resident.residentID + '" data-search="' + buildSearch(resident, rooms, unitsSearch) + '">' + 
		'<td class="col-xs-12 col-sm-6 col-md-4">' + 
			'<div class="residentHover residentClick" data-residentid="' + resident.residentID + '">' + 
			'<span class="fa fa-' + (resident.sex === 'male' ? 'male' : 'female') + '"></span>&nbsp;&nbsp;<span class="residentnotes" data-toggle="popover" data-html="true" data-trigger="hover" data-placement="auto bottom" data-content="' + (resident.notes === null || resident.notes === '' ? '' : resident.notes) + '"><strong>' + (resident.notes === null || resident.notes === '' ? resident.surname : '<u>' + resident.surname + '</u>') + '</strong>, ' + resident.forename + '</span> ' + 
			'<span class="label-box label-specialbreak">' + 
				'<span class="label label-additional label-ranks" data-toggle="popover" data-html="true" data-trigger="hover" data-placement="auto bottom" data-content="' + resident.ranklabel + '">' + resident.rankshort + '</span>' +
			'</span>' + 
			'</div>' +
			'<span class="hidden-sm hidden-md hidden-lg residentroom-sm"><br>' + buildRooms(rooms) + 
			'</span>' +
		'</td>' + 
		'<td class="hidden-xs col-sm-6 col-md-8">' + 
		'<span class="pull-right text-right">' + 
		buildRooms(rooms) + 
		'</span>' +
		'</td>' +
	'</tr>';

	return contentString;
}

function buildRooms(rooms) {
	var contentString = '';

	rooms.forEach(function(room) {
		contentString +=
		'<span class="label label-residentroom btn-rooms ' + (room.alias !== '' || room.until !== null ? '' : 'cursordefault') + '" data-residentid="' + room.residentID + '" data-toggle="popover" data-html="true" data-trigger="hover" data-placement="auto bottom" ';

		if (room.alias !== '' || room.until !== null) {
			contentString +=
			'data-content="' + (room.alias === '' ? '' : '<strong>' + room.alias + '</strong><br>') + (room.until === null ? '' : 'vorauss. Belegung: <em>' + room.until + '</em><br>') + '"';
		}

		contentString +=
		'>' + 
		'Gebäude ' + room.buildingnumber + ', Raum ' + room.roomnumber + 
		'</span>';
	});

	return contentString;
}

function buildSearch(resident, rooms, units) {
	var searchString = 
		'°' + resident.ranklabel + '° ' +
		'°' + resident.rankshort + '° ' +
		'°' + resident.surname + '° ' +
		'°' + resident.forename + '° ' + 
		units[resident.unitID].search + ' ';

	if (resident.notes !== null && resident.notes !== '') {
		searchString += resident.notes + ' ';
	}

	if (resident.sex === 'male') {
		searchString += '°m° °Mann° °Herr° °männlich° °male° ';
	} else {
		searchString += '°w° °Frau° °weiblich° °female° ';
	}

	if (rooms.length < 1) {
		searchString += '°keine Unterkunft° ';
	}

	rooms.forEach(function(room) {
		searchString += room.alias + ' °Gebäude ' + room.buildingnumber + '° °Raum ' + room.roomnumber + '° ';
	});

	return searchString;
}