/* 
 Created on : 20.02.2016, 00:11:41
 Author     : Dennis
 */

/* global */

$('a.indexPage').click(function(e) {
	e.preventDefault();
	routeProvider.navigateTo( $(this).attr('href') );
})