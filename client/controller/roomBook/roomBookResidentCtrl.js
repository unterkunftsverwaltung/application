/**
 *  roomBookResidentCtrl
 *  
 *  Anzeige, um einen gegebenden Bewohner in eine Unterkunft zu buchen
 *  @author Dennis Freitag
 * 
 */

var $unittree       = $('#tree'),
    $accomm         = $('#accomm'),    
    $rank           = $('#rank'),
    $gender         = $('#gender'),
    $datepicker     = $('.datepickerInput'),
    $addBtn         = $('#addBtn'),
    $clearBtn       = $('#clearBtn'),
    $btnback        = $('#btn-back'),
    filter          = ['gender'],
    fullRooms       = null,
    ownUnit         = false,
    importantData   = [],
    parents         = [],
    emptyRooms      = [],
    fullRooms       = [],
    genderRooms     = [],
    until           = null,
    unitID          = null,
    residentID      = transfer.roomBook.residentID || actURL[4],
    rightEdit       = (uvRights.roomBook + ',').split(','),
    unitNodeID      = null;
    
// Wenn keine residentID angegeben ist -> zurück zur Accom Übersicht
if (residentID === undefined) {
    routeProvider.navigateTo('residentsView');
}

// Prüfen ob Rechte und Resident vorhanden sind
socket.emit('loadResident', residentID, function(data) {
    // Überprüfen ob der Bewohner existiert
    if (data === null) {
        notifyError('Dieser Bewohner existiert nicht');
        routeProvider.navigateTo('residentsView');
        return;
    } 

    // Überprüfen ob der Benutzer die nötigen Rechte hat
    var rightEdit = (uvRights.roomBook + ',').split(',');
    var found = false;

    rightEdit.forEach(function(unit) {
        if (unit.toString() === (data.unitID).toString() ) found = true;
    });

    if (found === false) {
            notifyError('Sie besitzten nicht die benötigten Rechte um diese Funktion zu nutzen');
        routeProvider.navigateTo('residentsView');
        return;
    }   
});

// SelectPicker initiieren
buildAccommodationPicker(filter);

// Zurück Button
$btnback.click(function(e) {
    e.preventDefault();
    routeProvider.navigateTo('residentsView');
});

//Bewohner Auswahl öffnen
$accomm.selectpicker('show');
setTimeout(function() {
    $accomm.selectpicker('toggle');
}, 100);

// Laden der leeren Unterkünfte
socket.emit('loadEmptyRooms', function(empRooms) {
    empRooms.forEach(function(emptyRoom) {
        emptyRooms.push(emptyRoom);
    });
 });

// Konfiguration für den datePicker
$('.input-group.date').datepicker({
    clearBtn: true,
    language: "de",
    calendarWeeks: false,
    todayHighlight: true,
    startDate: new Date(+new Date() + 86400000),                // 86400000ms = 1 Tag später
    endDate: new Date(+new Date() + 315576000000),              // 315576000000ms = 10 Jahre später
    maxViewMode: 2
});

$('.content-inner').on('show', '.input-group.date', function() {
    if ( ($(this).children('input').val().trim() === '') && ($(this).children('input').hasClass('firstDate') === false) ) {
        $(this).datepicker('update', new Date(+new Date() + 86400000));
        $(this).datepicker('update');
        $(this).children('input').addClass('firstDate');
    } 
});

// datePicker Logik
$datepicker.on('change', function(e) {
    var dateArray   = $datepicker.val().split('.'),  
        date        = new Date(dateArray[2] + "-" + dateArray[1] + "-" + dateArray[0]);

    until = date;
});

// Checkboxen auf Änderung überprüfen
$('#checkBoxen > li > label > input').on('change', function(e) {
    if ($(this).prop('checked')) {
        filter.push($(this).attr('id'));
    } else {
        var index = filter.indexOf($(this).attr('id'));
        if (index > -1) {
            filter.splice(index, 1);
        }
    }
    buildAccommodationPicker(filter);
});

// Felder zurücksetzen
$clearBtn.click(function(e) {
    var selectedNode = $($unittree).treeview('getSelected', 0);
    e.preventDefault();
   
    $accomm.selectpicker('val', '');
    $($unittree).treeview( 'unselectNode' , [ selectedNode, { silent: true } ]);
    $datepicker.val('');
    unitID           = null,
    
    $('#checkBoxen > li > label > input').prop('checked', false);
    autolabelClean();
    filter = [];
    buildAccommodationPicker(filter);
});

// buchen Button
$addBtn.click(function(e) {
    e.preventDefault();
    
    var foundRoom = null;
    
    if (!checkEntryRoomBookResident()) {
        socket.emit('loadAllRoomsAndBuildings', function(allRoomsAndBuildings) {
            socket.emit('loadResident', residentID, function(resident) {
                var booking = {
                    roomID: $('#accomm').val().trim(),
                    residentID: residentID
                };

                if (until === null) {
                    booking.until = '';
                } else {
                    booking.until = until.toMysqlFormat();
                }

                // Laden der Unterkunft
                allRoomsAndBuildings.forEach(function(accomm) {
                    if (accomm.roomID == booking.roomID)
                        foundRoom = accomm;
                });

                // Unterkunft zuweisen
                socket.emit('bookResident', booking, function(result) {
                    if (result !== undefined) {
                        notifySuccess(
                                "Unterkunft zugewiesen", "Die Unterkunft " 
                                + "Gebäude " + foundRoom.buildingNumber + " Raum " + foundRoom.roomNumber + " " 
                                + " wurde für den Bewohner " 
                                + resident.surname + ", " + resident.forename + " erfolgreich zugewiesen."
                        );
                        routeProvider.navigateTo('accomView');
                    }
                }); 
            });
        });
    }
});

// Bei Änderung des SelectPickers, fillInfo (Tabelle unterhalb des SelectPickers) befüllen
$accomm.change(function(e) {
    fillInfo();
});

/**
 * buildAccommodationPicker     Aufbau des SelectPickers
 *                              dabei Berücksichtung der Filter
 * 
 * @author Dennis Freitag
 * @param {array of strings} filter         beinhaltet die Filter
 */
function buildAccommodationPicker(filter) {
        importantData = [];
        contentString   = "",
        hiddenCandidate = null,
        gender          = false,
        emptyAccoms     = false,
        aboveUnits      = false;
    
    ownUnit         = false;
    $('#roomInfo').html('');
    
    socket.emit('loadResident', residentID, function(resident) {
       
        if (isInArray('gender', filter))        { gender        = true; }
        if (isInArray('ownUnit', filter))       { ownUnit       = true; }
        if (isInArray('emptyAccomms', filter))   { emptyAccoms   = true; }
        if (isInArray('aboveUnits', filter))    { aboveUnits    = true; } 
        findAboveUnits(resident, true);
        
        socket.emit('loadRoomsByGender', resident, function (roomsByGender) {
            
            roomsByGender.forEach(function(rooms) {
                genderRooms.push(rooms.roomID);
            });         
                    
            socket.emit('usedRooms', function(allData) {
                allData.forEach(function(data) {
                    importantData.push(data);
                });
                socket.emit('loadEmptyRooms', function(emptyRooms) {
                    emptyRooms.forEach(function(emptyRoom) {
                        emptyRoom.currentUsing = 0;
                        importantData.push(emptyRoom);
                        
                    });
                    
                    importantData.sort(function(a, b){ return a.roomID-b.roomID});
                    
                    $.each(importantData, function(index, data) {
                        var index = rightEdit.indexOf(data.unitID.toString());
                        if (index < 0) return;                       
                        
                        if (data.currentUsing >= data.maxoccupancy)
                            return;

                        if (ownUnit && !aboveUnits) {
                            if (resident.unitID != data.unitID) {
                                hiddenCandidate = data;
                            }
                        }

                        if (aboveUnits) {
                            if (!(isInArray(data.unitID, parents))) {
                                hiddenCandidate = data;
                            }
                        }

                        if(emptyAccoms) {
                            if (data.currentUsing != 0)
                                hiddenCandidate = data;
                        }

                        if (gender && !emptyAccoms) {
                            if(!(isInArray(data.roomID, genderRooms)) && data.currentUsing != 0) {
                                hiddenCandidate = data;
                            }
                        }

                        if (hiddenCandidate !== null) {
                            hiddenCandidate = null;
                            return;
                        }

                        contentString   += '<option data-content="<span class=\'pull-right subtext\'>'
                                        + data.currentUsing + "/" + data.maxoccupancy
                                        + '</span> <span class=\'text\'>' 

                                        + 'Gebäude ' + data.buildingNumber + ', ' + 'Raum ' + data.roomNumber
                                        + '</span>">' 
                                        + data.roomID + '</option>';

                    });
                    
                    if (contentString === '') {
                        $accomm.prop('disabled', true);
                        $accomm.selectpicker('val', '');
                        $accomm.selectpicker('refresh'); 
                    } else {
                        $accomm.prop('disabled', false);
                        $accomm.html(contentString).selectpicker();
                        $accomm.selectpicker('refresh');
                    }
                });
            });
        });
    });
}

/**
 * isInArray Prüft, ob übergebender wert in übergebendem array ist
 * 
 * @author Dennis Freitag
 * @param {type} value
 * @param {type} array
 * @returns {Boolean}
 */
function isInArray(value, array) {
    return array.indexOf(value) > -1;
}

/**
 * findAboveUnits   Sucht die übergeordneten Einheiten heraus
 * 
 * @author Dennis Freitag
 * @param {object} resident
 * @param {boolean} own
 */
function findAboveUnits(resident, own) {
    parents = [];
    if (own)
        parents.push(resident.unitID);
    
    socket.emit('allUnits', function(allUnits) {
        schleife(resident.unitID, allUnits, resident);
    });
}

/**
 * schleife     rekursive Funktion zur Ermittlung der übergeordneten Einheiten
 * 
 * @author Dennis Freitag
 * @param {int} unitID
 * @param {object} allUnits
 * @param {object} resident
 */
function schleife(unitID, allUnits, resident) {
    if (unitID !== null) {
        allUnits.forEach(function(unit) {
             if (unitID == unit.unitID) { 
                parents.push(unitID);
                schleife(unit.member, allUnits, resident);
            }
        });
    }
}

/**
 * twoDigits    Fügt, wenn nötig, eine führende 0 
 *              an das übergebende Datum
 * 
 * @author Dennis Freitag
 * @param {type} d
 * @returns {String}
 */
function twoDigits(d) {
    if(0 <= d && d < 10) return "0" + d.toString();
    if(-10 < d && d < 0) return "-0" + (-1*d).toString();
    return d.toString();
}

/**
 * toMysqlFormat    wandelt ein JavaScript Date-Objekt in ein für die Datenbank
 *                  gültiges Format um
 * 
 * @author Dennis Freitag
 * @returns {String}
 */
Date.prototype.toMysqlFormat = function() {
    return this.getUTCFullYear() + "-" 
            + twoDigits(1 + this.getUTCMonth()) + "-" 
            + twoDigits(this.getUTCDate()) + " " 
            + twoDigits(this.getUTCHours()) + ":" 
            + twoDigits(this.getUTCMinutes()) + ":" 
            + twoDigits(this.getUTCSeconds());
};

/**
 * checkEntryRoomBookResident  prüft, ob alle benötigten Felder ausgefüllt sind
 * 
 * @author Dennis Freitag
 * @returns {Boolean}
 */
function checkEntryRoomBookResident() {
    var error = false;
    
    $(".has-error").removeClass("has-error");
    
    if ($('#accomm').val().trim() === '') {
        error = true;
        $('#accomm').parent().parent().addClass('has-error');
        notifyError('Bitte das rot markierte Feld ausfüllen.');
    }
    
    return error;
}

/**
 * fillInfo     Befüllt die Tabelle unterhalb des Unterkunft - SelectPickers
 * 
 * @author Dennis Freitag
 */
function fillInfo() {
    $('#roomInfo').html('');
    var roomID = $('#accomm').val().trim();
    var contentString = "";
    var date = "";
    var datum = "";
    
    socket.emit('loadRoom', roomID, function(room) {
        socket.emit('fillInfo', roomID, function(datas) {
            socket.emit('allUnits', function(allUnits) {
                if (datas.length == 1)
                    contentString += '<tr><th>derzeitiger Bewohner</th><th>voraussichtliche Belegungsdauer</th><th>Einheit</th></tr>';
                else if(datas.length > 1)
                    contentString += '<tr><th>derzeitige Bewohner</th><th>voraussichtliche Belegungsdauer</th><th>Einheit</th></tr>';

                datas.forEach(function(data) {
                    findAboveUnits(data, true);
                    parents = [];
                    schleife(data.unitID, allUnits, data);

                    contentString   += '<tr>'
                                    + '<td><span class="fa fa-' + (data.sex === 'male' ? 'male' : 'female') + '"></span> <label> '
                                    + data.surname + ", " + data.forename + " ";

                    if (data.until !== null)
                        contentString += "<td>" + data.until + "</td>";
                    else                     
                        contentString += "<td>Keine Belegungsdauer angegeben</td>";
                    
                    contentString += "<td>" + drawUnit(allUnits, parents) + "</td>";
                   
                    contentString += '</label></td></tr>';
                });  

                if (contentString === '') {
                    contentString = "<tr><td>Es befindet sich kein Bewohner auf diesem Raum</td></tr>";
                }

                if (room.notes !== '')
                    contentString += "<tr><td> Rauminformation: " + room.notes + "</td>/tr>";

                $('#roomInfo').append(contentString);
            });
        });
    });
}

/**
 * drawUnit Hilfsfunktion um die Einheiten in Tabelle einzutragen
 * 
 * @author Dennis Freitag
 * @param {object} allUnits
 * @param {object} parents
 * @returns {String}
 */
function drawUnit(allUnits, parents) {
    var unitString= "";
    parents.reverse();
    
    parents.forEach(function(parentUnit) {
        allUnits.forEach(function(unit) {
            if (parentUnit == unit.unitID) {
                unitString += unit.name + "<br>";
            }
        });
    });
    return unitString;
}