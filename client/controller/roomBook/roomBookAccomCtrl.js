/**
 *  roomBookAccomCtrl
 *  
 *  Anzeige, um auf einen Raum einen Bewohner buchen zu können
 *  @author Dennis Freitag
 * 
 */
var $unittree       = $('#tree'),
    $residents      = $('#residents'),    
    $rank           = $('#rank'),
    $gender         = $('#gender'),
    $datepicker     = $('.datepickerInput'),
    $addBtn         = $('#addBtn'),
    $clearBtn       = $('#clearBtn'),
    $btnback        = $('#btn-back'),
    fullResident    = null,
    selectedGender  = '',
    selectedRankID  = '',
    until           = null,
    unitID          = null,
    roomID          = transfer.roomBook.roomID || actURL[4],
    unitNodeID      = null;

// Wenn keine roomID angegeben ist -> zurück zur Accom Übersicht
if (roomID === undefined) {
    routeProvider.navigateTo('accomView');
}

// Prüfen ob Rechte und Raum vorhanden sind
socket.emit('loadRoom', roomID, function(data) {
    // Überprüfen ob der Bewohner existiert
    if (data.roomID === null) {
        notifyError('Dieser Raum existiert nicht');
        routeProvider.navigateTo('accomView');
        return;
    } 

    // Überprüfen ob der Benutzer die nötigen Rechte hat
    var rightEdit = (uvRights.roomBook + ',').split(',');
    var found = false;

    rightEdit.forEach(function(unit) {
        if (unit.toString() === (data.unitID).toString() ) found = true;
    });

    if (found === false) {
            notifyError('Sie besitzten nicht die benötigten Rechte um diese Funktion zu nutzen');
        routeProvider.navigateTo('accomView');
        return;
    }   
});

// Zurück Button
$btnback.click(function(e) {
    e.preventDefault();
    routeProvider.navigateTo('accomView');
});

// Einheiten für den TreeView laden
// SOCKET EMIT loadUnits
socket.emit('loadUnits', false, uvRights.view, function(units) {
    $($unittree).treeview({
        data: units,
        levels: 2, 
        color: '#555',
        backColor: '#fff',
        showTags: true,
        selectedBackColor: 'rgb(250,137,0)',
        onNodeSelected: function(event, data) {
            unitID = data.id;
            buildResidentSelectPicker();
        }, 
        onNodeUnselected: function(event, data) {
            unitID = null;
            buildResidentSelectPicker();
        }
    });
    
    // Einheiten die ausgewählt werden können ausklapppen (falls nicht alle verfügbar sind)
    var tree = $($unittree).treeview('getEnabled', 0);
    var nodes = [];

    Object.keys(tree).forEach(function(key) {
        if (tree[key].selectable === true) {
            nodes.push(tree[key].nodeId);
        }
    });

    // Alle übergeordneten Einheiten öffnen (Parents)
    if (nodes.length !== tree.length) {
        nodes.forEach(function(node) {
            $($unittree).treeview('revealNode', [ node, { silent: true } ]);
            
            // Wenn nur Rechte für eine Einheit verfügbar sind, diese Einheit selektieren
            if (nodes.length < 2) {
                $($unittree).treeview('selectNode', [ node, { silent: true } ]);
                unitID = $($unittree).treeview('getSelected', unitID)[0].id;
            }
        });
    }

    // Bewohner Auswahl öffnen
    $residents.selectpicker('show');
    setTimeout(function() {
        $residents.selectpicker('toggle');
    }, 100);
});

// Konfiguration für den datePicker
$('.input-group.date').datepicker({
    clearBtn: true,
    language: "de",
    calendarWeeks: false,
    todayHighlight: true,
    startDate: new Date(+new Date() + 86400000),                // 86400000ms = 1 Tag später
    endDate: new Date(+new Date() + 315576000000),              // 315576000000ms = 10 Jahre später
    maxViewMode: 2
});

$('.content-inner').on('show', '.input-group.date', function() {
    if ( ($(this).children('input').val().trim() === '') && ($(this).children('input').hasClass('firstDate') === false) ) {
        $(this).datepicker('update', new Date(+new Date() + 86400000));
        $(this).datepicker('update');
        $(this).children('input').addClass('firstDate');
    } 
});


// datePicker Logik
$datepicker.on('change', function(e) {
    var dateArray   = $datepicker.val().split('.'),  
        date        = new Date(dateArray[2] + "-" + dateArray[1] + "-" + dateArray[0]);

    until = date;
});

// Selectpicker Männlich/Weiblich laden
$gender.selectpicker();
$gender.on('change', function(e) {
    selectedGender = $gender.val();
    buildResidentSelectPicker();
});

socket.emit('fillInfo', roomID, function(datas) {
    var different = false;
    var gender = "";
    if (datas.length > 0) {
        gender = datas[0].sex;
        
        datas.forEach(function(data) {
            if(data.sex != gender) {
                different = true;
            }
        });
        
        // Vorauswahl des Selectpickers
        if(!different) {
            $('#gender').selectpicker('val', gender);
            selectedGender = gender;
            buildResidentSelectPicker();
        } else {
            selectedGender = '';
            buildResidentSelectPicker();
        }
    } else {
        // Aufbauen des selectPickers, wenn kein Bewohner auf dem Raum ist
        buildResidentSelectPicker();
    }
});

// Laden der Dienstgrade
// SOCKET EMIT loadRanks
socket.emit('loadRankData', function(rank) {
    var contentString;

    $.each(rank, function(index, element) {
        contentString   += '<option data-content="<span class=\'pull-right subtext\'>' 
                        + element.short + '</span> <span class=\'text\'>' + element.label 
                        + '</span>" data-tokens="' + element.short + ' ' + element.label + '">' 
                        + element.rankID + '</option>';
    });

    $rank.append(contentString).selectpicker();
    
    // auf Änderung des Dienstgrad-SelectPickers achten
    $rank.on('change', function(e) {
        selectedRankID = $rank.val();
        buildResidentSelectPicker();
    });
});

// buchen Button
$addBtn.click(function(e) {
    e.preventDefault();
    var max     = 0;
    
    if (!checkEntryRoomBookAccom()) {
    // prüfen ob bewohner schon auf dem raum sitzt
        socket.emit('loadRoom', roomID, function(room) {
            max = room.maxoccupancy;
            socket.emit('loadRoomResidentCount', roomID, function(roomResidents) {        
            
                // Vergleich der maximal möglichen Belegungsanzahl des Raumes
                if (max <= roomResidents[0].anzahl) {
                    notifyError('Die maximale Belegungsanzahl von \"' + max + '\" wurde für diesen Raum erreicht.');
                    return;
                }
                
                var foundResident = null;
                
                var booking = {
                    roomID: roomID,
                    residentID: $('#residents').val().trim()
                };
                
                if (until === null) {
                    booking.until = '';
                } else {
                    booking.until = until.toMysqlFormat();
                }

                // Laden des Bewohners
                fullResident.forEach(function(resident) {
                    if (resident.residentID.toString() === booking.residentID) {
                       foundResident = resident;
                   }
                });

                // Unterkunft zuweisen
                socket.emit('bookResident', booking, function(result) {
                    if (result !== undefined) {
                        notifySuccess(
                                "Bewohner gebucht", "Der Bewohner " 
                                + foundResident.forename + " " 
                                + foundResident.surname + " wurde erfolgreich gebucht."
                        );
                        routeProvider.navigateTo('accomView');
                    }
                });
            });
        }); 
    }
});

// Felder zurücksetzen
$clearBtn.click(function(e) {
    var selectedNode = $($unittree).treeview('getSelected', 0);
    e.preventDefault();
   
    $residents.selectpicker('val', '');
    $rank.selectpicker('val', '');
    $gender.selectpicker('val', '');
    $($unittree).treeview( 'unselectNode' , [ selectedNode, { silent: true } ]);
    $datepicker.val('');
    selectedGender  = '',
    selectedRankID  = '',
    unitID           = null,
    buildResidentSelectPicker();
    autolabelClean();
});

/**
 * buildResidentSelectPicker    baut den SelectPicker für die Bewohner auf
 *                              berücksichtigt die Filtereinstellungen
 * 
 * @author Dennis Freitag
 */
function buildResidentSelectPicker() {
    var rightEdit = (uvRights.view + ',').split(',');

    // Laden aller Bewohner -> immer neu abfragen, da andere Benutzer bereits neue Bewohner erstellt haben könnten
    socket.emit('loadAllResidents', function(allResidents) {
        fullResident = allResidents;
        
        // Befüllen des resident selectpickers
        var contentString   = "",
            sex             = "",
            residentsFromRoomArray = [];
            
        // ist resident schon auf dem raum? -> nicht mehr anzeigen!
        socket.emit('loadResidentsFromRoom', roomID, function(residentsFromRoom) {
            
            residentsFromRoom.forEach(function(residentFromRoom) {
                residentsFromRoomArray.push(residentFromRoom.residentID);
            });
            
            // Ausblenden von Bewohnern, die bereits auf dem Raum gebucht sind
            $.each(allResidents, function(index, resident) {
                var i = residentsFromRoomArray.indexOf(resident.residentID);
                if (!(i < 0)) {
                    return;
                }
                        
                // Anwenden der Filter
                var index = rightEdit.indexOf(resident.unitID.toString());
                if (index < 0) return;
                if (selectedGender  !== ''      && resident.sex     !== selectedGender) return;
                if (unitID          !== null    && resident.unitID  != unitID)          return;
                if (selectedRankID  != ''       && resident.rankID  != selectedRankID)  return;

                if (resident.sex === 'male')    sex = 'fa-male';
                else                            sex = 'fa-female';

                contentString   += '<option data-content="<span class=\'pull-right subtext\'>' 
                                + '</span> <span class=\'text\'>' 
                                + '<span class=\'fa ' + sex + '\'> </span> '
                                + resident.surname + ', ' + resident.forename
                                + '</span>">' 
                                + resident.residentID + '</option>';
            });
            if (contentString === '') {
                $residents.prop('disabled', true);
                $residents.selectpicker('refresh'); 
            } else {
                $residents.prop('disabled', false);
                $residents.html(contentString).selectpicker();
                $residents.selectpicker('refresh');
            }
            
        });
    });
}

/**
 * twoDigits    Fügt, wenn nötig, eine führende 0 
 *              an das übergebende Datum
 * 
 * @author Dennis Freitag
 * @param {type} d
 * @returns {String}
 */
function twoDigits(d) {
    if(0 <= d && d < 10) return "0" + d.toString();
    if(-10 < d && d < 0) return "-0" + (-1*d).toString();
    return d.toString();
}

/**
 * toMysqlFormat    wandelt ein JavaScript Date-Objekt in ein für die Datenbank
 *                  gültiges Format um
 * 
 * @author Dennis Freitag
 * @returns {String}
 */
Date.prototype.toMysqlFormat = function() {
    return this.getUTCFullYear() + "-" 
            + twoDigits(1 + this.getUTCMonth()) + "-" 
            + twoDigits(this.getUTCDate()) + " " 
            + twoDigits(this.getUTCHours()) + ":" 
            + twoDigits(this.getUTCMinutes()) + ":" 
            + twoDigits(this.getUTCSeconds());
};

/**
 * checkEntryRoomBookAccom  prüft, ob alle benötigten Felder ausgefüllt sind
 * 
 * @author Dennis Freitag
 * @returns {Boolean}
 */
function checkEntryRoomBookAccom() {
    var error = false;

    $(".has-error").removeClass("has-error");
    
    if ($('#residents').val().trim() === '') {
        error = true;
        $('#residents').parent().parent().addClass('has-error');
        notifyError('Bitte das rot markierte Feld ausfüllen.');
    }
    
    return error;
}