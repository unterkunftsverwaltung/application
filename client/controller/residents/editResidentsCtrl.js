/**
 * editResidentsView Controller
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @extends {socket, transfer, routeProvider}
 */

// Deklaration

var $ci 			= '.content-inner',
	$forename 		= '#forename',
	$surname 		= '#surname',
	$rank 			= '#rank',
	$gender			= '#gender',
	$notes			= '#notes',
	$units 			= '#tree',

	$clearBtn 		= '#clearBtn',
	$resetBtn 		= '#resetBtn',
	$addBtn			= '#addBtn',
	$addRoomBtn		= '#addRoomBtn',
	$addBackBtn		= '#addBackBtn',
	$editBtn		= '#editBtn',

	$add 			= $addBtn + ', ' + $addBackBtn + ', ' + $addRoomBtn;

	userFunc		= null,
	unitID			= null,
	residentID      = transfer.residents.residentID || actURL[4],
	residentData	= {}, 
	residentOrg		= {};

// Bearbeiten oder Hinzufügen
var userFunc            = ($('h2.page_title').text() === 'Bewohner bearbeiten' ? true : false);

$(document).ready(function() {

	// Laden der Dienstgrade
    // SOCKET EMIT loadRanks
    socket.emit('loadRankData', function(rank) {
    	var contentString;

    	$.each(rank, function(index, element) {
    		contentString += '<option data-content="<span class=\'pull-right subtext\'>' + element.short + '</span> <span class=\'text\'>' + element.label + '</span>" data-tokens="' + element.short + ' ' + element.label + '">' + element.rankID + '</option>';
    	});

    	$($rank).append(contentString).selectpicker();
    });

    // Selectpicker Männlich/Weiblich laden
    $($gender).selectpicker();

    // Einheiten für den TreeView laden
	// SOCKET EMIT loadUnits
	socket.emit('loadUnits', false, (userFunc === true ? uvRights.residentEdit : uvRights.residentAdd), function(units) {
	    $($units).treeview({
	        data: units,
	        levels: 2, 
	        color: '#555',
	        backColor: '#fff',
	        showTags: true,
	        selectedBackColor: 'rgb(250,137,0)',
	        onNodeSelected: function(event, data) {
	            unitID = data.id;
	            if (userFunc === true) {
                    checkChangesResident();
                } else {
                    transfer.residents.selectedNode = data.nodeId;
                }
	        }, 
	        onNodeUnselected: function(event, data) {
	            unitID = null;
	            if (userFunc === true) { 
                    checkChangesResident(); 
                } else {
                    transfer.residents.selectedNode = null;
                }
	        }
	    });

        // Einheiten die ausgewählt werden können ausklapppen (falls nicht alle verfügbar sind)
        if (userFunc === false) {
            var tree = $($units).treeview('getEnabled', 0);
            var nodes = [];

            Object.keys(tree).forEach(function(key) {
                if (tree[key].selectable === true) {
                    nodes.push(tree[key].nodeId);
                }
            });

            // Alle übergeordneten Einheiten öffnen (Parents)
            if (nodes.length !== tree.length) {
                nodes.forEach(function(node) {
                    $($units).treeview('revealNode', [ node, { silent: true } ]);
                });
            }

            // Wenn letzte Einheit markieren, die verwendet wurde
            if (transfer.residents.selectedNode !== null && transfer.residents.selectedNode >= 0) {
                $($units).treeview('selectNode', transfer.residents.selectedNode);
                $($units).treeview('expandNode', transfer.residents.selectedNode);
                $($units).treeview('revealNode', transfer.residents.selectedNode);
            }
        } 
	});

    // Berechtigungen überprüfen ob Räume zugewiesen werden können
    if (! uvRights.roomBook) {
        $($addRoomBtn).remove();
        $('.dropdown-menu .divider').remove();
    }

	// Laden des Bewohners zum bearbeiten
	if (userFunc === true) {
		socket.emit('loadResident', residentID, function(data) {
			// Überprüfen ob der Bewohner existiert
			if (data === null) {
	            notifyError('Dieser Bewohner existiert nicht');
	            routeProvider.navigateTo('residentsView');
	            return;
	        } 

	        // Überprüfen ob der Benutzer die nötigen Rechte hat
	        var rightEdit = (uvRights.residentEdit + ',').split(',');
	        var found = false;
	        
	        rightEdit.forEach(function(unit) {
	        	if (unit.toString() === (data.unitID).toString() ) found = true;
	        });

	        if (found === false) {
	        	notifyError('Sie besitzten nicht die benötigten Rechte um diese Funktion zu nutzen');
	            routeProvider.navigateTo('residentsView');
	            return;
	        }

			residentOrg = data;
			loadEditResident();
		});

		// Bei Änderungen an den Eingabefelder diese als Änderung anzeigen
        $($rank).change(function(e) {
            checkChangesResident();
        });

        $($gender).change(function(e) {
            checkChangesResident();
        });

        $($ci + ' input').change(function(e) {
            checkChangesResident();
        });

        $($ci + ' input').keyup(function(e) {
            checkChangesResident();
        });
	}

	// Zurück zur Übersicht
    $("#btn-back").click(function(e) {
        e.preventDefault();
        routeProvider.navigateTo('residentsView');
        transfer.residents.selectedNode = null;
    });

    $('#sidebar-wrapper a').click(function() {
        transfer.residents.selectedNode = null;
    });


    // Felder leeren
    // Eingabefelder leeren
	$($clearBtn).on('click', function(e) {
	    e.preventDefault();
	    $('input[type="text"]').val('');

	    var selectedNode = $($units).treeview('getSelected', 0);
	    $($units).treeview('unselectNode', [ selectedNode, { silent: true } ]);
	    unitID = null;

	    $($rank).selectpicker('deselectAll');
	    $($gender).selectpicker('deselectAll');

	    $(".has-error").removeClass("has-error");
	    autolabelClean();
	    setTimeout(function() {
	        $($forename).focus();
	    }, 300);
	});

	 // Zurücksetzen der eingegebenen Daten
    $($resetBtn).click(function(e) {
        e.preventDefault();
        loadEditResident();
        checkChangesResident();
        $(".has-error").removeClass("has-error");
        setTimeout(function() {
            $('.content-inner input:first').focus();
        }, 100);
    });

	// Hinzufügen des neuen Benutzers
    $($ci).on('click', $add, function(e) {
        e.preventDefault();
        var clicked = $(this).attr('id');

        if (checkEntryResident()) { return; }
        
        prepareAddEdit();
        
        socket.emit('addResident', residentData, function(result) {
        	if (result.exists) {
        		notifyError('Ein Bewohner mit gleichem Namen und gleicher Einheit ist vorhanden. Bitte das Bemerkungsfeld zur Unterscheidung ausfüllen.');
        		$($notes).parent().addClass("has-error");
        		return;
        	} else {
        		if (result !== undefined) {
        			notifySuccess("Bewohner hinzugefügt ", "Bewohner '" + residentData.surname + ", " + residentData.forename + "' wurde erfolgreich hinzugefügt");
        			
        			// Navigation je nach Auswahl
        			if (clicked === $addBackBtn.substring(1) ) {
                        transfer.residents.selectedNode = null;
                        routeProvider.navigateTo('residentsView');
                    } else if (clicked === $addRoomBtn.substring(1) ) {
                    	transfer.residents.selectedNode = null;
                    	routeProvider.navigateTo('roomBookResident', result.resident.insertId);
                	} else {
                        routeProvider.navigateTo('addResidentsView');
                    }
        		}
        	}
        });
    });

    // Bearbeiten eines Bewohners
    $($ci).on('click', $editBtn, function(e) {
    	e.preventDefault();

    	if (checkEntryResident()) {
            return;
        } else if (checkChangesResident()) {
            notifyWarning("Information", "Es wurden keine Änderungen durchgeführt");
            return;
        } else {

        	prepareAddEdit();

        	socket.emit('editResident', residentData, function(result) {
                if(result.exists) {
                    notifyError('Ein Bewohner mit gleichem Namen und gleicher Einheit ist vorhanden. Bitte das Bemerkungsfeld zur Unterscheidung ausfüllen.');
	        		$($notes).parent().addClass("has-error");
	        		return;
                } else {
                    if (result !== undefined) {
                        notifySuccess("Bewohner angepasst ", "Bewohner '" + residentData.surname + ", " + residentData.forename + "' wurde erfolgreich geändert");
                        routeProvider.navigateTo('residentsView');
                    }
                }
            });
        }
    });

});

/**
 * prepareAddEdit Erstellen der Datenvorlage für das Hinzufügen und ändern der Bewohner
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @return {residentData} vorbereitete Vorlage
 */
function prepareAddEdit() {
    // Eingegebene Daten zusammenstellen
    residentData = {
        forename:   $($forename).val().trim(),
        surname:    $($surname).val().trim(),
        rankID:   	$($rank).val(),
        sex:     	$($gender).val(),  
        notes:   	$($notes).val().trim(),  
        unitID: 	unitID
    };

    if (userFunc) residentData.residentID = residentID;

    return residentData;
}

/**
 * checkEntryUser Funktion um zu überprüfen ob alle Felder ausgefüllt wurden
 *
 * @description Fehlerhafte Felder werden rot markiert
 * @author Rafal Welk <rafal@project.hatua.de>
 * @return {bool} Fehler vorhanden = true
 */
function checkEntryResident() {
    var error = false;

    $(".has-error").removeClass("has-error");

    if (removeHTML()) { return true; }

    if ($($forename).val().trim() === '') {
        error = true;
        $($forename).parent().addClass("has-error");
    }

    if ($($surname).val().trim() === '') {
        error = true;
        $($surname).parent().addClass("has-error");
    }

    if ($($rank).val().trim() === '') {
        error = true;
        $($rank).parent().parent().addClass("has-error");
    }

    if ($($gender).val() === '') {
        error = true;
        $($gender).parent().parent().addClass("has-error");
    }

	if (unitID === null) {
		error = true;
        $($units).addClass('has-error');
        $($units).prev('label').addClass('has-error');
	}

    if (error) {
        notifyError('Bitte die rot markierten Felder ausfüllen.');
    } 

    return error;
}

function loadEditResident() {

	$($forename).val(residentOrg.forename);
	$($surname).val(residentOrg.surname);
	$($rank).selectpicker('val', residentOrg.rankID);
	$($gender).selectpicker('val', residentOrg.sex);
	$($notes).val(residentOrg.notes);

	var tree = $($units).treeview('getEnabled', 0);

	Object.keys(tree).forEach(function(key) {
		if (tree[key].id == residentOrg.unitID) {
			residentOrg.nodeId = tree[key].nodeId;
		} 
	});

	unitID = residentOrg.unitID;

	// Aktuelle übergeordnete Einheit auswählen
	$($units).treeview('selectNode', [ residentOrg.nodeId, { silent: true } ]);

	// Alle übergeordneten Einheiten öffnen (Parents)
	$($units).treeview('revealNode', [ residentOrg.nodeId, { silent: true } ]);

	// Autolabel
	autolabelSet();
}

function checkChangesResident() {
	prepareAddEdit();

	if (residentOrg.notes === null) residentOrg.notes = '';

	if (residentOrg.forename !== residentData.forename || 
		residentOrg.surname !== residentData.surname ||
		residentOrg.rankID.toString() !== residentData.rankID ||
		residentOrg.sex.toString() !== residentData.sex ||
		residentOrg.notes !== residentData.notes ||
		residentOrg.unitID !== residentData.unitID) 
	{
		$($resetBtn).prop('disabled', false);
        return false;
	} else {
		$($resetBtn).prop('disabled', true);
        return true;
	}
}