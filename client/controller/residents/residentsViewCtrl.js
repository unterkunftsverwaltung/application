/**
 * residentsViewCtrl
 * Bewohner anzeigen
 */

// Laden der Bewohner
$(document).ready(function() {

	// Laden der Bewohner (auf die der Benutzer zugreifen darf)
	socket.emit('loadResidents', 'resident', function(data) {
		var contentString = '';
		var units = {};

		// Einheiten ermitteln
		$.each(data.units, function(index, element) {
			units[element.unitID] = {
				id: element.unitID,
				name: element.name,
				short: element.short,
				order: '',
				search: '',
				member: element.member,
				level: -1
			}
		});

		// Struktur der Einheiten für die Darstellung einbinden
		$.each(units, function(index, element) {
			var next = units[element.id].id;
				do {
					if (element.order === '') {
						element.order = '<strong>' + units[next].name + '</strong>';
						element.search = '°' + units[next].name + '° °' + units[next].short + '°';
					} else {
						element.order = units[next].name + '\n' + element.order;
						element.search = units[next].name + ' ' + units[next].short + ' ' + element.search;
					}
					element.level += 1;
					next = units[next].member;
				} while (next > 0);
		});

		// Bewohner zusammenstellen
		$.each(data.residents, function(index, resident) {
			
			// Räume des Bewohners ermitteln
			var residentrooms = [];
			$.each(data.rooms, function(index, room) {
				if (room.residentID == resident.residentID) {
					residentrooms.push(room);
				}
			});

			contentString += buildResidents(resident, residentrooms, units);
		});

		// Bewohner ausgeben auf der View
		$("#tableResidents tbody").append(contentString);

		// Popover aktivieren
		$('.label-additional, .label-residentroom, .residentnotes').popover({html: true});

		// AddHandler: Hinzufügen von Bewohnern
		if (uvRights.residentAdd) {
			$('#addNewResident').click(function(e) {
				e.preventDefault(); 
				routeProvider.navigateTo('addResidentsView');
			}); $('#addNewResident').removeClass('hidden');
		} else {
			$('#addNewResident').remove();
		}

		// Bearbeiten und Löschen von Bewohnern entfernen wenn die Berechtigungen nicht ausreichen
		// Bedingungen um Bewohner zu löschen
		// 1. Bewohner ohne Räume (Recht erforderlich: residentDel)
		// 2. Bewohner mit Räumen (Rechte erforderlich: residentDel + roomClear)
		// 3. Bewohner mit Räumen und Schlüsseln (nicht löschbar)
		var rightEdit = (uvRights.residentEdit + ',').split(',');
		var rightDel = (uvRights.residentDel + ',').split(',');
		var rightClear = (uvRights.roomClear + ',').split(',');

		$('.btn-edit, .btn-delete').each(function() {
			var found = false;
			var unitBtn = $(this).attr('data-unitid');

			if ($(this).hasClass('btn-edit')) {
				rightEdit.forEach(function(unit) {
					if ( unitBtn === unit ) found = true; 	
				});	
			} else {
				rightDel.forEach(function(unit) {
					if ( unitBtn === unit ) found = true; 	
				});

				if (found === true && $(this).attr('data-rooms') > 0) {
					found = false;
					rightClear.forEach(function(unit) {
						if ( unitBtn === unit ) found = true; 	
					});
				}
			}
			
			if (found === false) $(this).remove();
		});

		// Überprüfen ob noch ActionBtn vorhanden sind sonst die Darstellung entzerren
		// um den vorhanden Platz voll auszunutzen
		if ($('.btn-edit, .btn-delete').length <= 0) {
			$('td.actions').removeClass('col-xs-4 col-sm-4 col-md-4').addClass('hidden-xs hidden-sm hidden-md hidden-lg');
			$('td.names').removeClass('col-xs-8 col-sm-4 col-md-4').addClass('col-xs-12 col-sm-6 col-md-4');
			$('td.rooms').removeClass('hidden-sm col-md-4').addClass('col-sm-6 col-md-8');
			$('span.residentroom-sm').addClass('hidden-sm');
			$('td.rooms .pull-left').removeClass('pull-left text-left').addClass('pull-right text-right');
		}

		// DeleteHandler:Disabled
        // Information anzeigen warum der Bewohner nicht gelöscht werden kann
        // Schlüssel vorhanden
        $('.content-inner').on("mousedown", ".btn-delete:disabled", function(e) {
            var errormsg = "Der Bewohner '" + $(this).attr("data-name") + "'' ist noch im Besitz von " + ($(this).attr("data-keys") > 1 ? $(this).attr("data-keys") + ' Schlüsseln' : 'einem Schlüssel') + ' und kann deswegen nicht gelöscht werden.';
            notifyError(errormsg);
        });

		// EditHandler
		$('.btn-edit').click(function(e) {
			e.preventDefault();
			transfer.residents.residentID = $(this).attr('data-residentid');
			routeProvider.navigateTo('editResidentsView', $(this).attr('data-residentid'));
		});

		// DeleteHandler
        // Anzeige des Modalfensters, wo noch bestätigt werden muss das dieser Bewohner gelöscht werden soll
        $(".btn-delete").click(function(e) {
        	e.preventDefault();
            notifyClose();
            $("#removeResident").text( $(this).attr("data-name") );
            if ( $(this).attr("data-rooms") <= 0) {
            	$("#removeRooms").hide();
            } else if ( $(this).attr("data-rooms") <= 1) {
 				$("#removeRooms").show().text('Der Raum des Bewohners wird automatisch freigegeben.');
            } else {
            	$("#removeRooms").show().text('Die ' + $(this).attr("data-rooms") + ' Räume des Bewohners werden automatisch freigegeben.');
            }
            $('#btnConfirmDelete').attr("data-residentid", $(this).attr("data-residentid") );
            $('.modal').modal('show');
        });

        // ConfirmDeleteHandler 
		// Bestätigen des Löschens eines Bewohners
		$('.modal').on('click', '#btnConfirmDelete', function(e) {
		    e.preventDefault();

		    socket.emit('deleteResident', $(this).attr("data-residentid"), function(result) {
		        if (result) {
		            // Löschen erfolgreich
		            $('.modal').modal('hide');
		            $('body').removeClass('modal-open');
		            $('.modal-backdrop').remove();
		            notifySuccess("Bewohner gelöscht ", $("#removeResident").text() + " wurde erfolgreich gelöscht");
		        } else {
		            // Löschen hat nicht funktioniert
		            $('.modal').modal('hide');
		            $('body').removeClass('modal-open');
		            $('.modal-backdrop').remove();
		            notifyError("Bewohner '" + $("#removeResident").text() + "'' konnte nicht gelöscht werden");
		        }

		        // Aktualisieren der Anzeige durch erneutes Anzeigen
		        routeProvider.navigateTo('residentsView');
		    });
		});

		if (uvRights.view === true) {
			$('.content-inner div.hidden').removeClass('hidden').html('<br><br><strong>Es wurde noch keine Einheiten hinzugefügt</strong><br><br>');
			$('#searchBox').addClass('hidden');
		} else if (data.residents.length <= 0) {
			$('.content-inner div.hidden').removeClass('hidden').html('<br><br><strong>Es wurden keine Bewohner gefunden für die Ausgabe</strong><br>' + (uvRights.residentAdd ? "Über die Schaltfläche 'Neue Bewohner hinzufügen' können diese angelegt werden<br>" : '') + '<br>');
			$('#searchBox').addClass('hidden');
		}

		// Unterkünfte eines Bewohners anzeigen
		$('#tableResidents').on('click', '.residentClick', function() {
			routeProvider.navigateTo('accomResidentView', $(this).attr('data-residentid'));
		});
	});

});

function buildResidents(resident, rooms, units) {
	var contentString = '';

	contentString =
	'<tr id="' + resident.residentID + '" data-search="' + buildSearch(resident, rooms, units) + '">' + 
		'<td class="col-xs-8 col-sm-4 col-md-4 names">' + 
			'<div class="residentHover residentClick" data-residentid="' + resident.residentID + '">' + 
			'<span class="fa fa-' + (resident.sex === 'male' ? 'male' : 'female') + '"></span>&nbsp;&nbsp;<span class="residentnotes" data-toggle="popover" data-html="true" data-trigger="hover" data-placement="auto bottom" data-content="' + (resident.notes === null || resident.notes === '' ? '' : resident.notes) + '"><strong>' + (resident.notes === null || resident.notes === '' ? resident.surname : '<u>' + resident.surname + '</u>') + '</strong>, ' + resident.forename + '</span> ' + 
			'<span class="label-box label-specialbreak">' + 
				'<span class="label label-additional label-ranks" data-toggle="popover" data-html="true" data-trigger="hover" data-placement="auto bottom" data-content="' + resident.ranklabel + '">' + resident.rankshort + '</span>' +
				'<span class="label label-additional label-residents" data-toggle="popover" data-html="true" data-trigger="hover" data-placement="auto bottom" data-content="' + (units[resident.unitID].order).replace(/\n/g,"<br>") + '" style="background-color: ' + shadeColor('#c06c1c', units[resident.unitID].level*15) + (units[resident.unitID].level > 4 ? ' color: #303030;' : '') + '"><span class="fa fa-cubes"></span>' + resident.unitshort + '</span>' + 
			'</span>' + 
			'</div>' + 
			'<span class="hidden-md hidden-lg residentroom-sm"><br>' + buildRooms(rooms) + 
			'</span>' +
		'</td>' + 
		'<td class="hidden-xs hidden-sm col-md-4 rooms">' + 
		'<span class="pull-left text-left">' + 
		buildRooms(rooms) + 
		'</span>' + 
		'</td>' +
		'<td class="col-xs-4 col-sm-4 col-md-4 actions">' + 
			'<button class="btn btn-sm btn-default btn-edit" data-unitid="' + resident.unitID + '" data-residentid="' + resident.residentID + '" role="button"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span><span class="btn-txt">Bearbeiten</span></button>' + 
		    '	<button class="btn btn-sm btn-danger btn-delete" data-unitid="' + resident.unitID + '" data-residentid="' + resident.residentID + '" data-rooms="' + rooms.length + '" data-name="' + resident.surname + ', ' + resident.forename + '" data-keys="' + resident.keys + '" ' + (resident.keys > 0 ? 'disabled' : '') + ' role="button"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span><span class="btn-txt">Löschen</span></button>';
		'</td>' + 
	'</tr>';

	return contentString;
}

function buildRooms(rooms) {
	var contentString = '';

	rooms.forEach(function(room) {
		contentString +=
		'<span class="label label-residentroom btn-rooms ' + (room.alias === '' && room.until === null ? 'cursordefault' : '')+ '" data-residentid="' + room.residentID + '" data-toggle="popover" data-html="true" data-trigger="hover" data-placement="auto bottom" ';

		if (room.alias !== '' || room.until !== '') {
			contentString +=
			'data-content="' + (room.alias === '' ? '' : '<strong>' + room.alias + '</strong><br>') + (room.until === null ? '' : 'vorauss. Belegung: <em>' + room.until + '</em><br>') + '"';
		}

		contentString +=
		'>' + 
		'Gebäude ' + room.buildingnumber + ', Raum ' + room.roomnumber + 
		'</span>';
	});

	return contentString;
}

function buildSearch(resident, rooms, units) {
	var searchString = 
		'°' + resident.ranklabel + '° ' +
		'°' + resident.rankshort + '° ' +
		'°' + resident.surname + '° ' +
		'°' + resident.forename + '° ' +
		units[resident.unitID].search + ' ';

	if (resident.notes !== null && resident.notes !== '') {
		searchString += resident.notes + ' ';
	}

	if (resident.sex === 'male') {
		searchString += '°m° °Mann° °Herr° °männlich° °male° ';
	} else {
		searchString += '°w° °Frau° °weiblich° °female° ';
	}

	if (rooms.length < 1) {
		searchString += '°keine Unterkunft° ';
	}

	rooms.forEach(function(room) {
		searchString += room.alias + ' °Gebäude ' + room.buildingnumber + '° °Raum ' + room.roomnumber + '° ';
	});

	return searchString;
}