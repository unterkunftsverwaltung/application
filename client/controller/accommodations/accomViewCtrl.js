/**
 * accomViewCtrl
 * Anzeigen der Gebäude und deren Belegung
 */

// Deklaration
var $ci 		= '.content-inner',
	$ta 		= '#tableAccom',

	units		= {},
	$data 		= null,
	today 		= Date.parse(new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate(), 0, 0, 0, 0));

// Anzeigen der Gebäude mit Räumen zzgl. der Bewohner
$(document).ready(function() {

	$($ci + ' [data-toggle="popover"]').popover({html: true});

	socket.emit('loadAccommodations', function(data) {

		if (uvRights.view === true) {
			$('.content-inner div.hidden').removeClass('hidden').html('<br><br><strong>Es wurde noch keine Einheiten hinzugefügt</strong><br><br>');
			$('#searchBox').addClass('hidden');
			return;
		} else if (data.buildings.length <= 0) {
			$('.content-inner div.hidden').removeClass('hidden').html('<br><br><strong>Es wurden keine Gebäude / Räume hinzugefügt</strong><br><br>');
			$('#searchBox').addClass('hidden');
			return;
		}

		// Einheiten ermitteln
		$.each(data.units, function(index, element) {
			units[element.unitID] = {
				id: element.unitID,
				name: element.name,
				short: element.short,
				order: '',
				search: '',
				member: element.member,
				level: -1
			}
		});

		// Struktur der Einheiten für die Darstellung einbinden
		$.each(units, function(index, element) {
			var next = units[element.id].id;
			do {
				if (element.order === '') {
					element.order = '<strong>' + units[next].name.replace(' ', '&nbsp;') + '</strong>';
					element.search = '°' + units[next].name + '° °' + units[next].short + '°';
				} else {
					element.order = units[next].name.replace(' ', '&nbsp;') + '\n' + element.order;
					element.search = units[next].name + ' ' + units[next].short + ' ' + element.search;
				}
				element.level += 1;
				next = units[next].member;
			} while (next > 0);
		});

		// Räume je Gebäude ermitteln
		data.buildingRooms = [];
		data.buildingMax = [];
		data.buildingUsed = [];
		data.rooms.forEach(function(element) {
			
			if (! data.buildingRooms[element.buildingID]) {
				data.buildingRooms[element.buildingID] = [];
				data.buildingMax[element.buildingID] = 0;
				data.buildingUsed[element.buildingID] = 0;
			}

			data.buildingRooms[element.buildingID].push(element);
			data.buildingMax[element.buildingID] += element.maxoccupancy;
			data.buildingUsed[element.buildingID] += element.residents;
		});

		// Bewohner je Raum ermitteln
		data.buildingResidents = [];
		data.residents.forEach(function(element) {
			
			if (! data.buildingResidents[element.roomID]) {
				data.buildingResidents[element.roomID] = [];
			}

			data.buildingResidents[element.roomID].push(element);
		});

		// Ermitteln welche Gebäude benötigt werden
		data.buildingList = [];
		data.buildings.forEach(function(building, index) {
			if (Array.isArray(data.buildingRooms[building.buildingID]) === true) {
				data.buildingList.push(building);
			}
		});

		// Hinweis wenn keine Unterkünfte vorhanden sind
		// gem. den aktuellen Benutzerrechten
		if (data.buildingList.length <= 0) {
			$('.content-inner div.hidden').removeClass('hidden').html('<br><br><strong>Es wurden keine Gebäude / Räume gefunden für die Ausgabe</strong><br><br>');
			$('#searchBox').addClass('hidden');
			return;
		}

		// Konfiguration für den datePicker
		$('#modalDate .input-group.date').datepicker({
		    clearBtn: true,
		    language: "de",
		    calendarWeeks: false,
		    todayHighlight: true,
		    startDate: new Date(+new Date() + 86400000),                // 86400000ms = 1 Tag später
		    endDate: new Date(+new Date() + 315576000000),              // 315576000000ms = 10 Jahre später
		    maxViewMode: 2
		});

		// Übertragen der Daten in ein HTML Format
		$data = data;
		$($ta).append(buildBuildings(data));

		$($ci + ' [data-toggle="popover"]').popover({html: true});

		// Bearbeiten und Löschen entfernen wenn die Berechtigungen nicht ausreichen
		var rightBook = (uvRights.roomBook + ',').split(','),
			rightClear = (uvRights.roomClear + ',').split(',');

		$('.btn-new, .btn-edit, .btn-delete').each(function() {
			var found = false;
			var unitBtn = $(this).attr('data-unitid');

			if ($(this).hasClass('btn-new') || $(this).hasClass('btn-edit')) {
				rightBook.forEach(function(unit) {
					if ( unitBtn === unit ) found = true; 	
				});	
			} else {
				rightClear.forEach(function(unit) {
					if ( unitBtn === unit ) found = true; 	
				});	
			}
			
			if (found === false) $(this).remove();
		});

		// Handler roomBook Bewohner zuweisen
		$($ci).on('click', '.btn-new', function(e) {
			e.preventDefault();
			transfer.roomBook.roomID = $(this).attr('data-roomid');
			routeProvider.navigateTo('roomBookAccom', $(this).attr('data-roomid'));
		});

		// Wechsel der Bewohner
		$($ta).on('click', '.residentClick', function() {
			routeProvider.navigateTo('accomResidentView', $(this).attr('data-residentid'));
		});

		// Ermitteln welche Bewohner betrachtet werden können
		var rightView = (uvRights.view + ',').split(',');
		$('.residentHover:not(.residentSelected)').each(function() {
			var found = false;
			var unitBtn = $(this).attr('data-unitid');

			rightView.forEach(function(unit) {
				if ( unitBtn === unit ) found = true; 	
			});	

			if (found === true) {
				$(this).addClass('residentClick');
			} else {
				$(this).addClass('residentNotAllowed');
			}
		});

	});	



});

function buildBuildings(data) {
	
	var contentString = '';

	data.buildingList.forEach(function(building) {
		contentString +=

		'<table id="building-' + building.buildingID + '" data-buildingid="' + building.buildingID + '"class="table table-hover simpletable">' + 
			'<thead>' + 
				'<tr>' + 
					'<th colspan="3">' +
						'<span class="pull-right label label-additional cursordefault label-residents' + (data.buildingUsed[building.buildingID] <= 0 && data.buildingMax[building.buildingID] > 0 ? 'free' : '') + (data.buildingUsed[building.buildingID] === data.buildingMax[building.buildingID] ? 'max' : '') + '">' + data.buildingUsed[building.buildingID] + ' / ' + data.buildingMax[building.buildingID] + '</span>' + 
						'<div class="theadBuilding" data-toggle="popover" data-html="true" data-trigger="hover" data-placement="auto bottom" data-content="' + optimalNotes(building.notes) + '">' +
						(building.alias ? '<small>' + building.alias + '</small><br>' : '<br>') +
						'Gebäude ' + building.number + 
						'</div>' +
					'</th>' + 
				'</tr>' + 
			'</thead>' + 
			'<tbody>' + 
				buildRooms(data, building, building.buildingID) + 
			'</tbody>' + 
		'</table>';

	});

    return contentString;
}

function buildRooms(data, building, buildingID) {
	var contentString = '';
	
	data.buildingRooms[buildingID].forEach(function(room) {
		contentString +=

		'<tr id="' + room.roomID + '" data-roomid="' + room.roomID + '" data-unitid="' + room.unitID + '" data-search="' + buildSearch(data, building, room) + '">' + 
			
			'<td class="hidden-xs col-sm-2 col-md-1">' + 
				'<span class="label label-residentroom btn-rooms ' + (room.notes.length <= 0 ? 'cursordefault' : '') + '" data-toggle="popover" data-html="true" data-trigger="hover" data-placement="auto bottom" data-content="' + optimalNotes(room.notes) + '">' + 
				'Raum <strong>' + room.number + '</strong> ' +
				'</span>' + 
				
			'</td>' + 

			'<td class="col-xs-10 col-sm-6 col-md-7">' +
				'<span class="label label-residentroom btn-rooms hidden-sm hidden-md hidden-lg ' + (room.notes.length <= 0 ? 'cursordefault' : '') + '" data-toggle="popover" data-html="true" data-trigger="hover" data-placement="auto bottom" data-content="' + optimalNotes(room.notes) + '">' + 
				'Raum <strong>' + room.number + '</strong> ' +
				'</span>' + 
				buildResidents(data, room.roomID) + 
			'</td>' +

			'<td class="col-xs-2 col-sm-4 col-md-4 actions actionBtn">' +
				'<div>' + 
				(room.residents >= room.maxoccupancy ? '' : '<button class="btn btn-sm btn-success btn-new" href="#" data-roomid="' + room.roomID + '" data-unitid="' + room.unitID + '" role="button"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span><span class="btn-txt">Zuweisen</span></button>') + 
				(room.residents <= 0 || room.maxoccupancy <= 0 ? '' : '<button class="btn btn-sm btn-default btn-edit" data-roomid="' + room.roomID + '" data-unitid="' + room.unitID + '" href="#" role="button"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span><span class="btn-txt">Anpassen</span></button>') +
                (room.residents <= 0 ? '' : '<button class="btn btn-sm btn-danger btn-delete" data-roomid="' + room.roomID + '" data-unitid="' + room.unitID + '" href="#" role="button"><span class="glyphicon glyphicon-export" aria-hidden="true"></span><span class="btn-txt">Freigeben</span></button>') +
                '</div>' + 
				'<span class="label label-additional label-top cursordefault label-residents' + (room.residents <= 0 && room.maxoccupancy > 0 ? 'free' : '') + (room.residents >= room.maxoccupancy ? 'max' : '') + '">' + room.residents + ' / ' + room.maxoccupancy + '</span> ' + 
			'</td>' +
		'</tr>';
	});

	return contentString;
}

function buildResidents(data, roomID) {
	var contentString = '';

	contentString += 
	'<table class="table table-hover simpletable tableresidents" id="room' + roomID + '">' + 
		'<tbody>';

	if (Array.isArray(data.buildingResidents[roomID]) === true) {
		data.buildingResidents[roomID].forEach(function(resident) {

			contentString += 
			'<tr>' + 
				'<td>' + 
					'<div class="residentHover" data-residentid="' + resident.residentID + '" data-name="' + resident.surname + ', ' + resident.forename + '" data-unitid="' + resident.unitID + '" data-rrid="' + resident.rrID + '">' + 
						'<span class="fa fa-' + (resident.sex === 'male' ? 'male' : 'female') + '"></span>&nbsp;&nbsp;<span class="residentnotes" data-toggle="popover" data-html="true" data-trigger="hover" data-placement="auto bottom" data-content="' + (resident.notes === null || resident.notes === '' ? '' : resident.notes) + '"><strong>' + (resident.notes === null || resident.notes === '' ? resident.surname : '<u>' + resident.surname + '</u>') + '</strong>, ' + resident.forename + '</span> ' + 
						'<span class="label-box label-specialbreak2">' + 					
							'<span class="label label-additional label-ranks" data-toggle="popover" data-html="true" data-trigger="hover" data-placement="auto bottom" data-content="' + resident.ranklabel + '">' + resident.rankshort + '</span>' +
							'<span class="label label-additional label-residents" data-toggle="popover" data-html="true" data-trigger="hover" data-placement="auto bottom" data-content="' + (units[resident.unitID].order).replace(/\n/g,"<br>") + '" style="background-color: ' + shadeColor('#c06c1c', units[resident.unitID].level*15) + (units[resident.unitID].level > 4 ? ' color: #303030;' : '') + '"><span class="fa fa-cubes"></span>' + resident.unitshort + '</span>' + 
							 residentUntil(resident.until) + 
						 '</span>' +
					'</div>' + 
				'</td>' +
			'</tr>';
		});	

	} else {
		contentString += '<tr><td>&nbsp;</td></tr>';
	}

	contentString += 
		'</tbody>' +
	'</table>';

	return contentString;
}

function buildSearch(data, building, room) {
	var searchString = 
		'°Gebäude ' + building.number + '° ' +
		'°' + building.alias + '° ' +
		'°Raum ' + room.number + '° ';

		if (building.notes !== null && building.notes !== '') {
			searchString += '°' + building.notes + '° ';
		}

		if (room.notes !== null && room.notes !== '') {
			searchString +='°' + room.notes + '° ';
		}

		if (Array.isArray(data.buildingResidents[room.roomID]) === true) {
			data.buildingResidents[room.roomID].forEach(function(resident) {
				searchString +=
					'°' + resident.ranklabel + '° ' +
					'°' + resident.rankshort + '° ' +
					'°' + resident.surname + '° ' +
					'°' + resident.forename + '° ' +
					units[resident.unitID].search + ' ';

					if (resident.notes !== null && resident.notes !== '') {
						searchString += resident.notes + ' ';
					}

					if (resident.sex === 'male') {
						searchString += '°m° °Mann° °Herr° °männlich° °male° ';
					} else {
						searchString += '°w° °Frau° °weiblich° °female° ';
					}

					searchString += '°belegt° ';
			});
		} else {
			searchString += '°frei° °freie° °leer° °leere° ';
		}

	return searchString;
}

function residentUntil(until) {
	if (until !== null) {

		var lbldate, icon, timeframe;

		var split = until.split('.'),
			bookedUntil = Date.parse(new Date(split[2], parseInt(split[1]) - 1, split[0], 0, 0, 0, 0) ),
			twoWeeksWarning = bookedUntil - 1209600000,
			timeframe = (today - bookedUntil - ((today - bookedUntil) % 86400000)) / 86400000;

		if (today > bookedUntil) {
			lbldate = 'over';
			icon = 'fa-calendar-times-o';
		} else if (today === bookedUntil) {
			lbldate = 'today';
			icon = 'fa-calendar';
		} else if (today >= twoWeeksWarning) {
			lbldate = 'warn';
			icon = 'fa-calendar-minus-o';
		} else {
			lbldate = '';
			icon = 'fa-calendar-check-o';
		}

		var contentString = 
		'<span class="label label-additional label-date' + lbldate + '" data-toggle="popover" data-html="true" data-trigger="hover" ' +
		'data-placement="auto bottom" data-content="voraussichtliche Belegung bis:<br><em>' + until + '</em>' + 
		(timeframe > 0 ? '<br><br>Überzogene Tage: ' + timeframe : '') + 
		(timeframe < 0 ? '<br><br>Verbleibende Tage: ' + timeframe*-1 : '') + 
		'"><span class="hiddentext-xs hiddentext-sm fa ' + icon + '"></span><span class="hidden-xs hidden-sm">' + until + '</span></span>';

		return contentString;
	} else {
		return '';
	}
}

/**
 * buildModal Befüllen eines Dialogfensters für die Funktionen Unterkunft freigeben und Datum anpassen
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param  {int} roomID Auswahl des Raumes über die roomID
 * @param  {string} target Dialogfenster welches angespriochen wird
 */
function buildModal(roomID, target) {
	$(target + ' .selectResident').hide();
	$(target + ' .oneResident').hide();
	$(target + ' .keys').hide();
	$(target + ' select').html('').selectpicker('destroy');

	var contentString = '',
	  	roomNumber = null,
	  	buildingNumber = null,
	  	start = 0;

	// Ermitteln der Raumnummer
	$data.rooms.forEach(function(room) {
		if (parseInt(room.roomID) === parseInt(roomID)) {
			roomNumber = room.number;
			buildingNumber = room.buildingID;
		}
	});

	// Ermittlen der Gebäudenummer
	$data.buildings.forEach(function(building) {
		if (parseInt(buildingNumber) === parseInt(building.buildingID)) {
			buildingNumber = building.number;
		}
	});
	
	$('#room' + roomID + '.tableresidents div[data-rrid]').each(function() {
		var actResident = $(this).attr('data-rrid');


		$data.buildingResidents[roomID].forEach(function(resident) {
			if (parseInt(resident.rrID) === parseInt(actResident)) {

				// Zusammenbauen der Optionen für den Selectpicker
				contentString += 
				'<option data-rrid="' + resident.rrID + '" data-keys="' + resident.keys + '" data-until="' + resident.until + '" data-name="' + resident.surname + ', ' + resident.forename + '" data-unterkunft="Gebäude ' + buildingNumber + ', Raum ' + roomNumber + '" data-content="' + 
				'<span style=\'width: 22px\' class=\'fa fa-' + (resident.sex === 'male' ? '' : 'fe') + 'male\'></span>' + 
				'<span class=\'text\'>' + resident.surname + ', ' + resident.forename + '</span>">' +
				resident.rrID + 
				'</option>';


				if (start <= 0) {
					$(target + ' .modalResident').text(resident.surname + ', ' + resident.forename);
					$(target + ' .modalRoom').text('Gebäude ' + buildingNumber + ', Raum ' + roomNumber);
					$(target + ' #btnConfirm').attr('data-rrid', resident.rrID).attr('data-keys', resident.keys).attr('data-name', resident.surname + ', ' + resident.forename).attr('data-unterkunft', 'Gebäude ' + buildingNumber + ', Raum ' + roomNumber);

					if ($('#room' + roomID + '.tableresidents div[data-rrid]').length <= 1) {
						$(target + ' .oneResident').show();
					} else {
						$(target + ' .selectResident').show();
					}

					if (target === '#modalDate') $('#modalDate .input-group.date').datepicker('setDate', resident.until);
					start += 1;
				}
			}
		});
	});
	$(target + ' select').append(contentString);
	$(target + ' select').selectpicker();

	// Datumsauswahl öffnen
	if (target === '#modalDate') {
		$('#modalDate .input-group.date input').removeClass('firstDate');

		setTimeout(function() {
			$('#modalDate .input-group.date').datepicker('show');
		}, 470);
	} else if (target === '#modalClear') { 
		if ($(target + ' #btnConfirm').attr('data-keys') >= 1) {
			$(target + ' #btnConfirm').prop('disabled', true);
			$(target + ' .keys').show();
		} else {
			$(target + ' #btnConfirm').prop('disabled', false);
			$(target + ' .keys').hide();
		}
	}

	$(target).modal('show');
}

// voraussichtliche Belegung bis
$($ci).on('click', '.btn-edit', function(e) {
	e.preventDefault();
    notifyClose();
    buildModal($(this).attr('data-roomid'), '#modalDate');
});

// Unterkunft freigeben
$($ci).on('click', '.btn-delete', function(e) {
	e.preventDefault();
    notifyClose();
    buildModal($(this).attr('data-roomid'), '#modalClear');
});

// Selectpicker Change
$('#modalClear select, #modalDate select').on('change', function(e) {
	var target = '#' + $(this).parentsUntil('div.modal').parent().attr('id');
	var selected = $(target + ' select option:selected');

	$(target + ' #btnConfirm').attr('data-rrid', selected.attr('data-rrid'));
	$(target + ' #btnConfirm').attr('data-name', selected.attr('data-name'));
	$(target + ' #btnConfirm').attr('data-keys', selected.attr('data-keys'));
	$(target + ' #btnConfirm').attr('data-unterkunft', selected.attr('data-unterkunft'));

	if (target === '#modalDate') {
		$('#modalDate .input-group.date input').removeClass('firstDate');
		$('#modalDate .input-group.date').datepicker('setDate', selected.attr('data-until'));
		$('#modalDate .input-group.date').datepicker('show');
	} else if (target === '#modalClear') {
		if (selected.attr('data-keys') >= 1) {
			$(target + ' #btnConfirm').prop('disabled', true);
			$(target + ' .keys').show();
		} else {
			$(target + ' #btnConfirm').prop('disabled', false);
			$(target + ' .keys').hide();
		}
	}
});

// Anzeige eines leeren Datumfeldes immer mit dem heutigem Datum ausfüllen
$('#modalDate').on('show', '.input-group.date', function() {
    if ( ($(this).children('input').val().trim() === '') && ($(this).children('input').hasClass('firstDate') === false) ) {
        $(this).datepicker('update', new Date(+new Date() + 86400000));
        $(this).datepicker('update');
        $(this).children('input').addClass('firstDate');
    } 
});

// Eintragen / Entfernen des neuen Belegungsdatums
// Anweisung an den Server schicken
$('#modalDate').on('click', '#btnConfirm', function(e) {
    e.preventDefault();
    var name = $(this).attr('data-name'),
    	unterkunftDate = $(this).attr('data-unterkunft'),
    	dateArray   = $('#modalDate .input-group.date input').val().split('.'),  
		date       	= new Date(dateArray[2] + "-" + dateArray[1] + "-" + dateArray[0]);

	if (date instanceof Date && !isNaN(date.valueOf())) {
		// Richtiges Datum vorhanden
	} else {
		// Kein Datum vorhanden
		date = '';
	}

    socket.emit('residentUntil', $(this).attr("data-rrid"), date, function(result) {
        if (result) {
            // Löschen erfolgreich
            $('#modalDate').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            notifySuccess("Belegungsdatum für ", name + " im " + unterkunftDate + " wurde erfolgreich geändert");
        } else {
            // Löschen hat nicht funktioniert
            $('#modalDate').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            notifyError("Belegungsdatum für '" + name + " im " + unterkunftDate + " konnte nicht geändert werden");
        }

        // Aktualisieren der Anzeige durch erneutes Anzeigen
        routeProvider.navigateTo('accomView');
    });
});

// Unterkunft freigeben
// Anweisung an der Server schicken
$('#modalClear').on('click', '#btnConfirm', function(e) {
    e.preventDefault();
    var name = $(this).attr('data-name'),
    	unterkunftClear = $(this).attr('data-unterkunft');

    socket.emit('removeResidentRoom', $(this).attr("data-rrid"), function(result) {
        if (result) {
            // Löschen erfolgreich
            $('#modalRemove').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            notifySuccess("Unterkunft freigegeben ", "Die Unterkunft im " + unterkunftClear + " vom Bewohner " + name + " wurde erfolgreich freigegeben");
        } else {
            // Löschen hat nicht funktioniert
            $('#modalRemove').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            notifyError("Unterkunft vom Bewohner '" + unterkunftClear + " im " + name + " konnte nicht freigegeben werden");
        }

        // Aktualisieren der Anzeige durch erneutes Anzeigen
        routeProvider.navigateTo('accomView');
    });
});