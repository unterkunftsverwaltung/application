/**
 * accomViewCtrl
 * Anzeigen der Unterkünfte eines Bewohners
 *
 * @author Rafal Welk
 */

// Deklaration
var $ci 			= '.content-inner',
	$rr 			= '#residentRooms',

	$addBtn			= '#addBtn',
	$backBtn		= '#btn-back',
	
	$data 			= null,
	units			= {},
	residentID 		= transfer.accommodations.residentID || actURL[4],
	today 			= Date.parse(new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate(), 0, 0, 0, 0));

$(document).ready(function() {

	socket.emit('loadAccommodationsResident', residentID, function(data) {

		// Sicherstellen das eine gültige ID übergeben werden
		if (data.resident.length <= 0) {
			notifyError('Dieser Bewohner existiert nicht');
			routeProvider.navigateBack('accomView');
			return
		}

		// Überprüfen ob der Benutzer diesen Bewohner sehen darf
		// Ansonsten Hinzufügen des Bewohnernamens in die Überschrift
        var rightView = (uvRights.view + ',').split(',');
        var found = false;
        
        rightView.forEach(function(unit) {
        	if (unit.toString() === (data.resident[0].unitID).toString() ) found = true;
        });

        if (found === false) {
        	notifyError('Sie besitzten nicht die benötigten Rechte um diese Funktion zu nutzen');
            routeProvider.navigateBack('accomView');
            return;
        } else {
        	$('h2.page_title').html($('h2.page_title').text() + '&nbsp;<span class="fa fa-' + (data.resident[0].sex === 'male' ? 'male' : 'female') + '"></span>&nbsp;' + data.resident[0].surname + ', ' + data.resident[0].forename);	
        	$data = data;
        }

        // Zurück zur Übersicht
	    $($backBtn).click(function(e) {
	        e.preventDefault();
	        routeProvider.navigateBack('accomView');
	    });

	     // AddHandler
		if (uvRights.roomBook) {
			$($addBtn).click(function(e) {
				e.preventDefault();
				routeProvider.navigateTo('roomBookResident', residentID);
			}); $($addBtn).removeClass('hidden');
		} else {
			$($addBtn).remove();
		}

		// Einheiten ermitteln
		$.each(data.units, function(index, element) {
			units[element.unitID] = {
				id: element.unitID,
				name: element.name,
				short: element.short,
				order: '',
				search: '',
				member: element.member,
				level: -1
			}
		});

		// Struktur der Einheiten für die Darstellung einbinden
		$.each(units, function(index, element) {
			var next = units[element.id].id;
			do {
				if (element.order === '') {
					element.order = '<strong>' + units[next].name.replace(' ', '&nbsp;') + '</strong>';
				} else {
					element.order = units[next].name.replace(' ', '&nbsp;') + '\n' + element.order;
				}
				element.level += 1;
				next = units[next].member;
			} while (next > 0);
		});

		if (data.residents.length <= 0) {
			$($ci + ' .hidden').removeClass('hidden').html('<br><strong>Dieser Bewohner besitzt keine zugewiesenen Unterkünfte</strong><br><br>' + (uvRights.roomBook ? 'Über die Schaltfläche Unterkunft zuweisen können Sie nach freien Unterkünften suchen<br><br>' : '') );
		}

		// Ermitteln der Gebäude und Räume
		data.buildingList  = [];
		data.buildingRooms = [];
		data.buildingOrder = [];
		data.residents.forEach(function(element) {

			if (! data.buildingRooms[element.roomID]) {
				data.buildingRooms[element.roomID] = null;
				data.buildingOrder.push(element.roomID);
			}
			
			var room = {
				buildingID: 	element.buildingID,
				buildingnumber: element.buildingnumber,
				buildingalias: 	element.buildingalias,
				buildingnotes: 	element.buildingnotes,

				roomID: 		element.roomID, 
				roomnumber: 	element.roomnumber,
				roomnotes: 		element.roomnotes,
				roomunit:		element.roomunit,

				actual: 		0,
				max: 			element.maxoccupancy,
				residents: 		[]
			}

			data.buildingRooms[element.roomID] = room;
		});

		// Ermitteln der Bewohner und den Räumen zuordnen
		data.residents.forEach(function(element) {
			var resident = {
				residentID: 	element.residentID, 
				forename: 		element.forename, 
				surname: 		element.surname, 
				sex: 			element.sex, 
				notes: 			element.residentnotes, 

				rankID: 		element.rankID,
				ranklabel: 		element.ranklabel,
				rankshort: 		element.rankshort,

				unitID: 		element.residentunit,
				unitname: 		element.unitname,
				unitshort: 		element.unitshort,
				
				until: 			element.until,
				rrID: 			element.rrID,
				keys: 			element.keys
			}

			if (resident.residentID == data.resident[0].residentID) resident.selected = true;

			data.buildingRooms[element.roomID].actual += 1;
			data.buildingRooms[element.roomID].residents.push(resident);
		});

		// Reihenfolge des Aufbaus festlegen
		data.buildingOrder.forEach(function(element) {
			data.buildingList.push(data.buildingRooms[element]);
		});

		// Konfiguration für den datePicker
		$('#modalDate .input-group.date').datepicker({
		    clearBtn: true,
		    language: "de",
		    calendarWeeks: false,
		    todayHighlight: true,
		    startDate: new Date(+new Date() + 86400000),                // 86400000ms = 1 Tag später
		    endDate: new Date(+new Date() + 315576000000),              // 315576000000ms = 10 Jahre später
		    maxViewMode: 2
		});

		// Auf der Räume mit den Bewohnern
		$($rr).append(buildRooms(data));
		$($ci + ' [data-toggle="popover"]').popover({html: true});

		// Entfernen von Schaltfläche auf denen der Benutzer keine Berechtigungen besitzt
		var rightBook = (uvRights.roomBook + ',').split(','),
		 	rightClear = (uvRights.roomClear + ',').split(',');

		$('.btn-edit, .btn-delete').each(function() {
			var found = false;
			var unitBtn = $(this).attr('data-roomunit');

			if ($(this).hasClass('btn-edit')) {
				rightBook.forEach(function(unit) {
					if ( unitBtn === unit ) found = true; 	
				});	
			} else {
				rightClear.forEach(function(unit) {
					if ( unitBtn === unit ) found = true; 	
				});
			}
			
			if (found === false) $(this).remove();
		});

		// Wechsel der Bewohner
		$('#residentRooms').on('click', '.residentClick', function() {
			routeProvider.navigateTo('accomResidentView', $(this).attr('data-residentid'));
		});

		// Ermitteln welche Bewohner betrachtet werden können
		var rightView = (uvRights.view + ',').split(',');
		$('.residentHover:not(.residentSelected)').each(function() {
			var found = false;
			var unitBtn = $(this).attr('data-unitid');

			rightView.forEach(function(unit) {
				if ( unitBtn === unit ) found = true; 	
			});	

			if (found === true) {
				$(this).addClass('residentClick');
			}
		});
		

	});
});

/**
 * buildRooms Erzeugen der Übersicht der einzelnen Räume und innerhalb dieser aller Bewohner
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param {obj} data vorbereitete Liste mit den gesamten Räumen und Bewohnern
 * @return {string} Rückgabe der formatierten Tabelle
 */
function buildRooms(data) {
	var contentString = '';
	
	data.buildingList.forEach(function(room) {
		contentString +=

		'<tr>' + 
			
			'<td class="hidden-xs col-sm-2 col-md-2">' + 
				
				'<span class="label label-residentroom btn-rooms ' + (room.buildingnotes.length <= 0 ? 'cursordefault' : '') + '" data-toggle="popover" data-html="true" data-trigger="hover" data-placement="auto bottom" data-content="' + optimalNotes(room.buildingnotes) + '">' + 
				'Gebäude <strong>' + room.buildingnumber + '</strong> ' +
				'</span>' + 

				'<span class="label label-residentroom btn-rooms ' + (room.roomnotes.length <= 0 ? 'cursordefault' : '') + '" data-toggle="popover" data-html="true" data-trigger="hover" data-placement="auto bottom" data-content="' + optimalNotes(room.roomnotes) + '">' + 
				'Raum <strong>' + room.roomnumber + '</strong> ' +
				'</span>' + 
				
			'</td>' + 

			'<td class="col-xs-10 col-sm-6 col-md-6">' +
				'<span class="label label-residentroom btn-rooms hidden-sm hidden-md hidden-lg ' + (room.buildingnotes.length <= 0 ? 'cursordefault' : '') + '" data-toggle="popover" data-html="true" data-trigger="hover" data-placement="auto bottom" data-content="' + optimalNotes(room.buildingnotes) + '">' + 
				'Gebäude <strong>' + room.buildingnumber + '</strong> ' +
				'</span>' + 

				'<span class="label label-residentroom btn-rooms hidden-sm hidden-md hidden-lg ' + (room.roomnotes.length <= 0 ? 'cursordefault' : '') + '" data-toggle="popover" data-html="true" data-trigger="hover" data-placement="auto bottom" data-content="' + optimalNotes(room.roomnotes) + '">' + 
				'Raum <strong>' + room.roomnumber + '</strong> ' +
				'</span>' + 
				buildResidents(room.residents, room.roomID, room.roomunit) + 
			'</td>' +

			'<td class="col-xs-2 col-sm-4 col-md-4 actions actionBtn">' +
				'<div>' +
				'<button class="btn btn-sm btn-default btn-edit" data-roomid="' + room.roomID + '" data-roomunit="' + room.roomunit + '" href="#" role="button"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span><span class="btn-txt">Anpassen</span></button>' +
                '<button class="btn btn-sm btn-danger btn-delete" data-roomid="' + room.roomID + '" data-roomunit="' + room.roomunit + '" href="#" role="button"><span class="glyphicon glyphicon-export" aria-hidden="true"></span><span class="btn-txt">Freigeben</span></button>' +
                '</div>' + 
				'<span class="label label-additional label-top pull-right cursordefault label-residents' + (room.actual <= 0 && room.max > 0 ? 'free' : '') + (room.actual >= room.max ? 'max' : '') + '">' + room.actual + ' / ' + room.max + '</span> ' + 
			'</td>' +
		'</tr>';
	});

	return contentString;
}

/**
 * buildResidents Bewohner eines Raumes optisch hervorheben
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param  {array} residents Bewohner des aktuellen Raumes
 * @param {int} roomID Raumnummer die Eindeutig ist
 * @param {int} roomunit Raum gehört dieser Einheit (roomBook, roomClear)
 * @return {string} HTML Tabelle mit den Bewohnern
 */
function buildResidents(residents, roomID, roomunit) {
	var contentString = '';

	contentString += 
	'<table class="table table-hover simpletable tableresidents" id="' + roomID + '" data-roomid="' + roomID + '" data-roomunit="' + roomunit + '">' + 
		'<tbody>';

	residents.forEach(function(resident) {

		contentString += 
		'<tr>' + 
			'<td>' + 
				'<div class="residentHover' + (resident.selected ? ' residentSelected' : '') + '" data-residentid="' + resident.residentID + '" data-name="' + resident.surname + ', ' + resident.forename + '" data-unitid="' + resident.unitID + '" data-rrid="' + resident.rrID + '">' + 
					'<span class="fa fa-' + (resident.sex === 'male' ? 'male' : 'female') + '"></span>&nbsp;&nbsp;' +
					'<span class="residentnotes" data-toggle="popover" data-html="true" data-trigger="hover" data-placement="auto bottom" data-content="' + (resident.notes === null || resident.notes === '' ? '' : resident.notes) + '"><strong>' + 
					 (resident.notes === null || resident.notes === '' ? resident.surname : '<u>' + resident.surname + '</u>') + '</strong>, ' + resident.forename + '</span> ' + 
					
					'<span class="label-box label-specialbreak2">' + 					
						'<span class="label label-additional label-ranks" data-toggle="popover" data-html="true" data-trigger="hover" data-placement="auto bottom" data-content="' + resident.ranklabel + '">' + resident.rankshort + '</span>' +
						'<span class="label label-additional label-residents" data-toggle="popover" data-html="true" data-trigger="hover" data-placement="auto bottom" data-content="' + (units[resident.unitID].order).replace(/\n/g,"<br>") + '" ' + 
						'style="background-color: ' + shadeColor('#c06c1c', units[resident.unitID].level*15) + (units[resident.unitID].level > 4 ? ' color: #303030;' : '') + '"><span class="fa fa-cubes"></span>' + resident.unitshort + '</span>' + 
						 residentUntil(resident.until) + 
					'</span>' +

				'</div>' + 
			'</td>' +
		'</tr>';
	});	

	contentString += 
		'</tbody>' +
	'</table>';

	return contentString;
}

/**
 * residentUntil Ermittlen ob Bewohner das Datum überschritten haben oder kurz davor sind
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param  {string} until Übergabe des Datums im Format tt.mm.yyyyy
 * @return {string} Ausgabe der HTML Darstellung mit dem passenden Icons und einem Popover mit den verbleibenden Tagen
 */
function residentUntil(until) {
	if (until !== null) {

		var lbldate, icon, timeframe;

		var split = until.split('.'),
			bookedUntil = Date.parse(new Date(split[2], parseInt(split[1]) - 1, split[0], 0, 0, 0, 0) ),
			twoWeeksWarning = bookedUntil - 1209600000,
			timeframe = (today - bookedUntil - ((today - bookedUntil) % 86400000)) / 86400000;

		if (today > bookedUntil) {
			lbldate = 'over';
			icon = 'fa-calendar-times-o';
		} else if (today === bookedUntil) {
			lbldate = 'today';
			icon = 'fa-calendar';
		} else if (today >= twoWeeksWarning) {
			lbldate = 'warn';
			icon = 'fa-calendar-minus-o';
		} else {
			lbldate = '';
			icon = 'fa-calendar-check-o';
		}

		var contentString = 
		'<span class="label label-additional label-date' + lbldate + '" data-toggle="popover" data-html="true" data-trigger="hover" ' +
		'data-placement="auto bottom" data-content="voraussichtliche Belegung bis:<br><em>' + until + '</em>' + 
		(timeframe > 0 ? '<br><br>Überzogene Tage: ' + timeframe : '') + 
		(timeframe < 0 ? '<br><br>Verbleibende Tage: ' + timeframe*-1 : '') + 
		'"><span class="hiddentext-xs hiddentext-sm fa ' + icon + '"></span><span class="hidden-xs hidden-sm">' + until + '</span></span>';

		return contentString;
	} else {
		return '';
	}
}

/**
 * buildModal Befüllen eines Dialogfensters für die Funktionen Unterkunft freigeben und Datum anpassen
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param  {int} roomID Auswahl des Raumes über die roomID
 * @param  {string} target Dialogfenster welches angespriochen wird
 */
function buildModal(roomID, target) {
	$(target + ' .selectResident').hide();
	$(target + ' .oneResident').hide();
	$(target + ' .keys').hide();
	$(target + ' select').html('').selectpicker('destroy');

		var contentString = '';
		
		$('#' + roomID + '.tableresidents div[data-rrid]').each(function() {
			var actResident = $(this).attr('data-rrid');

			$data.residents.forEach(function(resident) {
				if (parseInt(resident.rrID) === parseInt(actResident)) {

					// Zusammenbauen der Optionen für den Selectpicker
					contentString += 
					'<option data-rrid="' + resident.rrID + '" data-keys="' + resident.keys + '" data-until="' + resident.until + '" data-name="' + resident.surname + ', ' + resident.forename + '" data-unterkunft="Gebäude ' + resident.buildingnumber + ', Raum ' + resident.roomnumber + '" ' + (parseInt(resident.residentID) === parseInt(residentID) ? 'selected' : '') + ' data-content="' + 
					'<span style=\'width: 22px\' class=\'fa fa-' + (resident.sex === 'male' ? '' : 'fe') + 'male\'></span>' + 
					'<span class=\'text\'>' + resident.surname + ', ' + resident.forename + '</span>">' +
					resident.rrID + 
					'</option>';

					if (parseInt(resident.residentID) === parseInt(residentID)) {
						$(target + ' .modalResident').text(resident.surname + ', ' + resident.forename);
						$(target + ' .modalRoom').text('Gebäude ' + resident.buildingnumber + ', Raum ' + resident.roomnumber);
						$(target + ' #btnConfirm').attr('data-rrid', resident.rrID).attr('data-keys', resident.keys).attr('data-name', resident.surname + ', ' + resident.forename).attr('data-unterkunft', 'Gebäude ' + resident.buildingnumber + ', Raum ' + resident.roomnumber);

						if ($('#' + roomID + '.tableresidents div[data-rrid]').length <= 1) {
							$(target + ' .oneResident').show();
						} else {
							$(target + ' .selectResident').show();
						}

						if (target === '#modalDate') $('#modalDate .input-group.date').datepicker('setDate', resident.until);
					}
				}
			});
		});
		$(target + ' select').append(contentString);
		$(target + ' select').selectpicker();

		// Datumsauswahl öffnen
		if (target === '#modalDate') {
			$('#modalDate .input-group.date input').removeClass('firstDate');

			setTimeout(function() {
				$('#modalDate .input-group.date').datepicker('show');
			}, 470);
		} else if (target === '#modalClear') { 
			if ($(target + ' #btnConfirm').attr('data-keys') >= 1) {
				$(target + ' #btnConfirm').prop('disabled', true);
				$(target + ' .keys').show();
			} else {
				$(target + ' #btnConfirm').prop('disabled', false);
				$(target + ' .keys').hide();
			}
		}

	$(target).modal('show');
}

// voraussichtliche Belegung bis
$($rr).on('click', '.btn-edit', function(e) {
	e.preventDefault();
    notifyClose();
    buildModal($(this).attr('data-roomid'), '#modalDate');
});

// Unterkunft freigeben
$($rr).on('click', '.btn-delete', function(e) {
	e.preventDefault();
    notifyClose();
    buildModal($(this).attr('data-roomid'), '#modalClear');
});

// Selectpicker Change
$('#modalClear select, #modalDate select').on('change', function(e) {
	var target = '#' + $(this).parentsUntil('div.modal').parent().attr('id');
	var selected = $(target + ' select option:selected');

	$(target + ' #btnConfirm').attr('data-rrid', selected.attr('data-rrid'));
	$(target + ' #btnConfirm').attr('data-name', selected.attr('data-name'));
	$(target + ' #btnConfirm').attr('data-keys', selected.attr('data-keys'));
	$(target + ' #btnConfirm').attr('data-unterkunft', selected.attr('data-unterkunft'));

	if (target === '#modalDate') {
		$('#modalDate .input-group.date input').removeClass('firstDate');
		$('#modalDate .input-group.date').datepicker('setDate', selected.attr('data-until'));
		$('#modalDate .input-group.date').datepicker('show');
	} else if (target === '#modalClear') {
		if (selected.attr('data-keys') >= 1) {
			$(target + ' #btnConfirm').prop('disabled', true);
			$(target + ' .keys').show();
		} else {
			$(target + ' #btnConfirm').prop('disabled', false);
			$(target + ' .keys').hide();
		}
	}
});

// Anzeige eines leeren Datumfeldes immer mit dem heutigem Datum ausfüllen
$('#modalDate').on('show', '.input-group.date', function() {
    if ( ($(this).children('input').val().trim() === '') && ($(this).children('input').hasClass('firstDate') === false) ) {
        $(this).datepicker('update', new Date(+new Date() + 86400000));
        $(this).datepicker('update');
        $(this).children('input').addClass('firstDate');
    } 
});

// Eintragen / Entfernen des neuen Belegungsdatums
// Anweisung an den Server schicken
$('#modalDate').on('click', '#btnConfirm', function(e) {
    e.preventDefault();
    var name = $(this).attr('data-name'),
    	unterkunftDate = $(this).attr('data-unterkunft'),
    	dateArray   = $('#modalDate .input-group.date input').val().split('.'),  
		date       	= new Date(dateArray[2] + "-" + dateArray[1] + "-" + dateArray[0]);

	if (date instanceof Date && !isNaN(date.valueOf())) {
		// Richtiges Datum vorhanden
	} else {
		// Kein Datum vorhanden
		date = '';
	}

    socket.emit('residentUntil', $(this).attr("data-rrid"), date, function(result) {
        if (result) {
            // Löschen erfolgreich
            $('#modalDate').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            notifySuccess("Belegungsdatum für ", name + " im " + unterkunftDate + " wurde erfolgreich geändert");
        } else {
            // Löschen hat nicht funktioniert
            $('#modalDate').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            notifyError("Belegungsdatum für '" + name + " im " + unterkunftDate + " konnte nicht geändert werden");
        }

        // Aktualisieren der Anzeige durch erneutes Anzeigen
        routeProvider.navigateTo('accomResidentView', residentID);
    });
});

// Unterkunft freigeben
// Anweisung an der Server schicken
$('#modalClear').on('click', '#btnConfirm', function(e) {
    e.preventDefault();
    var name = $(this).attr('data-name'),
    	unterkunftClear = $(this).attr('data-unterkunft');

    socket.emit('removeResidentRoom', $(this).attr("data-rrid"), function(result) {
        if (result) {
            // Löschen erfolgreich
            $('#modalRemove').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            notifySuccess("Unterkunft freigegeben ", "Die Unterkunft im " + unterkunftClear + " vom Bewohner " + name + " wurde erfolgreich freigegeben");
        } else {
            // Löschen hat nicht funktioniert
            $('#modalRemove').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            notifyError("Unterkunft vom Bewohner '" + unterkunftClear + " im " + name + " konnte nicht freigegeben werden");
        }

        // Aktualisieren der Anzeige durch erneutes Anzeigen
        routeProvider.navigateTo('accomResidentView', residentID);
    });
});