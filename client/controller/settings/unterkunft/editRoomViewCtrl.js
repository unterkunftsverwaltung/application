/* 
 *  Settings -> editRoomViewCtrl
 */

/* global socket, routeProvider, actURL */

var $backBtn = $('#btn-back'),
        $editBtn = $('#editBtn'),
        $roomNumber = $('#roomNumber'),
        $roomOccu = $('#roomOccu'),
        $roomNotes = $('#roomNotes'),
        $resetBtn = $('#resetBtn'),
        $unittree = $('#tree'),
        completeData,
        completeBuilding,
        unitID = null,
        unitNodeID = null,
        oldRoomID = transfer.room.editRoom || actURL[4];


// Backbutton
$backBtn.click(function(e) {
    e.preventDefault();
    routeProvider.navigateTo('unterkunftView');
});

// Einheiten für den TreeView laden
// SOCKET EMIT loadUnits
socket.emit('loadUnits', false, '', function(units) {
    $unittree.treeview({
        data: units,
        levels: 2, 
        color: '#555',
        backColor: '#fff',
        showTags: true,
        selectedBackColor: 'rgb(250,137,0)',
        onNodeSelected: function(event, data) {
            unitID = data.id;
            checkChangeRoom();
        }, 
        onNodeUnselected: function(event, data) {
            unitID = null;
            checkChangeRoom();
        }
    });
});

// Laden des Raumes und befüllen der Eingabefelder
socket.emit('loadRoom', oldRoomID, function(room) {
    if (room.buildingID === null) {
        notifyError('Dieses Gebäude existiert nicht');
        routeProvider.navigateTo('unterkunftView');
        return;
    }

    completeData = room;
    $roomNumber.val(room.number);
    $roomOccu.val(room.maxoccupancy);
    $roomNotes.val(room.notes);
    unitID = room.unitID;

    // Wenn Bewohner sich auf der Unterkunft befinden, dies anzeigen
    $roomOccu.attr('min', room.residents);
    if (room.residents === 1) {
        $('.residents').text(' einem Bewohner ');
    } else if (room.residents > 1) {
        $('.residents').text(room.residents + ' Bewohnern ');
    }
    if (room.residents > 0) $('.resident').removeClass('hidden');

    // Auswahl der Einheit im TreeView
    var tree = $unittree.treeview('getEnabled', 0);

    Object.keys(tree).forEach(function(key) {
        if (tree[key].id == unitID) {
            unitNodeID = tree[key].nodeId;
        }
    });
    
    // Aktuelle Einheit auswählen
    $unittree.treeview('selectNode', [ unitNodeID, { silent: true } ]);

    // Alle übergeordneten Einheiten öffnen (Parents)
    $unittree.treeview('revealNode', [ unitNodeID, { silent: true } ]);

    autolabelSet();
});

// Bei Eingabe prüfen ob es Änderungen gibt
$('input, textarea').change(function() {
    checkChangeRoom();
});

$('input, textarea').keyup(function() {
    checkChangeRoom();
});

// Zurücksetzen-Button
$resetBtn.click(function(e) {
    e.preventDefault();
    $roomNumber.val(completeData.number);
    $roomOccu.val(completeData.maxoccupancy);
    $roomNotes.val(completeData.notes);
    unitID = completeData.unitID;
    $unittree.treeview('selectNode', [ unitNodeID, { silent: true } ]);
    $unittree.treeview('revealNode', [ unitNodeID, { silent: true } ]);
    $(".has-error").removeClass("has-error");
    $resetBtn.prop('disabled', true);
    autolabelSet();
    setTimeout(function() {
        $roomNumber.focus();
    });
});

// Speichern Button
$editBtn.click(function(e) {
   e.preventDefault();
   
   if (!checkChangeRoom()) {
        notifyWarning("Information", "Es wurden keine Änderungen durchgeführt");
        return;
   } else if (checkEntryRoom()) {
        return;
   } else {
        // neue Daten
       var newRoom = {
            roomID: completeData.roomID,
            number: $roomNumber.val().trim(),
            maxoccupancy: parseInt($roomOccu.val().trim()),
            notes: $roomNotes.val().trim(),
            buildingID: completeData.buildingID,
            unitID: unitID,
            useID: 1    // Da noch keine Nutzungsarten angelegt werden können, ist immer die 1 (Unterkunft) festgelegt
        };
        
        // Laden des Geäudes in dem sich der Raum befinden, um die notifyErrorMsg zu vervollständigen
        socket.emit('loadBuilding', newRoom.buildingID, function(building) {
            completeBuilding = building;
        });
        
        // Prüfen ob es die möglicherweise geänderte Raumnummer schon im Gebäude existiert
        socket.emit('loadRoomsByBuildingID', newRoom.buildingID, function(rooms) {
            var exists = false;
            $.each(rooms, function(index, room) {
               if(room.number === newRoom.number) {
                    if (room.roomID !== newRoom.roomID) {
                        notifyError('Es existiert bereits ein Raum mit dieser Nummer in Gebäude ' + completeBuilding.number + (completeBuilding.alias !== '' ? " (" + completeBuilding.alias + ")" : ''));
                        $roomNumber.parent().addClass("has-error");
                        exists = true;
                    }
               }   
            });
            
            if(!exists) {
                socket.emit('editRoom', newRoom, function(result) {
                    if (result) {
                        notifySuccess("Raum geändert ", "Raum " + newRoom.number +" wurde erfolgreich geändert");
                        routeProvider.navigateTo('unterkunftView');
                    }
                });
            }
        });
   }
   
});

// Nur Zahleneingabe bei maximaler Belegungsanzahl
// Automatisch entfernen aller anderen Zeichen und 
// begrenzen des Zahlenbereichs auf 0 bis 50
$roomOccu.keyup(function() {

    var value = $(this).val().replace(/[^0-9]/g, '');

    if (value != '') {
        if (value < parseInt(completeData.residents)) value = completeData.residents;
        if (value > 50) value = '50';
    }

    $(this).val(value);
});

function checkChangeRoom() {
    var change = false;
    
    if (   completeData.number                  !== $roomNumber.val().trim()
        || completeData.maxoccupancy.toString() !== $roomOccu.val().trim()
        || completeData.notes                   !== $roomNotes.val().trim()
        || completeData.unitID                  !== unitID)
        change = true;
    
    if (change === true) {
        $resetBtn.prop('disabled', false);
    } else {
        $resetBtn.prop('disabled', true);
    }
    return change;
}

// Prüft Eingabefelder auf leere Einträge
function checkEntryRoom() {
    var error = false;
    
    $(".has-error").removeClass("has-error");

    if (removeHTML()) { return true; }
    
    if ($roomNumber.val().trim() === '') {
        error = true;
        $roomNumber.parent().addClass("has-error");
    }

    if ($roomOccu.val().trim().replace(/[^0-9]/g, '') === '') {
        error = true;
        $roomOccu.parent().addClass("has-error");
    } else if ($roomOccu.val() < completeData.residents || $roomOccu.val() > 50) {
        error = true;
        $roomOccu.parent().addClass("has-error");
    }

    if (unitID === null) {
        error = true;
        $unittree.addClass('has-error');
        $unittree.prev('label').addClass('has-error');
    }
    
    if (error) {
        notifyError('Bitte die rot markierten Felder ausfüllen');
    }

    return error;
}