/* 
 *  Settings -> addRoomViewCtrl
 */


/* global socket, routeProvider, actURL, transfer */

var $backBtn = $('#btn-back'),
    $addBtn = $('#addBtn'),
    $roomNumber = $('#roomNumber'),
    $roomOccu = $('#roomOccu'),
    $roomNotes = $('#roomNotes'),
    $clearBtn = $('#clearBtn'),
    $unittree = $('#tree'),
    buildingNumber = transfer.room.addRoom || actURL[4],
    unitID = null;

// Sicherstellen das die Gebäudenummer übergeben wurde
if (buildingNumber === undefined) {
    notifyError('Gebäudenummer wurde nicht übergeben');
    routeProvider.navigateTo('unterkunftView');
}

// Backbutton
$backBtn.click(function(e) {
    e.preventDefault();
    transfer.room.selectedNode = null;
    routeProvider.navigateTo('unterkunftView');
});

$('#sidebar-wrapper a').click(function() {
    transfer.room.selectedNode = null;
});

// Eingabefelder leeren
$clearBtn.on('click', function(e) {
    e.preventDefault();
    $roomNumber.val('');
    $roomOccu.val('');
    $roomNotes.val('');
    
    var selectedNode = $unittree.treeview('getSelected', 0);
    $unittree.treeview('unselectNode', [ selectedNode, { silent: true } ]);
    unitID = null;
    $(".has-error").removeClass("has-error");
    autolabelClean();
    setTimeout(function() {
        $roomNumber.focus();
    }, 300);
});

// Einheiten für den TreeView laden
// SOCKET EMIT loadUnits
socket.emit('loadUnits', false, '', function(units) {
    $unittree.treeview({
        data: units,
        levels: 2, 
        color: '#555',
        backColor: '#fff',
        showTags: true,
        selectedBackColor: 'rgb(250,137,0)',
        onNodeSelected: function(event, data) {
            unitID = data.id;
            transfer.room.selectedNode = data.nodeId;
        }, 
        onNodeUnselected: function(event, data) {
            unitID = null;
            transfer.room.selectedNode = null;
        }
    });

    // Wenn ein Raum bereits hinzugefügt wurde, letzte markierte Einheit wieder auswählen
    if (transfer.room.selectedNode !== null && transfer.room.selectedNode >= 0) {
        $unittree.treeview('selectNode', transfer.room.selectedNode);
        $unittree.treeview('expandNode', transfer.room.selectedNode);
        $unittree.treeview('revealNode', transfer.room.selectedNode);
    }
});

$(".content-inner").on('click', '#addBtn, #addBackBtn', function(e) {
    e.preventDefault();
    var clicked = $(this).attr('id');

    if (checkEntryRoom()) {
        return;
    } else {
        var error = false;

        var newRoom = {
            number: $roomNumber.val().trim(),
            maxoccupancy: parseInt($roomOccu.val().trim()),
            notes: $roomNotes.val().trim(),
            buildingID: buildingNumber,
            unitID: unitID,
            useID: 1   // Solange keine Nutzungsart festgelegt werden kann wird die 1 (Unterkunft) festgelegt
        };
    }
    
    // Laden aller Räume des Gebäudes
    socket.emit('loadRoomFromBuilding', buildingNumber, function(rooms) {
        
        // Prüfen ob die neue Raumnummer schon als Raumnummer vergeben ist
        $.each(rooms, function(index, room) {
            if (room.number.toString() === newRoom.number.toString()) {
                notifyError('Ein Raum mit dieser Nummer existiert bereits in Gebäude ' + room.buildingnumber + (room.alias !== '' ? " (" + room.alias + ")" : ''));
                $roomNumber.parent().addClass("has-error");
                error = true;
                return;
            }
        });
        
        if(!error) {

            socket.emit('addRoom', newRoom, function(result) {
                if (result) {
                    notifySuccess("Raum hinzugefügt ", "Raum " + newRoom.number + " wurde erfolgreich hinzugefügt");
                    if ( clicked === 'addBackBtn' ) {
                        routeProvider.navigateTo('unterkunftView');
                    } else {
                        routeProvider.navigateTo('addRoomView', buildingNumber);
                    }
                }
            });
        }
    }); 
});

// Nur Zahleneingabe bei maximaler Belegungsanzahl
// Automatisch entfernen aller anderen Zeichen und 
// begrenzen des Zahlenbereichs auf 0 bis 50
$roomOccu.keyup(function() {

    var value = $(this).val().replace(/[^0-9]/g, '');

    if (value < 0) value = '0';
    if (value > 50) value = '50';

    $(this).val(value);
});

// Prüft Eingabefelder auf leere Einträge
function checkEntryRoom() {
    var error = false;
    
    $(".has-error").removeClass("has-error");

    if (removeHTML()) { return true; }
    
    if ($roomNumber.val().trim() === '') {
        error = true;
        $roomNumber.parent().addClass("has-error");
    }

    if ($roomOccu.val().trim().replace(/[^0-9]/g, '') === '') {
        error = true;
        $roomOccu.parent().addClass("has-error");
    } else if ($roomOccu.val() < 0 || $roomOccu.val() > 50) {
        error = true;
        $roomOccu.parent().addClass("has-error");
    }

    if (unitID === null) {
        error = true;
        $unittree.addClass('has-error');
        $unittree.prev('label').addClass('has-error');
    }
    
    if (error) {
        notifyError('Bitte die rot markierten Felder ausfüllen');
    }

    return error;
}