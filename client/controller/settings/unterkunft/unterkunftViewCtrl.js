/* 
 Created on : 18.04.2016, 22:11:19
 Author     : Dennis
 */


/* global changeHeader, socket, routeProvider, transfer, $list, uvRights */

var     $contentInner = $('.content-inner'),
        $addNewBuilding = $('#addNewBuilding'),
        $editBtn = $('#editBtn');
        $contentInner = $('.content-inner'),
        $list = $('#tableUnterkunft tbody'),
        $modal = $('#modalRemove'),
        $removeThing = $('#removeThing'),
        $removeID = $('#removeID'),
        $headModal = $('#head'),
        $infoMsg = $('#infoMsg'),
        deleteCandidate = {},
        roomInUse = [],
        unterkunft = [];

// Gebäude Objekt
var Gebäude = {};

// Neues Gebäude hinzufügen
if (uvRights.buildingAdd) {
    $addNewBuilding.on('click', function(e) {
        e.preventDefault();
        routeProvider.navigateTo('addBuildingView');
    });
} else {
    $addNewBuilding.remove();
}

// Gebäude bearbeiten
if (uvRights.buildingEdit) {
    $contentInner.on('click', '.btn-edit' , function(e) {
        e.preventDefault();
        var id = $(this).attr('data-buildingid');
        transfer.building.editBuilding = id;
        routeProvider.navigateTo('editBuildingView', id);
    });
}

// Neuen Raum hinzufügen
if (uvRights.roomAdd) {
    $contentInner.on('click', '.btn-newRoom', function(e) {
        e.preventDefault();
        var id = $(this).attr('data-buildingid');
        transfer.room.addRoom = id;
        routeProvider.navigateTo('addRoomView', id);
    });
}

// Raum bearbeiten
if (uvRights.roomEdit) {
    $contentInner.on('click', '.btn-editRoom', function(e) {
        e.preventDefault();
        var id = $(this).attr('data-roomid');
        transfer.room.editRoom = id;
        routeProvider.navigateTo('editRoomView', id);
    });
}

// Gebäude-Delete-Button, wenn er disabled ist
$contentInner.on('mousedown', '.btn-delete:disabled', function(e) {
    notifyError('Gebäude ist in Verwendung und kann deswegen nicht gelöscht werden');
});

// Raum-Delete-Button, wenn er disabled ist
$contentInner.on('mousedown', '.btn-deleteRoom:disabled', function(e) {
    notifyError('Raum ist in Verwendung und kann deswegen nicht gelöscht werden');
});

// Laden aller Gebäude
socket.emit('loadAllBuildings', function(buildings) {
// daten in buidlings: buildingID, number, alias, notes
    $.each(buildings, function(index, building) {
        Gebäude.buildingID = building.buildingID;
        Gebäude.number = building.number;
        Gebäude.alias = building.alias;
        Gebäude.notes = building.notes;
        Gebäude.rooms = [];
        unterkunft.push(Gebäude);
        Gebäude = {};
    });

    if (unterkunft.length === 0) {
        if (uvRights.buildingAdd) {
            $contentInner.addClass("text-center").html('<strong>Es wurden noch keine Gebäude angelegt</strong><br><br>Über die Schaltfläche "Neue Gebäude hinzufügen" können diese angelegt werden<br><br>');
        } else {
            $contentInner.addClass("text-center").html('<strong>Es wurden noch keine Gebäude angelegt</strong><br><br>');
        }
    } else {
        socket.emit('loadAllRooms', function(rooms) {
        // daten in rooms: roomID, number, maxoccupancy, notes, buildingID, unitID, useID
            $.each(rooms, function(index, room) {
                $.each(unterkunft, function(index, unterkunft) {
                   if (room.buildingID === unterkunft.buildingID) {
                       unterkunft.rooms.push(room);
                       return;
                   } 
                });
            });
            show(unterkunft);
        });
    } 
});

// zeigt alle Gebäude und Räume an
// deaktiviert entsprechende Delete-Buttons
function show(unterkunft) {
    var contentString = "";
    $.each(unterkunft, function(index, building) {
        // Ausgabe umbrechen bei zulangen Wörtern
        building.notes = optimalNotes(building.notes);

        contentString = 
        '<tr id="' + building.buildingID + '">' +
        '   <td class="col-xs-7 col-sm-6 col-md-6">' + 
        '       <a data-building="' + building.buildingID + '" href="">' +
        '       <span class="buildingToggle glyphicon glyphicon-triangle-bottom" aria-hidden="true"></span>' + 
        '       <span class="label label-fixed label-building">Gebäude ' + building.number + '</span>' + 
        '       </a>' + 
        '       ' + (building.notes === '' ? '' : ' <span class="glyphicon glyphicon-file building-notes" aria-hidden="true" data-toggle="popover" data-trigger="hover" title="Gebäude ' + building.number + (building.alias === '' ? '' : ' - ' + building.alias) + '" data-content="' + building.notes + '"></span>') +
        '       ' + (building.alias === '' ? '' : '<strong class="building-alias">' + building.alias + '</strong> ') +  
        

        '   </td>' +
        '   <td class="col-xs-5 col-sm-6 col-md-6 actions">';

        if (uvRights.roomAdd) {
            contentString +=
            '       <button class="btn btn-sm btn-success btn-newRoom" href="#" data-buildingid="' + building.buildingID + '" role="button"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span><span class="btn-txt">Raum hinzufügen</span></button>';
        }
        if (uvRights.buildingEdit) {
            contentString +=
            '       <button class="btn btn-sm btn-default btn-edit" href="#" data-buildingid="' + building.buildingID + '" role="button"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span><span class="btn-txt">Bearbeiten</span></button>';
        }
        if (uvRights.buildingDel) {
            contentString +=
            '       <button class="btn btn-sm btn-danger btn-delete" href="#" data-name="' + building.number + '" data-buildingid="' + building.buildingID + '" role="button"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span><span class="btn-txt">Löschen</span></button>';
        }
        contentString +=
            '   </td>' +
            '</tr>';
        
        contentString +=
            '<tr class="building-rooms" data-buildingID="' + building.buildingID + '"><td colspan="2">' + 
            '    <table class="table table-hover simpletable building-rooms">';

        // Liste der Räume durchgehen und dem contentString hinzufügen
        if (building.rooms.length > 0) {
            $.each(building.rooms, function(index, room) {
                // Ausgabe umbrechen bei zulangen Wörtern
                room.notes = optimalNotes(room.notes);

                contentString += 
                    '<tr id="Raum'+ room.roomID + '" data-buildingID="' + building.buildingID + '">' +
                    '   <td class="col-xs-4 col-sm-4 col-md-6 ">' +
                    '       <span class="label label-fixed-sm label-building">Raum ' + room.number + '</span>' +
                    '      ' + (room.notes === '' ? '' : ' <span class="glyphicon glyphicon-file building-notes" aria-hidden="true" data-toggle="popover" data-trigger="hover" title="Raum ' + room.number + '" data-content="' + room.notes + '"></span>') +
                    '   </td>' +
                    '   <td class="col-xs-8 col-sm-8 col-md-6 actions">';
                
                if (uvRights.roomEdit) {
                    contentString += 
                    '       <button class="btn btn-sm btn-default btn-editRoom" href="#" data-building ="' + building.buildingID + '" data-roomid="' + room.roomID + '" role="button"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span><span class="btn-txt">Bearbeiten</span></button>';
                }
                if (uvRights.roomDel) {
                    contentString += 
                    '       <button class="btn btn-sm btn-danger btn-deleteRoom" href="#" data-buildingNumber="' + building.number + '" data-building ="' + building.buildingID + '" data-name="' + room.number + '" data-roomid="' + room.roomID + '" role="button"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span><span class="btn-txt">Löschen</span></button>';
                }
                contentString += 
                    '   </td>' +
                    '</tr>';
            });

        } else {
            contentString += 
            '<tr><td>Dieses Gebäude besitzt keine Räume</td></tr>';
        }

        contentString +=
            '   </table>' +
            '</tr></td>';

        $list.append(contentString);
    }); 

    // Prüfen, ob Raum in Verwendung ist  
    socket.emit('loadRoomResident', function(rooms) {
        $.each(rooms, function(index, room) {
            $('#Raum' + room.roomID +' > .actions > .btn-deleteRoom').attr('disabled', true);
            roomInUse.push(room.roomID);
        });
        
        // Prüfen ob Gebäude in Verwendung ist
        //  -> geht effizienter ;)
        socket.emit('loadAllRooms', function(rooms) {
            $.each(rooms, function(index, room) {
                var indexRoom = roomInUse.indexOf(room.roomID);
                if (room.roomID === roomInUse[indexRoom]) {
                    $('#' + room.buildingID +' > .actions > .btn-delete').attr('disabled', true);
                }
            });
        });
    });
    
    // Popover aktivieren
    $('.building-notes').popover({html: true});

    // Alle Räume verstecken
    $('tr[data-buildingID]').hide();

    // Aktivierte Räume einblenden
    socket.emit('BuildingToggle', function(buildings) {
        $.each(buildings, function(index, building) {
            $('a[data-building="' + building + '"] .buildingToggle').addClass('glyphicon-triangle-top').removeClass('glyphicon-triangle-bottom');
            $('tr[data-buildingID="' + building + '"]').show();
        });
    });

    // Ein- und Ausblenden der Räume
    $('span.building-notes').click(function(e) {
        e.preventDefault();

        $(this).prev('a').click();
    });

    $('a[data-building]').click(function(e) {
        e.preventDefault();
        var toggleID = $(this).attr('data-building');
        
        if ( $(this).children('.buildingToggle').hasClass('glyphicon-triangle-bottom') ) {
            $(this).children('.buildingToggle').addClass('glyphicon-triangle-top').removeClass('glyphicon-triangle-bottom');
            $('tr[data-buildingID="' + toggleID + '"]').fadeIn(150);
            socket.emit('BuildingToggleIn', toggleID);

        } else {
            $(this).children('.buildingToggle').removeClass('glyphicon-triangle-top').addClass('glyphicon-triangle-bottom');
            $('tr[data-buildingID="' + toggleID + '"]').fadeOut(150);
            socket.emit('BuildingToggleOut', toggleID);

        }
    });
}

// delete-Button für Gebäude
if (uvRights.buildingDel) {
    $contentInner.on('click', '.btn-delete', function(e) {
        e.preventDefault();
        $removeThing.text('das Gebäude');
        $headModal.text('Gebäude');
        
        // suchen des richtigen gebäudes in unterkunft und anzeigen der Infomeldung
        for (var i=0; i < unterkunft.length; i++) {
            if (unterkunft[i].buildingID.toString() === $(this).attr('data-buildingid')) {
                if (unterkunft[i].rooms.length > 0)
                    $infoMsg.text((unterkunft[i].rooms.length < 2) ? "Der einzelne Raum des Gebäudes wird auch gelöscht" : 'Die ' + unterkunft[i].rooms.length + ' Räume des Gebäudes werden auch gelöscht');
                else
                    $infoMsg.text('');
            }
        }    

        $removeID.text($(this).attr('data-name'));
        deleteCandidate = $(this);
        $modal.modal('show');
    });
}

// delete-Button für Räume
if (uvRights.roomDel) {
    $contentInner.on('click', '.btn-deleteRoom', function(e) {
        e.preventDefault();
        $infoMsg.text('');
        $removeThing.text('den Raum');
        $headModal.text('Raum');
        $removeID.text($(this).attr('data-name')  + ' des Gebäudes ' + $(this).attr('data-buildingNumber'));
        deleteCandidate = $(this);
        $modal.modal('show');
    });
}

// Lösch-Funktion für Gebäude und Räume
$modal.on('click', '#deleteOkBtn', function(e) {
    if ($headModal.text() === 'Gebäude') {
        for (var i=0; i < unterkunft.length; i++) {
            if (unterkunft[i].buildingID.toString() === deleteCandidate.attr('data-buildingid')) {
                if (unterkunft[i].rooms.length > 0) {
                    for (var j = 0; j < unterkunft[i].rooms.length; j++) {
                        deleteRoom(unterkunft[i].rooms[j].roomID, unterkunft[i].rooms[j].number, false);
                    }
                }
            }
        } 
        var dataName = deleteCandidate.attr('data-name');
        
        socket.emit('deleteBuilding', deleteCandidate.attr('data-buildingid'), function(result) {
            socket.emit('BuildingToggleOut', deleteCandidate.attr('data-buildingid'));
            if(result)
                notifySuccess("Gebäude gelöscht ", 'Gebäude ' + dataName + " wurde erfolgreich gelöscht");
            });
    } else {
        deleteRoom(deleteCandidate.attr('data-roomid'), deleteCandidate.attr('data-name'), true);
    }
    $('body').removeClass('modal-open');
    $('.modal-backdrop').remove();
    $modal.modal('hide');
    routeProvider.navigateTo('unterkunftView');
});

/**
 * deleteRoom Raum löschen
 *
 * @author Dennis Freitag
 */
function deleteRoom(id, number, anzahl) {
    socket.emit('deleteRoom', id, function(result) {
        if (result)
            if (anzahl) {
                notifySuccess("Raum gelöscht ", "Raum " + number + " wurde erfolgreich gelöscht");
            }
    });
}