/* 
 *  Settings -> editBuildingViewCtrl
 */

/* global socket, actURL, transfer, routeProvider */

var $backBtn = $('#btn-back'),
    $editBtn = $('#editBtn'),
    $buildingNumber = $('#buildingNumber'),
    $buildingAlias = $('#buildingAlias'),
    $buildingNotes = $('#buildingNotes'),
    $resetBtn = $('#resetBtn'),
    completeData,
    oldBuildingID = transfer.building.editBuilding || actURL[4];


// Backbutton
$backBtn.click(function(e) {
    e.preventDefault();
    routeProvider.navigateTo('unterkunftView');
});

// Landen des Gebäudes und befüllen der Eingabefelder
socket.emit('loadBuilding', oldBuildingID, function(building) {
    if (building === null) {
        notifyError('Dieses Gebäude existiert nicht');
        routeProvider.navigateTo('unterkunftView');
        return;
    }
    completeData = building;

    $buildingNumber.val(building.number);
    $buildingAlias.val(building.alias);
    $buildingNotes.val(building.notes);
    autolabelSet();
});

// Bei der Eingabe prüfen ob sich etwas geändert hat
$('input, textarea').keyup(function() {
    checkChangeBuilding();
});

// Zurücksetzen-Button
$resetBtn.click(function(e) {
    e.preventDefault();
    $buildingNumber.val(completeData.number);
    $buildingAlias.val(completeData.alias);
    $buildingNotes.val(completeData.notes);
    $resetBtn.prop('disabled', true);
    $(".has-error").removeClass("has-error");
    autolabelSet();
    setTimeout(function() {
        $buildingNumber.focus();
    }, 100);
});

// Speichern-Button
$editBtn.click(function(e) {
    e.preventDefault();

    if (!checkChangeBuilding()) {
        notifyWarning("Information", "Es wurden keine Änderungen durchgeführt");
        return;
    } else if (checkEntryBuilding()) {
        return;       
    } else {
        // neue Daten
        var newBuilding = {
            buildingID: oldBuildingID,
            number: $buildingNumber.val().trim(),
            alias: $buildingAlias.val().trim(),
            notes: $buildingNotes.val().trim()
        };

        // Prüfen ob es die möglicherweise geänderte Gebäudenummer schon existiert
        socket.emit('loadAllBuildings', function(buildings) {
            var exists = false;
            $.each(buildings, function(index, building) {
                if (building.number === newBuilding.number) {
                    if (building.buildingID.toString() !== newBuilding.buildingID.toString()) { 
                        notifyError('Es existiert bereits ein Gebäude mit dieser Nummer');
                        $buildingNumber.parent().addClass("has-error");
                        exists = true;
                        return;
                    } 
                }
            });

            if (!exists) {
                socket.emit('editBuilding', newBuilding, function(result) {
                    if (result) {
                        notifySuccess("Gebäude geändert ", "Gebäude " + newBuilding.number +" wurde erfolgreich geändert");
                        routeProvider.navigateTo('unterkunftView');
                    }
                });
            }
        });
    }
});

// Prüft Änderungen in den Eingabefeldern
function checkChangeBuilding() {
    var change = false;
    
    if (       completeData.number !== $buildingNumber.val().trim()
            || completeData.alias !== $buildingAlias.val().trim() 
            || completeData.notes !== $buildingNotes.val().trim())
        change = true; 
     
    if (change === true) {
        $resetBtn.prop('disabled', false);
    } else {
        $resetBtn.prop('disabled', true);
    }
    return change;
}

// Prüft Eingabefelder auf leere Einträge
function checkEntryBuilding() {
    var error = false;
    
    $(".has-error").removeClass("has-error");

    if (removeHTML()) { return true; }
    
    if ($buildingNumber.val().trim() === '') {
        error = true;
        $buildingNumber.parent().addClass("has-error");
    }
    
    if (error) {
        notifyError('Bitte eine Gebäudenummer eintragen');
    }

    return error;
}