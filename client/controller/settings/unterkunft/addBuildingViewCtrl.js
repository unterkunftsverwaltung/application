/* 
 *  Settings -> addBuildingViewCtrl
 */

var     $clearBtn = $('#clearBtn'),
        $backBtn = $('#btn-back'),
        $buildingNumber = $('#buildingNumber'),
        $buildingAlias = $('#buildingAlias'),
        $buildingNotes = $('#buildingNotes');


// Backbutton
$backBtn.click(function(e) {
    e.preventDefault();
    routeProvider.navigateTo('unterkunftView');
});

// Eingabefelder leeren
$clearBtn.on('click', function(e) {
    e.preventDefault();
    $buildingNumber.val('');
    $buildingAlias.val('');
    $buildingNotes.val('');
    $(".has-error").removeClass("has-error");
    autolabelClean();
    setTimeout(function() {
        $buildingNumber.focus();
    },300);
});

$(".content-inner").on('click', '#addBtn, #addBackBtn', function(e) {
    e.preventDefault();
    var clicked = $(this).attr('id');

    if (checkEntryBuilding()) {
        return;       
    } else {
        var error = false;

        var newBuilding = {
            number: $buildingNumber.val().trim(),
            alias: $buildingAlias.val().trim(),
            notes: $buildingNotes.val().trim()
        };
        
        socket.emit('loadAllBuildings', function(buildings) {
            $.each(buildings, function(index, building) {
                if (newBuilding.number === building.number) {
                    notifyError('Ein Gebäude mit dieser Nummer existiert bereits');
                    $buildingNumber.parent().addClass("has-error");
                    error = true;
                }       
            });
            
            if (!error) {
                socket.emit('addBuilding', newBuilding, function(result) {
                    if (result) {
                        notifySuccess("Gebäude hinzugefügt ", "Gebäude " + newBuilding.number + " wurde erfolgreich hinzugefügt");
                        if ( clicked === 'addBackBtn' ) {
                            routeProvider.navigateTo('unterkunftView');
                        } else {
                            routeProvider.navigateTo('addBuildingView');
                        }
                    }
                });
            }
        });
    }
});

// Prüft Eingabefelder auf leere Einträge
function checkEntryBuilding() {
    var error = false;
    $(".has-error").removeClass("has-error");

    if (removeHTML()) { return true; }

    if ($buildingNumber.val().trim() === '') {
        notifyError('Bitte eine Gebäudenummer eintragen');
        $buildingNumber.parent().addClass("has-error");
        error = true;
    }
    
    return error;
}