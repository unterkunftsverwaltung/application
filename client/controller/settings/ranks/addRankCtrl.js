/* 
 Created on : 20.02.2016, 02:42:10
 Author     : Dennis Freitag
 */

/* global socket, routeProvider */

var $contentInner = $('.content-inner'),
    $titleNew = $('#titleNew'),
    $shortNew = $('#shortNew'),
    $backBtn = $('#btn-back'),
    $clearBtn = $('#clearBtn'),
    dobbleEntry = false,
    newRankObject = Object;


// Button handler
$backBtn.click(function(e) {
    e.preventDefault();
    routeProvider.navigateTo('rankView');
});

// Handler clearBtn 
// Alle Felder zurücksetzen / leeren
$clearBtn.click(function() {
    $(".has-error").removeClass("has-error");

    $titleNew.val('');
    $shortNew.val('');
    autolabelClean();

    setTimeout(function() {
        $titleNew.focus();
    }, 300);
});

$contentInner.on('click', '#addRankBtn, #addBackBtn', function(e) {
    e.preventDefault();
    var clicked = $(this).attr('id');
    
    if (removeHTML()) { 
        return;
     } else if (checkEntry($titleNew, $shortNew)) {
        return;
    } else {

        newRankObject = {
            label: $titleNew.val().trim(),
            short: $shortNew.val().trim()
        };

        socket.emit('loadRankData', function(data) {
            $.each(data, function(index, element) {
                if (element.label.toString().toLowerCase() === newRankObject.label.toString().toLowerCase() || 
                        element.short.toString().toLowerCase() === newRankObject.short.toString().toLowerCase()) {
                    dobbleEntry = true;
                    notifyError('Es existiert bereits ein Dienstgrad mit diesem Namen und/oder der Kurzbezeichnung');
                    $titleNew.parent().addClass('has-error');
                    $shortNew.parent().addClass('has-error');
                    return;
                }
            });

            if (dobbleEntry) {
                dobbleEntry = false;
                return;
            }
            socket.emit('addRank', newRankObject, function(result) {
                notifySuccess("Dienstgrad hinzugefügt ", $titleNew.val().trim() + " wurde erfolgreich hinzugefügt");
                if ( clicked === 'addBackBtn' ) {
                    routeProvider.navigateTo('rankView');
                } else {
                    routeProvider.navigateTo('addRankView');
                }
            });
        });
    }
});