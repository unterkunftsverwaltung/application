/* 
 Created on : 20.02.2016, 13:40:37
 Author     : Dennis
 */

/* global transfer, socket, routeProvider */

var $contentInner = $('.content-inner'),
        $titleEdit = $('#titleEdit'),
        $shortEdit = $('#shortEdit'),
        $backBtn = $('#btn-back'),
        $resetBtn = $('#resetBtn'),
        rankID = transfer.ranks.editRank || actURL[4],
        rankObject = Object,
        editObject = Object;


// Laden des Dienstgrades
socket.emit('loadRank', rankID, function(result) {
    if (result === null) {
        notifyError('Dieser Dienstgrad existiert nicht');
        routeProvider.navigateTo('rankView');
        return;
    }

    rankObject = result;
    $titleEdit.val(rankObject.label);
    $shortEdit.val(rankObject.short);
    autolabelSet();
});

// button handler
$backBtn.click(function(e) {
    e.preventDefault();
    routeProvider.navigateTo('rankView');
});

// Handler resetBtn 
// Felder wieder auf den ursprünglichen Wert setzen
$resetBtn.click(function() {
    $(".has-error").removeClass("has-error");
    
    $titleEdit.val(rankObject.label);
    $shortEdit.val(rankObject.short);
    checkChange($titleEdit, $shortEdit);
    autolabelSet();

    setTimeout(function() {
        $titleEdit.focus();
    }, 100);
});

// Handler inputChange
// Bei Änderung der Eingabefelder die Funktion checkChange aufrufen
$("input").keyup(function() {
    checkChange($titleEdit, $shortEdit);
});

$contentInner.on('click', '#editBtn', function(e) {
    e.preventDefault();
    
    if (checkChange($titleEdit, $shortEdit) === false) {
            notifyWarning("Information", "Es wurden keine Änderungen durchgeführt");
            return;
    }
    
    if (removeHTML()) { 
        return; 
    } else if (checkEntry($titleEdit, $shortEdit)) {
        return;
    } else {

        editObject = {
            rankID: rankObject.rankID,
            label: $titleEdit.val().trim(),
            short: $shortEdit.val().trim()
        };

        // Prüfen ob geänderter rank (label oder short) schon in datenbank vorhanden ist
        socket.emit('loadRanksWithoutId', editObject.rankID, function(data) {
            var errorFound = false;
            $.each(data, function(index, element) {
                if (element.label.toString().toLowerCase() === editObject.label.toString().toLowerCase() || 
                        element.short.toString().toLowerCase() === editObject.short.toString().toLowerCase()) {
                    notifyError('Es existiert bereits ein Dienstgrad mit diesem Namen und/oder der Kurzbezeichnung');
                    $titleEdit.parent().addClass('has-error');
                    $shortEdit.parent().addClass('has-error');
                    errorFound = true;
                }
            });
            if (errorFound) return;

            socket.emit('editRank', editObject, function(result) {
                notifySuccess("Dienstgrad geändert ", $titleEdit.val().trim() + " wurde erfolgreich geändert");
                if (result) routeProvider.navigateTo('rankView');
            });
        });
    }
});

/**
 * checkChange Überprüfen ob Änderungen zur ursprünglichen Version vorliegen
 *
 * @author Rafal Welk, Anpassung: Dennis Freitag
 * @param {jQuery} e1 jQuery Element
 * @param {jQuery} e2 jQuery Element
 * @return {bool} Änderungen liegen vor = true
 */
function checkChange(e1, e2) {
    var change = false;

    if (e1.val() !== rankObject.label) {
        change = true;
    } else if (e2.val() !== rankObject.short) {
        change = true;
    }

    if (change === true) {
    	$resetBtn.prop('disabled', false);
    } else {
        $resetBtn.prop('disabled', true);
    }
    return change;
}