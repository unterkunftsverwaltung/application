/* 
 Created on : 20.02.2016, 02:16:49
 Author     : Dennis Freitag
 */

/* global socket, transfer, routeProvider */

var $addNewRank = $('#addNewRank'),
    $contentInner = $('.content-inner'),
    $removeRank = $('#removeRank'),
    $modal = $('#modalRemove'),
    $list = $('#tablerank tbody'),
    idForDelete = -1;
    completeData = Object,
    index = -1;

// Button newRank handler
$addNewRank.on('click', function(e) {
    e.preventDefault();
    routeProvider.navigateTo('addRankView');
});

// lade vorhandene Dienstgrade
socket.emit('loadRankData', function(data) {
    var contentString = '';
    completeData = data;
    
    // Prüfen ob Dienstgrad in Verwendung ist
    socket.emit('loadRankIdFromResidents', function(idFromResidents) {
        socket.emit('loadRankIdFromUser', function(idFromUser) {
            $.each(data, function(index, element) {
                var entryFound = false;

                contentString = 
                    '<tr id="' + element.rankID + '">' +
                    '   <td class="col-xs-7 col-sm-9 col-md-6">' + 
                    '       <span class="label label-fixed">' + element.short + '</span>' + 
                    '       <strong>' + element.label + '</strong>' +
                    '   </td>' +
                    '   <td class="col-xs-5 col-sm-3 col-md-6 actions">' + 
                    '       <button class="btn btn-sm btn-default btn-edit" href="#" data-unitid="' + element.rankID + '" role="button"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span><span class="btn-txt">Bearbeiten</span></button>' + 
                    '       <button class="btn btn-sm btn-danger btn-delete" href="#" data-unitid="' + element.rankID + '" role="button"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span><span class="btn-txt">Löschen</span></button>' +
                    '   </td>' +
                    '</tr>';
                $list.append(contentString);


                // Prüfen ob id in verwendung und disablen der Delete-Button
                idFromUser.forEach(function(entryFromUser) {
                    if (element.rankID === entryFromUser.rankID)
                        entryFound = true;
                });

                idFromResidents.forEach(function(entryFromResidents) {
                    if (element.rankID === entryFromResidents.rankID)
                        entryFound = true;
                });
            
                if (entryFound) {
                    $('#' + element.rankID + ' > .actions > .btn-delete').attr('disabled', true);
                }
            });
        });
    });
});

// Button handler
$contentInner.on('click', '.btn-edit', function(e) {
    e.preventDefault();
    var id = $(this).attr('data-unitid');
    transfer.ranks.editRank = id;
    routeProvider.navigateTo('editRankView', id);
});

$contentInner.on('mousedown', '.btn-delete:disabled', function(e) {
    notifyError('Dienstgrad ist in Verwendung und kann deswegen nicht gelöscht werden');
});

$contentInner.on('click', '.btn-delete', function(e) {
    e.preventDefault();
    idForDelete = $(this).attr('data-unitid');
    index = findIdInCompleteData(idForDelete);
    $removeRank.text(completeData[index].label);
    $modal.modal('show');
});

$modal.on('click', '#deleteOkBtn', function(e) {
    e.preventDefault();
    socket.emit('deleteRank', idForDelete, function(result) {
        if (result) {
            notifySuccess("Dienstgrad gelöscht ", completeData[index].label + " wurde erfolgreich gelöscht");
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            $modal.modal('hide');
            routeProvider.navigateTo('rankView');
        }
    });
});

function findIdInCompleteData(id) {
    var foundIndex = -1;
    $.each(completeData, function(index, element) {
        if (element.rankID.toString() === id.toString()) {
            foundIndex = index;
            return;
        }
    });
    return foundIndex;
}