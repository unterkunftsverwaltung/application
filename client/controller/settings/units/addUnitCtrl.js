/**
 * addUnitView Controller
 *
 * @extends {socket, routeProvider}
 */

// Deklaration
var $contentInner = $('.content-inner'),
    $titleNew = $('#titleNew'),
    $shortNew = $('#shortNew'),
    $parentNew = $('#tree'),
    $clearBtn = $('#clearBtn'),
    $backBtn = $('#btn-back'),
    selectedID = null,
    newUnitObject = Object;

// Handler addUnitBtn und addBackBtn
// Einheit hinzufügen
$contentInner.on('click', '#addBackBtn, #addUnitBtn', function(e) {
    e.preventDefault();
    var clicked = $(this).attr('id');

    if (checkEntryUnit()) {
        return;
    }

    newUnitObject = {
        name: $titleNew.val().trim(),
        short: $shortNew.val().trim(),
        member: selectedID
    };

    socket.emit('addUnit', newUnitObject, function(result) {
        if (!result) {
            notifyError('Es existiert bereits eine Einheit mit diesem Namen und/oder der Kurzbezeichnung');
            $titleNew.parent().addClass("has-error");
            $shortNew.parent().addClass("has-error");
            $parentNew.addClass('has-error');
        } else {
            notifySuccess("Einheit hinzugefügt ", newUnitObject.name + " wurde erfolgreich eingetragen");
            if ( clicked === 'addBackBtn' ) {
                routeProvider.navigateTo('unitView');
                transfer.units.selectedNode = null;
            } else {
                routeProvider.navigateTo('addUnitView');
            }
        }
    });
});

// Handler clearBtn 
// Alle Felder zurücksetzen / leeren
$clearBtn.click(function() {
    $(".has-error").removeClass("has-error");

    $titleNew.val('');
    $shortNew.val('');
    
    var selectedNode = $('#tree').treeview('getSelected', 0);
    $('#tree').treeview('unselectNode', [ selectedNode, { silent: true } ]);
    selectedID = null;
    autolabelClean();

    setTimeout(function() {
        $titleNew.focus();
    }, 300);
});


// Handler btnBack
// Zurück zu Einheiten
$backBtn.click(function(e) {
    e.preventDefault();
    transfer.units.selectedNode = null;
    routeProvider.navigateTo('unitView');
});

$('#sidebar-wrapper a').click(function() {
    transfer.units.selectedNode = null;
});

// Einheiten für den TreeView laden
// SOCKET EMIT loadUnits
socket.emit('loadUnits', true, '', function(units) {
    $parentNew.treeview({
        data: units,
        levels: 2, 
        color: '#555',
        backColor: '#fff',
        showTags: true,
        selectedBackColor: 'rgb(250,137,0)',
        onNodeSelected: function(event, data) {
            selectedID = data.id;
            transfer.units.selectedNode = data.nodeId;
        }, 
        onNodeUnselected: function(event, data) {
            selectedID = null;
            transfer.units.selectedNode = null;
        }
    });

    // Wenn keine Einheiten vorhanden sind, Keine auswählen
    if (units.length === 1) {
        $parentNew.treeview('selectNode', 0);
    }

    // Wenn eine Einheit bereits hinzugefügt wurde, letzte übergeordnete Einheit markieren
    if (transfer.units.selectedNode !== undefined && transfer.units.selectedNode > 0) {
        $parentNew.treeview('selectNode', transfer.units.selectedNode);
        $parentNew.treeview('expandNode', transfer.units.selectedNode);
        $parentNew.treeview('revealNode', transfer.units.selectedNode);
    }
});

/**
 * checkEntryUnit Funktion um zu überprüfen ob alle Felder ausgefüllt wurden
 *
 * @description Fehlerhafte Felder werden rot markiert
 * @author Rafal Welk <rafal@project.hatua.de>
 * @return {bool} Fehler vorhanden = true
 */
function checkEntryUnit() {
    var error = false;

    $(".has-error").removeClass("has-error");

    if (removeHTML()) { return true; }

    if ($titleNew.val().trim() === '') {
        error = true;
        $titleNew.parent().addClass("has-error");
    }

    if ($shortNew.val().trim() === '') {
        error = true;
        $shortNew.parent().addClass("has-error");
    }

    if (selectedID === null) {
        error = true;
        $parentNew.addClass('has-error');
        $parentNew.prev('label').addClass('has-error');
    }

    if (error) {
        notifyError('Bitte die rot markierten Felder ausfüllen');
    }

    return error;
}