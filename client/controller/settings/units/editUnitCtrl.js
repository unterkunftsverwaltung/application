/**
 * editUnitView Controller
 *
 * @extends {socket, transfer, routeProvider}
 */

// Deklaration
var $contentInner = $('.content-inner'),
	$titleChg = $('#titleChg'),
    $shortChg = $('#shortChg'),
    $parentChg = $('#tree'),
    $editUnitBtn = $('#editUnitBtn'),
    $resetBtn = $('#resetBtn'),
    $backBtn = $('#btn-back'),
    editUnitID = transfer.units.editUnit || actURL[4],
    parentNodeID = null,
    unitNodeID = null,
    selectedID = null,
    unitObject = new Object(),
    chgUnitObject = new Object();
    

// Handler btnBack 
// Zurück zu Einheiten
$backBtn.click(function(e) {
    e.preventDefault();
    routeProvider.navigateTo('unitView');
});

// Handler resetBtn 
// Felder wieder auf den ursprünglichen Wert setzen
$resetBtn.click(function() {
    $(".has-error").removeClass("has-error");
    
    $titleChg.val(unitObject.name);
    $shortChg.val(unitObject.short);
    selectedID = unitObject.parent;
    $parentChg.treeview('selectNode', [ parentNodeID, { silent: true } ]);
    checkChangeUnit();
    autolabelSet();

    setTimeout(function() {
        $titleChg.focus();
    }, 100);
});

// Handler inputChange + KeyUp
// Bei Änderung der Eingabefelder die Funktion checkChangeUnit aufrufen
$("input").change(function() {
	checkChangeUnit();
});

$("input").keyup(function() {
    checkChangeUnit();
});

// Handler editUnitBtn 
// Änderungen übernehmen
$editUnitBtn.click(function(e) {
	e.preventDefault();

	if (checkChangeUnit() === false) {
		notifyWarning("Information", "Es wurden keine Änderungen durchgeführt");
		return;
	}

	if (checkEntryUnit()) {
        return;
    }

    chgUnitObject = {
        unitID: editUnitID,
        name: $titleChg.val().trim(),
        short: $shortChg.val().trim(),
        member: selectedID
    };

    socket.emit('editUnit', chgUnitObject, function(result) {
    	if (!result) {
    		notifyError('Es existiert bereits eine Einheit mit diesem Namen und/oder der Kurzbezeichnung');
            $titleChg.parent().addClass("has-error");
            $shortChg.parent().addClass("has-error");
            $parentChg.addClass('has-error');
    	} else {
            notifySuccess("Einheit geändert ", chgUnitObject.name + " wurde erfolgreich abgeändert");
            routeProvider.navigateTo('unitView');
        }
    });
});

// Einheiten für den TreeView laden
// SOCKET EMIT loadUnits
socket.emit('loadUnits', true, '', function(units) {
    // Laden des UnitTrees
    $parentChg.treeview({
        data: units,
        levels: 1, 
        color: '#555',
        backColor: '#fff',
        showTags: true,
        selectedBackColor: 'rgb(250,137,0)',
        onNodeSelected: function(event, data) {
            selectedID = data.id;
            checkChangeUnit();
        }, 
        onNodeUnselected: function(event, data) {
            selectedID = null;
            checkChangeUnit();
        }
    });

    // Laden der Daten der Einheit
    socket.emit('loadUnit', editUnitID, function(unit) {
        if (unit === null) {
            notifyError('Diese Einheit existiert nicht');
            routeProvider.navigateTo('unitView');
            return;
        }

        unitObject = unit;

    	$titleChg.val(unitObject.name);
    	$shortChg.val(unitObject.short);
    	selectedID = unitObject.member;

    	// Auswahl der übergeordneten Einheit im TreeView
    	var tree = $parentChg.treeview('getEnabled', 0);

    	Object.keys(tree).forEach(function(key) {
    		if (tree[key].id == unitObject.member) {
    			parentNodeID = tree[key].nodeId;
    		} else if (tree[key].id == editUnitID) {
    			unitNodeID = tree[key].nodeId;
    		}
    	});

    	// ggf. keine auswählen wenn es sich um die oberste Ebene handelt.
    	if (unitObject.member === null) {
    		parentNodeID = 0;
    		selectedID = 'NULL';
    	}

    	// Speichern der übergeordneten Einheit zum Vergleich
    	unitObject.parent = selectedID;
    	
    	// Aktuelle übergeordnete Einheit auswählen
		$parentChg.treeview('selectNode', [ parentNodeID, { silent: true } ]);

		// Alle übergeordneten Einheiten öffnen (Parents)
		$parentChg.treeview('revealNode', [ parentNodeID, { silent: true } ]);

		// Deaktivieren der eigenen Einheit und damit auch der Kinder
		$parentChg.treeview('disableNode', [ unitNodeID, { silent: true } ]);

        autolabelSet();
    });
});

/**
 * checkChangeUnit Überprüfen ob Änderungen zur ursprünglichen Version vorliegen
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @return {bool} Änderungen liegen vor = true
 */
function checkChangeUnit() {
	var change = false;

	if ($titleChg.val() !== unitObject.name) {
		change = true;
	} else if ($shortChg.val() !== unitObject.short) {
		change = true;
	} else if (selectedID !== unitObject.parent) {
		change = true;
	}

	if (change === true) {
    	$resetBtn.prop('disabled', false);
    } else {
		$resetBtn.prop('disabled', true);
    }

    return change;
}

/**
 * checkEntryUnit Funktion um zu überprüfen ob alle Felder ausgefüllt wurden
 *
 * @description Fehlerhafte Felder werden rot markiert
 * @author Rafal Welk <rafal@project.hatua.de>
 * @return {bool} Fehler vorhanden = true
 */
function checkEntryUnit() {
    var error = false;

     $(".has-error").removeClass("has-error");

    if (removeHTML()) { return true; }

    if ($titleChg.val().trim() === '') {
        error = true;
        $titleChg.parent().addClass("has-error");
    }

    if ($shortChg.val().trim() === '') {
        error = true;
        $shortChg.parent().addClass("has-error");
    }

    if (selectedID === null) {
        error = true;
        $parentChg.addClass('has-error');
        $parentChg.prev('label').addClass('has-error');
    }

    if (error) {
        notifyError('Bitte die rot markierten Felder ausfüllen');
    }

    return error;
}