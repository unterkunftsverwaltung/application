/**
 * unitView Controller
 * 
 * @extends {socket, transfer, routeProvider}
 */

// Deklaration
var $addNewUnit = $('#addNewUnit'),
    $contentInner = $('.content-inner'),
    $list = $('#list'),
    $modal = $('#modalRemove'),
    parents = [];


// Handler addNewUnit
// Hinzufügen neuer Einheiten
$addNewUnit.on('click', function(e) {
    e.preventDefault();
    routeProvider.navigateTo('addUnitView');
});

/**
 * Einheiten anzeigen 
 * Hierzu werden zwei seperate Sockets verwendet
 *  - loadUnitsWithoutParents (Einheiten die an oberster Stelle stehen)
 *  - loadUnitData (Alle Einheiten die untergeordnet sind)
 * Aufbau der Liste und eintragen der benötigten Daten
 */
socket.emit('loadUnitsWithoutParent', function(parentUnits) {
    // laden aller Units
    socket.emit('loadUnitData', function(units) {
        
        if (parentUnits.length === 0) {
            // Wenn keine Einheiten vorhanden sind, Hinweistext
            $list.addClass("text-center").html("<strong>Es wurden noch keine Einheiten angelegt</strong><br><br>Über die Schaltfläche 'Neue Einheit hinzufügen' können diese angelegt werden");
        } else {

            var childSearch = new Array();

            // Übernehmen der Einheiten die an oberste Stelle stehen [member IS NULL]
            $.each(parentUnits, function(index, parentElement) {
                
                // Hinzufügen zu 'parents' und vorher level auf 0 setzen
                // Erstellen des Eintrags mit der Funktion unitTemplate
                parentElement.level = 0;
                $list.append(unitTemplate(parentElement));
                parents.push(parentElement);


                childSearch.push(parentElement);

                // Bewohner und Unterkünfte beachten
                disableDelete(parentElement.unitid);
            });

            // Ermitteln der untergeordneten Einheiten und erstellen des Eintrags unter der übergeordneten Einheit
            do {
                var nextSearch = new Array();
                childSearch.forEach(function(above) {
                    var nextUnits = new Array();

                    units.forEach(function(below) {

                        if (above.unitid === below.member) {

                            // Als möglicher Elternteil für den nächsten Suchlauf eintragen
                            nextSearch.push(below);

                            // Level und ParentName anfügen
                            below.level = above.level + 1;                 
                            below.parent = above.name;

                            // Eintrag hinzufügen
                            $('#' + above.unitid).append(unitTemplate(below));

                            // Übergeordnete Einheit kann nicht gelöscht werden
                            $('#' + above.unitid + ' > .pull-right > .btn-delete').attr("disabled", true).attr("data-parent", "true");

                            // Bewohner und Unterkünfte beachten
                            disableDelete(below.unitid);
                        } else {
                            // Da kein Elternteil muss es sich um eine untergeordnete Einheit handeln
                            // diese für den nächsten Durchgang eintragen
                            nextUnits.push(below);
                        }
                    });

                    units = nextUnits;
                });

                childSearch = nextSearch;
            } while (nextSearch.length > 0);

            // jQuery Funktion reverse
            jQuery.fn.reverse = [].reverse;

            // Umbau der Liste auf eine Ebene
            $('#list li').reverse().each(function() {
                $(this).prependTo("#list");
            });

            // Handler disabled-deleteUnit
            // Information anzeigen warum die Einheit nicht gelöscht werden kann
            $contentInner.on("mousedown", ".btn-delete:disabled", function(e) {
                var errormsg = ''
                
                // untergeordnete Einheiten?
                if ($(this).attr("data-parent") === 'true') {
                    errormsg += 'Einheit besitzt untergeordnete Einheiten<br>';
                }

                // Unterkünfte?
                if ($(this).attr("data-rooms") > 0) {
                    errormsg += $(this).attr("data-rooms") + 'x ' + ($(this).attr("data-rooms") > 1 ? 'Unterkünfte sind' : 'Unterkunft ist') + ' der Einheit zugeweisen<br>';
                }

                // Bewohner?
                if ($(this).attr("data-residents") > 0) {
                    errormsg += $(this).attr("data-residents") + 'x Bewohner ' + ($(this).attr("data-residents") > 1 ? 'sind' : 'ist') + ' der Einheit zugeweisen<br>';
                }

                notifyError(errormsg);
            });

            // Handler editUnit
            // Bearbeiten der Einheit
            $contentInner.on('click', '.btn-edit', function(e) {
                e.preventDefault();
                transfer.units.editUnit = $(this).attr('data-unitid');
                routeProvider.navigateTo('editUnitView', $(this).attr('data-unitid'));
            });

            // Handler btn-delete 
            // Anzeige des Modalfensters, wo noch bestätigt werden muss das diese Einheit gelöscht werden soll
            $(".btn-delete").click(function(e) {
                e.preventDefault();
                notifyClose();
                $("#removeUnit").text( $(this).attr("data-unitname") );
                $('#btnConfirmDelete').attr("data-unitid", $(this).attr("data-unitid") );
                $modal.modal('show');
            });

            // Tooltips einblenden
            $('.label-additional').popover();
        }
    });
});


// Handler btnConfirmDelete
// Bestätigen des Löschens einer Einheit
$modal.on('click', '#btnConfirmDelete', function(e) {
    e.preventDefault();

    socket.emit('deleteUnit', $(this).attr("data-unitid"), function(result) {
        if (result) {
            // Löschen erfolgreich
            $modal.modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            notifySuccess("Einheit gelöscht: ", $("#removeUnit").text() + " wurde erfolgreich gelöscht");
        } else {
            // Löschen hat nicht funktioniert
            $modal.modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
            notifyError("Einheit '" + $("#removeUnit").text() + "'' konnte nicht gelöscht werden");
        }

        // Aktualisieren der Anzeige durch erneutes Anzeigen
        routeProvider.navigateTo('unitView');
    });
});

/**
 * unitTemplate Funktion um ein Template zusammenzubauen für die HTML Einträge
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param  {object} unit Einheit aus der die Daten ermittelt werden
 * @return {string} Rückgabe des HTML Einträgs für die Einheit
 */
function unitTemplate(unit) {
    var $listtemplate = 
    '<li class="clearfix" id="' + unit.unitid + '">' +
    '<div class="pull-left">';

    for (var k = 1; k <= unit.level; k++) {
        $listtemplate += '<span class="unitSpace"></span>';
    }

    $listtemplate += 
    '<span class="unitLabel size-' + (unit.short.indexOf(' ') > -1 ? unit.short.indexOf(' ') : (unit.short).length) + '" style="background-color: ' + shadeColor('#c06c1c', unit.level*15) + '; ' + (unit.level > 4 ? 'color: #303030;' : '') + '">' + unit.short.replace(' ', ' <br>') + '</span></div>' + 
    '<div class="pull-right">' + 
    ' <button class="btn btn-sm btn-default btn-edit" href="#" data-unitid="' + unit.unitid + '" role="button"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span><span class="btn-txt">Bearbeiten</span></button>' +
    ' <button class="btn btn-sm btn-danger btn-delete" href="#" data-unitname="' + unit.name + '" data-unitid="' + unit.unitid + '" data-parent="false" data-rooms="' + unit.rooms + '" data-residents="' + unit.residents + '" role="button"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span><span class="btn-txt">Löschen</span></button>' +                    
    '</div><p>';

    if (unit.parent) {
        $listtemplate += '<small>' + unit.parent + '</small>';
    }

    $listtemplate += unit.name + '<span class="label-box">';
    
    if (unit.rooms > 0) {
        $listtemplate += '<span class="label label-additional label-rooms" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="' + (unit.rooms > 1 ? unit.rooms + ' Unterkünfte gehören' : 'Eine Unterkunft gehört') + ' der Einheit ' + unit.name + '"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>' + unit.rooms + '</span>';
    }

    if (unit.residents > 0) {
        $listtemplate += '<span class="label label-additional label-residents" data-toggle="popover" data-trigger="hover" data-placement="bottom" data-content="' + unit.residents + (unit.residents > 1 ? ' Bewohner gehören' : ' Bewohner gehört') + ' zu der Einheit ' + unit.name + '"><span class="glyphicon glyphicon-user" aria-hidden="true"></span>' + unit.residents + '</span>';
    }

    $listtemplate += '</span></p>';
    
    return $listtemplate;
}

/**
 * disableDelete Einträge mit Bewohnern und/oder Unterkünften können nicht
 *   gelöscht werden diese Funktion soll den Löschbutton ggf. deaktivieren
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param  {int} unitId Übergabe der Einheit mit der unitID die per jQuery
 *   überpüft wird 
 */
function disableDelete(unitId) {
    if ( ($('#' + unitId + " .btn-delete").attr("data-rooms") > 0) || ($('#' + unitId + " .btn-delete").attr("data-residents") > 0) ) {
        $('#' + unitId + " .btn-delete").attr("disabled", true);
    }
}