/**
 * userView Controller
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @extends {socket, transfer, routeProvider}
 */

$(document).ready(function() {

	// New User Handler
	if (uvRights.users === true) {
		$("#addNewUser").click(function(event) {
			event.preventDefault();
			routeProvider.navigateTo('addUserView');
		});
	} else {
		$('#addNewUser').remove();
	}
	
	// laden der vorhanden Benutzer
	socket.emit('loadUsers', function(data) {
		var contentString = '';

		$.each(data, function(index, element) {

			contentString = 
				'<tr id="' + element.userID + '">' +
		        '   <td class="col-xs-7 col-sm-9 col-md-6">' + 
		        '       <span class="label label-fixed label-fixed-xl">' + (element.username).toUpperCase() + '</span>' + 
		        '       <strong>' + element.surname + ', ' + element.forename +'</strong>' +
		        '   </td>' +
		        '   <td class="col-xs-5 col-sm-3 col-md-6 actions">';

		    if (uvRights.users === true) {
		    	contentString += 
		        '       <button class="btn btn-sm btn-default btn-edit" data-userid="' + element.userID + '" role="button"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span><span class="btn-txt">Bearbeiten</span></button>' + 
		        '       <button class="btn btn-sm btn-danger btn-delete" data-userid="' + element.userID + '" data-username="' + element.username + '" role="button"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span><span class="btn-txt">Löschen</span></button>';
		  	} else if (uvRights.reset === true) {
		  		contentString += 
		        '       <button class="btn btn-sm btn-default btn-edit" data-userid="' + element.userID + '" role="button"><span class="glyphicon glyphicon-repeat" aria-hidden="true"></span><span class="btn-txt">Passwort zurücksetzen</span></button>';
		  	}

			contentString +=
		        '   </td>' +
		        '</tr>';
		        
		    $("#tableuser tbody").append(contentString);
		});

		// Administrator kann nicht gelöscht oder bearbeitet werden
		// Eigener Benutzer kann nicht gelöscht werden
		// Dadurch wird sichergestellt, das immer ein Admin vorhanden sein muss.
		if (uvUser.userID !== 1) {
			$('#' + uvUser.userID + ' > .actions > .btn-delete').attr("disabled", true).removeClass("btn-delete");
		}
		$('#1 > .actions > .btn-delete').remove();
		$('#1 > .actions > .btn-edit').remove();
	});
});

// Handler disabled-buttons
// Benutzer kann sich nicht selber löschen!
$('.content-inner').on("mousedown", "button:disabled", function(e) {
    notifyError('Der eigene Benutzer kann nicht gelöscht werden');
});

// Handler editUser
// Bearbeiten eines Benutzers
$('.content-inner').on('click', '.btn-edit', function(e) {
	e.preventDefault();
    transfer.users.editUser = $(this).attr('data-userid');
    routeProvider.navigateTo('editUserView', $(this).attr('data-userid'));
});

// Handler btn-delete 
// Anzeige des Modalfensters, wo noch bestätigt werden muss das dieser Benutzer gelöscht werden soll
$('.content-inner').on('click', '.btn-delete', function(e) {
    e.preventDefault();
    notifyClose();
    if ($(this).attr("data-userid") !== '1') {
	    $("#removeUser").text( $(this).attr("data-username") );
	    $('#btnConfirmDelete').attr("data-userid", $(this).attr("data-userid") );
	    $('#modalRemove').modal('show');
	}
});

// Handler btnConfirmDelete
// Bestätigen des Löschens eines Benutzers
$('#modalRemove').on('click', '#btnConfirmDelete', function(e) {
    e.preventDefault();

    if ( $(this).attr("data-userid") === '1' ) {
    	return;
    } else {
	    socket.emit('deleteUser', $(this).attr("data-userid"), function(result) {
	        if (result) {
	            // Löschen erfolgreich
	            $('#modalRemove').modal('hide');
	            $('body').removeClass('modal-open');
	            $('.modal-backdrop').remove();
	            notifySuccess("Benutzer gelöscht: ", $("#removeUser").text() + " wurde erfolgreich gelöscht");
	        } else {
	            // Löschen hat nicht funktioniert
	            $('#modalRemove').modal('hide');
	            $('body').removeClass('modal-open');
	            $('.modal-backdrop').remove();
	            notifyError("Benutzer '" + $("#removeUser").text() + "'' konnte nicht gelöscht werden");
	        }

	        // Aktualisieren der Anzeige durch erneutes Anzeigen
	        routeProvider.navigateTo('userView');
	    });
	}
});