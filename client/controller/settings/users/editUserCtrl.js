/**
 * editUserView Controller
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @extends {socket, transfer, routeProvider}
 */

// Deklaration
var $username           = $('#username'),
    $surname            = $('#surname'),
    $forename           = $('#forename'),
    $rank               = $('#rank'),
    $password           = $('#password'),
    $passwordConfirm    = $('#passwordConfirm'),
    userData            = Object,
    roleData            = Object,
    maxLevel            = 0, 
    userOld             = Object,
    rolesOld            = Object,
    editFunc            = '',
    tabIndex            = 7;

// Bearbeiten oder Hinzufügen
var userFunc            = ($('h2.page_title').text() === 'Benutzer bearbeiten' ? true : false);

// Rechte ermittlen
if (userFunc && uvRights.users && !uvRights.reset) { 
    editFunc = 'no-reset';
} else if (userFunc && !uvRights.users && uvRights.reset) { 
    editFunc = 'no-user';
}

$(document).ready(function() {

    // Darstellung je nach Berechtigung anpassen
    if (editFunc === 'no-reset') {
        $('.no-reset').addClass('hidden');

    } else if (editFunc === 'no-user') {
        $('.no-user').addClass('hidden');
        $('#editBtn').html('<span class="glyphicon glyphicon-repeat" aria-hidden="true"></span> Passwort zurücksetzen');
        $('#resetBtn').html('<span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span> Felder leeren');
        $('.page_title').text('Passwort zurücksetzen');
        document.title = 'Passwort zurücksetzen | Unterkunftsverwaltung';

    }

    // back button handler
    $("#btn-back").click(function(e) {
        e.preventDefault();
        routeProvider.navigateTo('userView');
    });

    // Laden der Dienstgrade
    // SOCKET EMIT loadRanks
    socket.emit('loadRankData', function(rank) {
    	var contentString;

    	$.each(rank, function(index, element) {
    		contentString += '<option data-content="<span class=\'pull-right subtext\'>' + element.short + '</span> <span class=\'text\'>' + element.label + '</span>" data-tokens="' + element.short + ' ' + element.label + '">' + element.rankID + '</option>';
    	});

    	$rank.append(contentString).selectpicker();
    });

    /**
     * Laden der Einheiten und Rollen
     * Hierzu werden drei seperate Sockets verwendet
     *  - loadUnitsWithoutParents   (Einheiten die an oberster Stelle stehen)
     *  - loadUnitData              (Alle Einheiten die untergeordnet sind)
     *  - loadRoles                 (Alle Rollen die existieren)
     * Aufbau der Liste und eintragen der benötigten Daten
     */
    socket.emit('loadUnitsWithoutParent', function(parentUnits) {
        // laden aller Units
        socket.emit('loadUnitData', function(units) {
            // laden der Rollen
            socket.emit('loadRoles', function(roles) {

                var parents = [],
                    childSearch = [],
                    contentString = ''; 
            	
                $.each(parentUnits, function(index, parentElement) {
                    // Hinzufügen zu 'parents' und vorher level auf 0 setzen
                    // Erstellen des Eintrags mit der Funktion buildmatrix
                    parentElement.level = 0;
                    $("#unit-right").append(buildmatrix(parentElement));

                    parents.push(parentElement);
                    childSearch.push(parentElement);
                });

                // Ermitteln der untergeordneten Einheiten und erstellen des Eintrags unter der übergeordneten Einheit
                var nextSearch = [];
                do {
                    nextSearch = [];

                    childSearch.forEach(function(above) {
                        var nextUnits = [];

                        units.forEach(function(below) {

                            if (above.unitid === below.member) {

                                // Als möglicher Elternteil für den nächsten Suchlauf eintragen
                                nextSearch.push(below);

                                // Level und ParentName anfügen
                                below.level = above.level + 1;                 
                                below.parent = above.name;

                                if (maxLevel < below.level) {
                                    maxLevel = below.level;
                                }

                                // Eintrag hinzufügen
                                $('#unit-' + above.unitid).append(buildmatrix(below));
                            } else {
                                // Da kein Elternteil muss es sich um eine untergeordnete Einheit handeln
                                // diese für den nächsten Durchgang eintragen
                                nextUnits.push(below);
                            }
                        });

                        units = nextUnits;
                    });

                    childSearch = nextSearch;
                } while (nextSearch.length > 0);

                // jQuery Funktion reverse
                jQuery.fn.reverse = [].reverse;

                // Umbau der Liste auf eine Ebene
                $('#unit-right div.input-group').reverse().each(function() {
                    $(this).prependTo("#unit-right");
                });

                // Hinzufügen der Rollen
                contentStringAll = '<option value="">keine Rechte</option>';
                contentStringOther = '<option value="">keine Rechte oder vererbte Rechte</option>';
                $.each(roles, function(index, element) {
                    contentString += '<option data-content="<span class=\'text\'>' + element.name + '</span> " data-tokens="' + element.name + '" value="' + element.roleID + '">' + element.roleID + '</option>';
                });

                // Eintragen der Rollen und übertrag in Bootstrap Select
                $(".rolepicker").append(contentStringOther + contentString);
                $("#unit-all").html('').append(contentStringAll + contentString);

                $(".rolepicker").selectpicker();

                // Bei Änderungen an den Eingabefelder diese als Änderung anzeigen
                $rank.change(function(e) {
                    checkChangesUser(true);
                });

                $('input').change(function(e) {
                    checkChangesUser(true);
                });

                $('input').keyup(function(e) {
                    checkChangesUser(true);
                });

                // Hinweisfenster für vererbte Berechtigungen
                $('#inheritHelp').attr('data-content', 'Berechtigungen die von einer übergeordneten Einheit übernommen wurden werden farblich <strong class="inherit">in dieser Farbe</strong> hervorgehoben').popover({html: true});

                // Tooltips aktivieren
                $('.input-group-addon').popover();

                // Laden des zu bearbeitenden Nutzers
                if (userFunc === true) {
                    loadEditUnit(function() {
                        rolepickerOn();
                    });
                } else {
                    autolabelSet();
                    rolepickerOn();
                }

                // Tabindex an die Schaltflächen weitergeben
                $("#addBtn").attr('tabindex', tabIndex + 1);
                $("#editBtn").attr('tabindex', tabIndex + 1);
                $('button[data-toggle="dropdown"]:last').attr('tabindex', tabIndex + 2);
                $("#clearBtn").attr('tabindex', tabIndex + 3);
                $("#resetBtn").attr('tabindex', tabIndex + 3);
                $("#btn-back").attr('tabindex', tabIndex + 4);

            });
        });
    });

    // Löschen der eingegebenen Daten
    $("#clearBtn").click(function(e) {
        e.preventDefault();
        autolabelClean();

        $("input").val('');
        $rank.selectpicker('deselectAll');
        $(".rolepicker").selectpicker('deselectAll');
        inherit(maxLevel, 0);
        $(".has-error").removeClass("has-error");
        checkChangesUser(false);

        setTimeout(function() {
            $('.content-inner input:first').focus();
        }, 300);
    });

    // Zurücksetzen der eingegebenen Daten
    $("#resetBtn").click(function(e) {
        e.preventDefault();
        rolepickerOff();
        loadEditUnit(function() {
            rolepickerOn();
        });
        $(".has-error").removeClass("has-error");
        autolabelSet();

        setTimeout(function() {
            $('.content-inner input:first').focus();
        }, 100);
    });
    
    // Hinzufügen des neuen Benutzers
    $(".content-inner").on('click', '#addBtn, #addBackBtn', function(e) {
        e.preventDefault();
        var clicked = $(this).attr('id');

        if (checkEntryUser()) {
            return;
        }
        
        prepare_addedit();

        // Benutzer eintragen
        socket.emit('addUser', userData, function(result) {
            if (!result) {
                notifyError('Der eingegebene Benutzername existiert bereits');
                $username.parent().addClass("has-error");
            } else {
                notifySuccess("Benutzer hinzugefügt ", userData.surname + ', ' + userData.forename + ' (' + userData.username + ") wurde erfolgreich angelegt");
                if ( clicked === 'addBackBtn' ) {
                    routeProvider.navigateTo('userView');
                } else {
                    routeProvider.navigateTo('addUserView');
                }
            }
        });
    });

    $('#editBtn').click(function(e) {
        e.preventDefault();

        if ($("#resetBtn").prop('disabled') === true) {
            if (editFunc !== 'no-user') {
                notifyWarning("Information", "Es wurden keine Änderungen durchgeführt");
            } else {
                notifyWarning("Information", "Bitte ein neues Passwort eingeben");
            }
            
            return;
        }

        if (checkEntryUser()) {
            return;
        }

        prepare_addedit();

        // Benutzer eintragen
        socket.emit('editUser', userData, function(result) {
            if (!result) {
                notifyError('Der eingegebene Benutzername existiert bereits');
                $username.parent().addClass("has-error");
            } else {
                if (editFunc === 'no-user') {
                    notifySuccess("Passwort zurückgesetzt ", userData.surname + ', ' + userData.forename + ' (' + userData.username + ") wurde neues Passwort zugewiesen");
                } else {
                    notifySuccess("Benutzer bearbeitet ", userData.surname + ', ' + userData.forename + ' (' + userData.username + ") wurde erfolgreich geändert");    
                }
                
                routeProvider.navigateTo('userView');
            }
        });

    });

});

/**
 * prepare_addedit Erstellen der Datenvorlage für das Hinzufügen und ändern der Benutzer
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @return {userData} vorbereitete Vorlage
 */
function prepare_addedit() {
    // Eingegebene Daten zusammenstellen
    userData = {
        username:   $username.val().trim(),
        surname:    $surname.val().trim(),
        forename:   $forename.val().trim(),
        rank:       $rank.val(),
        password:   $password.val().trim(),
        roles:      []
    };

    if (userFunc === true) {
        userData.userID = transfer.users.editUser || actURL[4];
        userData.editFunc = editFunc;
    }

    // Ausgewählte Rollen übernehmen
    $(".rolepicker").each(function(index) {
        if ( $(this).val() !== '' ) {
            roleData = {
                unit:   (($(this).parent().children("button").attr("data-id")).replace('unitright-', '')).replace('unit-all', 'NULL'),
                role:   $(this).val()
            };
            userData.roles.push(roleData);
        }
    });

    return userData;
}

/**
 * checkEntryUser Funktion um zu überprüfen ob alle Felder ausgefüllt wurden
 *
 * @description Fehlerhafte Felder werden rot markiert
 * @author Rafal Welk <rafal@project.hatua.de>
 * @return {bool} Fehler vorhanden = true
 */
function checkEntryUser() {
    var error = false,
        errorMsg = '',
        roles = 0;

    $(".has-error").removeClass("has-error");

    if (removeHTML()) { return true; }

    if ($username.val().trim() === '') {
        error = true;
        $username.parent().addClass("has-error");
    }

    if ($surname.val().trim() === '') {
        error = true;
        $surname.parent().addClass("has-error");
    }

    if ($forename.val().trim() === '') {
        error = true;
        $forename.parent().addClass("has-error");
    }

    if ($rank.val() === '') {
        error = true;
        $rank.parent().parent().addClass("has-error");
    }

    if (userFunc === false) {

        if ($password.val().trim() === '') {
            error = true;
            $password.parent().addClass("has-error");
        }

        if ($passwordConfirm.val().trim() === '') {
            error = true;
            $passwordConfirm.parent().addClass("has-error");
        }
    }

    if ($password.val().trim().length < 8 && $password.val().trim() !== '') {
        error = true;
        $password.parent().addClass("has-error");
        $passwordConfirm.parent().addClass("has-error");
        errorMsg += '<br>Passwort muss aus min. 8 Zeichen bestehen';
    }

    if ($passwordConfirm.val() !== $password.val()) {
        error = true;
        $password.parent().addClass("has-error");
        $passwordConfirm.parent().addClass("has-error");
        errorMsg += '<br>Das eingegebene Passwort stimmt nicht überein.';
    }

    $('.rolepicker').each(function(index) {
        if ( $(this).val() !== '' ) {
            roles += 1;
        }
    });

    if (roles === 0) {
        error = true;
        $('#unit-right').addClass('has-error');
        $('.input-group-allunits').parent().addClass('has-error');
        $('.user-rights > label').addClass('has-error');
        errorMsg += '<br>Mindestens 1x Rolle muss ausgewählt werden';
    }

    if (error) {
        notifyError('Bitte die rot markierten Felder ausfüllen.' + errorMsg);
    } 

    return error;
}

/**
 * checkChangesUser Wechsel des Buttons zurücksetzen
 *
 * @description Ermittlen ob an den Daten Änderungen zu den ursprüglichen Daten bestehen
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param  {bool} status Deaktiviert (true) oder aktiviert (false)
 */
function checkChangesUser(status) {
    if (status === true) {
        var changes = false,
            compareRoles1 = [],
            compareRoles2 = [];

        if (userFunc === false) {
            userOld.username = '';
            userOld.surname = '';
            userOld.forename = '';
            userOld.rankID = 0;
            rolesOld = [];
        }

        // Überprüfen ob noch die Ursprungsdaten vorhanden sind
        if ( userOld.username !== $username.val().trim() ) {                           changes = true; } 
        if ( userOld.surname !== $surname.val().trim() ) {                             changes = true; } 
        if ( userOld.forename !== $forename.val().trim() ) {                           changes = true; } 
        if ( userOld.rankID.toString() !== $rank.val() ) {                             changes = true; } 
        if ( $password.val().trim() !== '' || $passwordConfirm.val().trim() !== '' ) { changes = true; }

        $(".rolepicker").each(function(index) {
            if ( $(this).val() !== '' ) {
                compareRoles1.push( (($(this).parent().children("button").attr("data-id")).replace('unitright-', '')).replace('unit-all', 'null') + '-' + $(this).val() );
            }
        });

        rolesOld.forEach(function(role) {
            compareRoles2.push (role.unitID + '-' + role.roleID);
        });

        // Vergleichen dieser miteinander
        if (compareRoles1.length !== compareRoles2.length) {
            changes = true;
        } else {
            compareRoles1.sort();
            compareRoles2.sort();

            for (var key in compareRoles1) {
                if (compareRoles1[key] !== compareRoles2[key]) {
                    changes = true;
                }
            }
        }

        // Schaltfläche aktivieren bei änderungen
        $('#resetBtn').prop('disabled', (changes === true ? false : true) );
    } else {
        $('#resetBtn').prop('disabled', true);
    }
}

/**
 * loadEditUnit Laden des zu bearbeitenden Benutzers 
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @return {[type]} [description]
 */
function loadEditUnit(callback) {    
    socket.emit('showUser', transfer.users.editUser || actURL[4], function(user) {
        socket.emit('showUserRoles', transfer.users.editUser || actURL[4], function(roles) {
            if (user === null) {
                notifyError('Dieser Benutzer existiert nicht');
                routeProvider.navigateTo('userView');
                return;
            }

            // Speichern der aktuellsten Daten für den Vergleich
            userOld = (JSON.parse(JSON.stringify(user)));
            rolesOld = (JSON.parse(JSON.stringify(roles)));

            // Der Administrator kann nicht bearbeitet werden
            if (userOld.userID === 1) {
                notifyError('Der Administrator kann nicht bearbeitet werden');
                routeProvider.navigateTo('userView');
                return;
            }

            // Zurücksetzen der Auswahlfelder
            $(".rolepicker").selectpicker('deselectAll');
            $(".rolepicker").selectpicker('destroy');
            $(".rolepicker").selectpicker();

            // Eintragen der geladenen Daten
            $username.val(user.username);
            $surname.val(user.surname);
            $forename.val(user.forename);
            $rank.selectpicker('val', user.rankID);
            $password.val('');
            $passwordConfirm.val('');

            for (i = 0; i < roles.length; i++) {
                if (roles[i].unitID === null) {
                    $('#unit-all').selectpicker('val', roles[i].roleID);
                } else {
                    $('#unit-' + roles[i].unitID + ' .rolepicker').selectpicker('val', roles[i].roleID);
                }
            }

            // Reihenfolge festlegen
            tabIndex = 6;
            $('div.rolepicker button').each(function(index) {
                tabIndex += 1;
                $(this).attr('tabindex', tabIndex);
            });

            // Vererben der Rollen
            inherit(maxLevel, 0);

            // Zurücksetzen Button deaktivieren
            checkChangesUser(false);

            // Überschriften einblenden
            autolabelSet();

            // Callback
            callback(true);
        });
    });
}

/**
 * inherit Anzeigen wie die Rechte über die gesamten Einheiten vererbt werden
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param  {int}    max     Anzahl der maximalen Ebenen
 * @param  {string} changed Objekt welches verändert wurde
 */
function inherit(max, changed) {
    var actUnit, actValue, actTitle, subUnit, subValue;

    $('.user-rights #unit-' + changed + ' .rolepicker .filter-option').removeClass('inherit');
    $('.user-rights .rolepicker .filter-option.inherit').html('').attr('data-top', '').removeClass('inherit');
    
    // Rechte auf alle Einheiten anwenden
    if ( $('#unit-all .rolepicker').val() !== '') {
        actValue = $('#unit-all').val();
        actTitle = $('#unit-all').parent().children('button').attr('title');

        if (actValue === '') {
            actTitle = '';
        }

        $('.user-rights [data-level=0]').each(function(index) {
            subUnit = $(this).attr('data-unit');
            subValue = $('#unitright-' + subUnit).val();

            if (subValue === '' && actValue !== '') {
                $('#unit-' + subUnit + ' .rolepicker .filter-option').html(actTitle).attr('data-top', actValue).addClass("inherit");
                $('#unit-' + subUnit + ' button').attr('title', actTitle);
            } else if (actValue === subValue) {
                $('#unit-' + subUnit + ' .rolepicker').selectpicker('val', '');
                $('#unit-' + subUnit + ' .rolepicker .filter-option').html(actTitle).attr('data-top', actValue).addClass("inherit");
                $('#unit-' + subUnit + ' button').attr('title', actTitle);
            }
        });
    }
    
    for (level = 0; level < max; level++) {
        $('.user-rights [data-level="' + level + '"]').each(function(index) {
            actUnit = $(this).attr('data-unit');
            actValue = $('#unitright-' + actUnit).val();
            actTitle = $('#unit-' + actUnit + ' button').attr('title');

            if (actValue === '') {
                actValue = $('#unit-' + actUnit + ' .rolepicker .filter-option').attr('data-top');
            }
            
            $('.user-rights [data-parent="' + actUnit + '"]').each(function(index) {
                subUnit = $(this).attr('data-unit');
                subValue = $('#unitright-' + subUnit).val();
                
                if (subValue === '' && actValue !== '') {
                    $('#unit-' + subUnit + ' .rolepicker .filter-option').html(actTitle).attr('data-top', actValue).addClass("inherit");
                    $('#unit-' + subUnit + ' button').attr('title', actTitle);
                } else if (actValue === subValue) {
                    $('#unit-' + subUnit + ' .rolepicker').selectpicker('val', '');
                    $('#unit-' + subUnit + ' .rolepicker .filter-option').html(actTitle).attr('data-top', actValue).addClass("inherit");
                    $('#unit-' + subUnit + ' button').attr('title', actTitle);
                }
                
            });
            
        });
    }
}

/**
 * buildmatrix Aufbau der Rechtetabelle mit den Einheiten
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param  {object} unit Daten der Einheit
 * @return {string} Formatiertes HTML eine Einheit darstellt
 */
function buildmatrix(unit) {
    var contentString; 
    tabIndex += 1;

    contentString = 
      '<div class="input-group" id="unit-' + unit.unitid + '" data-unit="' + unit.unitid + '" data-level="' + unit.level + (unit.member === undefined ? '"' : '" data-parent="' + unit.member + '"') + '>'
    + '  <span class="input-group-addon" data-toggle="popover" data-html="true" data-trigger="hover" data-placement="auto left" data-content="' + unit.name + '" style="' + shadeColorLinear('#c06c1c', unit.level*15) + (unit.level > 4 ? ' color: #303030;' : '') + '">';

    for (var k = 0; k <= unit.level; k++) {
        contentString += '<span class="unitSpace"></span>';
    }

    contentString +=
      unit.short + '</span>'
    + '  <select id="unitright-' + unit.unitid + '" tabindex="' + tabIndex + '" title="" class="rolepicker show-tick" data-live-search="true">'
    + '  </select>'
    + '</div>';

    return contentString;
}

/**
 * rolepickerChange Funktion mit der auf Änderungen der Rollen reagiert wird
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 */
function rolepickerChange() {
    // Bei Änderungen an den Rollen die Funktion inherit anwenden um die Darstellung der Vererbung zu erreichen
    if ( $(this).children("button").length > 0 ) {
        inherit(maxLevel, ($(this).children("button").attr('data-id')).replace('unitright-', ''));
    }
    checkChangesUser(true);
}

/**
 * rolepickerOn Überwachen der Rollen auf Änderungen
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 */
function rolepickerOn() {
    $(".user-rights").on('change', '.rolepicker', rolepickerChange);
}

/**
 * rolepickerOff Rollen Änderungen werden nicht überwacht
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 */
function rolepickerOff() {
    $(".user-rights").off('change', '.rolepicker', rolepickerChange);
}