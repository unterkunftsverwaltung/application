/* 
 Created on : 12.03.2016, 21:11:19
 Author     : Dennis
 */

/* global transfer, routeProvider, socket */

var $addNewRole = $('#addNewRole'),
    $contentInner = $('.content-inner'),
    $list = $('#tablerole tbody'),
    $removeRole = $('#removeRole'),
    $modal = $('#modalRemove'),
    deleteCandidate = Object,
    completeData = [];

// Button newRole handler
$addNewRole.on('click', function(e) {
    e.preventDefault();
    routeProvider.navigateTo('addRoleView');
});

// lade vorhandene Rollen
socket.emit('loadRoles', function(data) {
    var contentString = '';
    completeData = data;

    // Prüfen ob Rolle in Verwendung ist
    socket.emit('loadRoleIDFromUserUnits', function(idFromUserUnits) {
        $.each(data, function(index, element) {
            var entryFound = false;

            contentString =
                    '<tr id="' + element.roleID + '">' +
                    '   <td class="col-xs-7 col-sm-9 col-md-6">' +
                    '       <strong>' + element.name + '</strong>' +
                    '   </td>' +
                    '   <td class="col-xs-5 col-sm-3 col-md-6 roleactions">';

                    if (element.name !== 'Administrator' && element.name !== 'Zuschauer') {
                        contentString +=
                        '       <button class="btn btn-sm btn-default btn-edit" href="#" data-roleid="' + element.roleID + '" role="button"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span><span class="btn-txt">Bearbeiten</span></button>' +
                        '       <button class="btn btn-sm btn-danger btn-delete" href="#" data-name="' + element.name + '" data-roleid="' + element.roleID + '" role="button"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span><span class="btn-txt">Löschen</span></button>';
                    }

            contentString +=
                    '   </td>' +
                    '</tr>';
            $list.append(contentString);

            // Prüfen ob roleID in Verwendung ist und disablen der Delete-Button
            $.each(idFromUserUnits, function(index, entry) {
                if (element.roleID === entry.roleID)
                    $('#' + element.roleID + ' > .roleactions > .btn-delete').attr('disabled', true);
            });
        });
    });
});

// Button handler
$contentInner.on('click', '.btn-edit', function(e) {
    e.preventDefault();
    var id = $(this).attr('data-roleid');
    transfer.roles.editRole = id;
    routeProvider.navigateTo('editRoleView', id);
});

$contentInner.on('mousedown', '.btn-delete:disabled', function(e) {
    notifyError('Rolle ist in Verwendung und kann deswegen nicht gelöscht werden');
});

$contentInner.on('mousedown', '.btn-edit:disabled', function(e) {
    notifyError('Rolle kann nicht bearbeitet werden');
});

$contentInner.on('click', '.btn-delete', function(e) {
    e.preventDefault();
    if ($(this).attr('data-roleid') !== '1' && $(this).attr('data-roleid') !== '2') {
        $removeRole.text($(this).attr('data-name'));
        deleteCandidate = $(this);
        $modal.modal('show');
    }
});

$modal.on('click', '#deleteOkBtn', function(e) {
    
    if (deleteCandidate.attr('data-roleid') !== '1' && deleteCandidate.attr('data-roleid') !== '2') {
        socket.emit('deleteRole', deleteCandidate.attr('data-roleid'), function(result) {
            if (result) {
                notifySuccess("Rolle gelöscht ", deleteCandidate.attr('data-name') + " wurde erfolgreich gelöscht");
                $('body').removeClass('modal-open');
                $('.modal-backdrop').remove();
                $modal.modal('hide');
                routeProvider.navigateTo('roleView');
            }
        });
    }
});