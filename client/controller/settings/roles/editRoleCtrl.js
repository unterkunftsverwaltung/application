/**
 * editRoleView Controller
 *
 * @author Dennis Freitag
 * @author Rafal Welk <rafal@project.hatua.de>
 * @extends {socket, transfer, routeProvider, uvVersion}
 */

// Deklaration
var $contentInner       = $('.content-inner'),
    $backBtn            = $('#btn-back'),
    $clearBtn           = $('#clearBtn'),
    $resetBtn           = $('#resetBtn'),
    $editBtn            = $('#editBtn'),
    $title              = $('#title'),
    newRole             = Object,
    orgRole             = Object,
    totalPermissions    = null,
    roleID              = transfer.roles.editRole || actURL[4],
    tabIndex            = 1;

// Hinzufügen oder Bearbeiten?
var roleFunc            = ($('h2.page_title').text() === 'Rolle bearbeiten' ? true : false);

$(document).ready(function() {

    // Button: Zurück zur Übersicht
    $backBtn.click(function(e) {
        e.preventDefault();
        routeProvider.navigateTo('roleView');
    });

    // Button: Alle Felder leeren
    $clearBtn.click(function(e) {
        e.preventDefault();

        $(".has-error").removeClass("has-error");
        $title.val('');

        for (var i = 1; i <= totalPermissions; i++) {
            $("#" + i).prop("checked", false);
        }
        autolabelClean();
        setTimeout(function () {
            $title.focus();
        },300);
    });

    // Button: Zurücksetzen auf die ursprünglichen Werte
    $resetBtn.click(function(e) {
        e.preventDefault();

        $(".has-error").removeClass("has-error");
        $(this).prop('disabled', true);
        getOrgRole();
        setTimeout(function () {
            $title.focus();
        },100);
    });

    // Laden der Rechte aus der Datenbank und für die Darstellung vorbereiten
    // Beim Bearbeiten die Rolle zusätzlich laden
    socket.emit('loadPermissions', function(permissions) {
        totalPermissions = permissions.length;
        $.each(permissions, function(index, element) {
            $('#' + element.reach).append(buildCheckbox(element));
        });

        // Falls keine Rechte vorhanden sind: Hinweis anzeigen
        $.each(Array('global', 'unit'), function(index, element) {
            if ( $('#' + element + ' li:not(.hidden)').length === 0 ) {
                
                var list = {};
                list.description = 'Keine Rechte zur Auswahl verfügbar';
                list.permissionID = '';
                list.disabled = true;

                $('#' + element).append(buildCheckbox(list));
            }
        });

        // Rolle bearbeiten
        if (roleFunc === true) {
            socket.emit('loadRoleId', roleID, function(data) {
                if (data === null) {
                    notifyError('Diese Rolle existiert nicht');
                    routeProvider.navigateTo('roleView');
                    return;
                } else if (data.name === "Administrator" || data.name === "Zuschauer") {
                    notifyError('Diese Rolle kann nicht bearbeitet werden');
                    routeProvider.navigateTo('roleView');
                    return;
                }

                orgRole = data;
                getOrgRole();

                // Überwachen ob Änderungen durchgeführt wurden
                $contentInner.on('change', 'input', function() {
                    checkChangeRole();
                });

                $contentInner.on('keyup', 'input', function() {
                    checkChangeRole();
                });
            });
        }

        // Tabindex an die Schaltflächen weitergeben
        $("#addBtn").attr('tabindex', tabIndex + 1);
        $("#editBtn").attr('tabindex', tabIndex + 1);
        $('button[data-toggle="dropdown"]:last').attr('tabindex', tabIndex + 2);
        $("#clearBtn").attr('tabindex', tabIndex + 3);
        $("#resetBtn").attr('tabindex', tabIndex + 3);
        $("#btn-back").attr('tabindex', tabIndex + 4);
    });

    // Rolle hinzufügen
    $(".content-inner").on('click', '#addBtn, #addBackBtn', function(e) {
        e.preventDefault();
        var clicked = $(this).attr('id');

        if (checkEntryRole($title)) {
            return;
        } else {
            
            prepareAddEdit();

            socket.emit('addRole', newRole, function(result) {
                if(result.exists) {
                    roleExists(result.exists);
                    return;
                } else {
                    if (result !== undefined) {
                        notifySuccess("Rolle hinzugefügt ", "Rolle '" + newRole.name + "' wurde erfolgreich hinzugefügt");
                        if ( clicked === 'addBackBtn' ) {
                            routeProvider.navigateTo('roleView');
                        } else {
                            routeProvider.navigateTo('addRoleView');
                        }
                    }
                }
            });
        }
    });

    // Rolle bearbeiten
    $editBtn.click(function(e) {
        e.preventDefault();

        if (checkEntryRole($title)) {
            return;
        } else if (checkChangeRole()) {
            notifyWarning("Information", "Es wurden keine Änderungen durchgeführt");
            return;
        } else {
            
            prepareAddEdit();

            socket.emit('editRole', newRole, function(result) {
                if(result.exists) {
                    roleExists(result.exists);
                    return;
                } else {
                    if (result !== undefined) {
                        notifySuccess("Rolle geändert ", "Rolle '" + newRole.name + "' wurde erfolgreich geändert");
                        routeProvider.navigateTo('roleView');
                    }
                }
            });
        }
    });

});

/**
 * prepareAddEdit Erstellen der Objekt 'newRole' aus den Checkboxen und übernehmen des Namens
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 */
function prepareAddEdit() {
    // Vorbereiten des Objekts
    newRole = {
        name:           $title.val().trim(),
        permissions:    ''
    };

    if (roleFunc) newRole.roleID = roleID;

    for (var i = 1; i <= totalPermissions; i++) {
        if ( $("#" + i).prop("checked") ) {
            newRole.permissions += '1';
        } else {
            newRole.permissions += '0';
        }
    }
}

/**
 * roleExists Anzeigen der Fehlermeldung das eine Rolle mit identischen Namen oder Rechten existiert
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param  {array} result Rückgabe aus der Datenbank, die alle Rollen aufzählt die übereinstimmungen hatten
 */
function roleExists(result) {
    var errorMsg = '';
    $.each(result, function(index, element) {
        if (element.name === newRole.name) { 
            errorMsg += '<br>- Es existiert bereits eine Rolle mit diesem Namen';
            $title.parent().addClass('has-error');
        }
        if (element.permissions === newRole.permissions) { 
            errorMsg += '<br>- Es existiert bereits eine Rolle (' + element.name + ') mit diesen Rechten'; 
            $('#global, #unit').parent().addClass('has-error');
        }
    });
    notifyError(errorMsg.substring(4));
}

/**
 * getOrgRole Zurücksetzen des Namens und der Eingabefelder gem. der ursprüglichen Daten
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @extends {orgRole}
 */
function getOrgRole() {
    $title.val(orgRole.name);

    for (var i = 1; i <= totalPermissions; i++) {
        if (orgRole.permissions[i - 1] === '1') {
            $("#" + i).prop("checked", true);
        } else {
            $("#" + i).prop("checked", false);
        }
    }

    autolabelSet();
}

/**
 * checkChangeRole Überprüfen ob Änderungen durchgeführt wurden
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @return {bool} Änderungen vorhanden = true
 */
function checkChangeRole() {
    prepareAddEdit();

    if (orgRole.name !== newRole.name || orgRole.permissions !== newRole.permissions) {
        $resetBtn.prop('disabled', false);
        return false;
    } else {
        $resetBtn.prop('disabled', true);
        return true;
    }
}


/**
 * buildCheckbox Erstellen der Checkbox zum markieren
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param  {obj} element Berechtigung mit den Namen und der ID
 * @return {string} HTML Listeneintrag mit der Checkbox
 */
function buildCheckbox(element) {
    tabIndex += 1;
    var contentString =
        '<li class="list-group-item' + ((parseFloat(element.version) > parseFloat(uvVersion.number) || element.version === '') ? ' hidden' : '') + (element.disabled ? ' disabled' : '') + '">' +
           '<label>' +
                '<input tabindex="' + tabIndex + '" ' + (element.disabled ? '' : 'id="' + element.permissionID + '"') + ' name="permissions" type="checkbox" class="checkbox" value=""' + (element.disabled ? 'disabled' : '') + '> ' +
                element.description +
            '</label>' +
        '</li>';
    return contentString;
}

/** 
 * checkEntry
 * 
 * @author Dennis Freitag
 * @param {jQueryElement} e1
 * @return {boolean} gibt Ergebnis zurück
 */
function checkEntryRole(e1) {
    var error = false;

    $(".has-error").removeClass("has-error");

    if (removeHTML()) { return true; }

    if (e1.val().trim() === '') {
        error = true;
        e1.parent().addClass("has-error");
    }

    if (error) {
        notifyError('Bitte einen Namen für die Rolle eingeben');
    }

    return error;
}