# Änderungen Version 0.7
Änderungen die durchgeführt wurden nach der Projektpräsentation um den Funktionsumfang zu erweitern

## Druckfunktion
Optimierte Darstellung der Druckausgabe, dies wurde mit einer CSS Media Query für den Druck realisiert
Betroffene Dateien:

- client/assets/css/default.css
- client/assets/css/print.css
- client/index.html


## Aktualisierung des Bootstrap Datepickers
Neue Version per NPM installiert und Pfade angepasst

- client/index.html
- node_modules/bootstrap-datepicker/*
- package.json


## Suchfunktion erweitert
Unterkünfte in Gebäuden können jetzt auch nach belegten / freien Unterkünften aussortiert werden

- client/controller/accommodations/accomViewCtrl.js