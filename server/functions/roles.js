/**
 * Rollen
 * @module roles
 */


/** ********************************************************
 * load lädt alle Rollen
 * 
 * @author Dennis Freitag <dennisfreitag@gmail.com>
 * @return {string} SQL SELECT Statement
 * ********************************************************/
module.exports.load = function() {
    var sql = "SELECT * "
            + "FROM roles "
            + "ORDER BY name ASC";
    return sql;
};

/** ********************************************************
 * inUse prüft ob Rolle in Verwendung ist
 * 
 * @author Dennis Freitag <dennisfreitag@gmail.com>
 * @return {string} SQL SELECT Statement
 * ********************************************************/
module.exports.inUse = function() {
    var sql = "SELECT roleID "
            + "FROM user_units "
            + "GROUP BY roleID";
    return sql;
};

/** ********************************************************
 * loadId lädt Rolle anhand der ID
 * 
 * @author Dennis Freitag <dennisfreitag@gmail.com>
 * @param {number} id Stellt Id des zu ladenden Rolle da
 * @return {string} SQL SELECT Statement
 * ********************************************************/
module.exports.loadId = function(id) {
    var sql = "SELECT * "
            + "FROM roles "
            + "WHERE roleID = " + id;
    return sql;
};

/** ********************************************************
 * edit Editiert die zu ändernde Rolle
 * 
 * @author Dennis Freitag <dennisfreitag@gmail.com>
 * @param {Object} editedRole Enthält alle neue Daten
 * @return {string} SQL UPDATE Statement
 * ********************************************************/
module.exports.edit = function(editedRole) {
    var sql = "UPDATE roles "
            + "SET name = '" + editedRole.name + "', permissions = '" + editedRole.permissions + "' "
            + "WHERE roleID = " + editedRole.roleID;
    return sql;
};

/** ********************************************************
 * add Fügt eine neue Rolle hinzu
 * 
 * @author Dennis Freitag <dennisfreitag@gmail.com>
 * @param {Object} newRole Enthält die Daten der neuen Rolle
 * @return {String} SQL INSERT Statement
 * ********************************************************/
module.exports.add = function(newRole) {
    var sql = "INSERT INTO roles (name, permissions) "
            + "VALUES ('" + newRole.name + "', '" + newRole.permissions + "')";
    return sql;
};

/** ********************************************************
 * delete Löscht ausgewählte Rolle
 * 
 * @author Dennis Freitag <dennisfreitag@gmail.com>
 * @param {string} id der zu löschenden Rolle
 * @return {String} SQL DELETE Statement
 * ********************************************************/
module.exports.delete = function(id) {
    var sql = "DELETE FROM roles "
            + "WHERE roleID = " + id;
    return sql;
};

/**
 * exists Überprüfen ob diese Rolle bereits existiert 
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param  {object} role Übergabe des Rollennamens und der Permissions
 * @return {string} SQL SELECT Statement
 */
module.exports.exists = function(role) {
    var sql =
      "SELECT * "
    + "FROM `roles` "
    + "WHERE (`name` LIKE '" + role.name + "' OR "
    + "`permissions` LIKE '" + role.permissions + "') ";

    if (role.roleID) {
        sql +=
        "AND `roleID` <> " + role.roleID;
    }

    return sql;
}