/**
 * Gebäude
 * @module buildings
 */

/** ********************************************************
 * loadAllBuildings lädt alle Gebäude
 * 
 * @author Dennis Freitag <dennisfreitag@gmail.com>
 * @return {string} SQL SELECT Statement
 * ********************************************************/
module.exports.loadAllBuildings = function() {
    var sql = "SELECT * "
            + "FROM buildings "
            + "ORDER by number*1 ASC";
    return sql;
};

/** ********************************************************
 * loadAllRooms lädt alle Räume
 * 
 * @author Dennis Freitag <dennisfreitag@gmail.com>
 * @return {string} SQL SELECT Statement
 * ********************************************************/
module.exports.loadAllRooms = function() {
    var sql = "SELECT * "
            + "FROM room "
            + "ORDER by number*1 ASC";
    return sql;
};

/** ********************************************************
 * addBuilding speichert ein neues Gebäude
 * 
 * @author Dennis Freitag <dennisfreitag@gmail.com>
 * @param {object} newBuilding beinhaltet die Daten des neuen Gebäudes
 * @return {string} SQL INSERT Statement
 * ********************************************************/
module.exports.addBuilding = function(newBuilding) {
    var sql = "INSERT INTO buildings (number, alias, notes) "
            + "VALUES ('" + newBuilding.number + "', '" + newBuilding.alias + "', '" + newBuilding.notes + "')";
    return sql;
};

/** ********************************************************
 * loadBuilding lädt alle Gebäude
 * 
 * @author Dennis Freitag <dennisfreitag@gmail.com>
 * @param {string} id Gebäude mit dieser ID soll geladen werden
 * @return {string} SQL SELECT Statement
 * ********************************************************/
module.exports.loadBuilding = function(id) {
    var sql = "SELECT * "
            + "FROM buildings "
            + "WHERE buildingID = " + id;
    return sql;
};

/** ********************************************************
 * editBuilding Editiert die zu ändernde Rolle
 * 
 * @author Dennis Freitag <dennisfreitag@gmail.com>
 * @param {Object} newBuilding Enthält alle neue Daten
 * @return {string} SQL UPDATE Statement
 * ********************************************************/
module.exports.editBuilding = function(newBuilding) {
    var sql = "UPDATE buildings "
            + "SET number = '" + newBuilding.number + "', alias = '" + newBuilding.alias + "', notes = '" + newBuilding.notes + "' "
            + "WHERE buildingID = " + newBuilding.buildingID;
    return sql;
};

/** ********************************************************
 * loadRoomResident lädt alle Gebäude
 * 
 * @author Dennis Freitag <dennisfreitag@gmail.com>
 * @return {string} SQL SELECT Statement
 * ********************************************************/
module.exports.loadRoomResident = function() {
    var sql = "SELECT * "
            + "FROM room_resident "
            + "GROUP BY roomID";
    return sql;
};

/** ********************************************************
 * loadRoomFromBuilding Räume eines Gebäudes
 * 
 * @author Dennis Freitag <dennisfreitag@gmail.com>, angepasst durch Rafal Welk <rafal@project.hatua.de>
 * @param {string} number ID 
 * @return {string} SQL SELECT Statement
 * ********************************************************/
module.exports.loadRoomFromBuilding = function(id) {
    var sql = "SELECT room.roomID, room.number, room.maxoccupancy, room.notes, room.buildingID, room.unitID, room.useID, buildings.number AS buildingnumber, buildings.alias "
            + "FROM room "
            + "INNER JOIN `buildings` ON `room`.`buildingID` = `buildings`.`buildingID` "
            + "WHERE room.buildingID = " + id + " "
            + "ORDER BY number ASC";
    return sql;
};

/** ********************************************************
 * addRoom speichert ein neues Gebäude
 * 
 * @author Dennis Freitag <dennisfreitag@gmail.com>
 * @param {object} newRoom beinhaltet die Daten des neuen Raums
 * @return {string} SQL SELECT Statement
 * ********************************************************/
module.exports.addRoom = function(newRoom) {
    var sql = "INSERT INTO room (number, maxoccupancy, notes, buildingID, unitID, useID) "
            + "VALUES ('" + newRoom.number + "', '" + newRoom.maxoccupancy + "', '" + newRoom.notes 
            + "', '" + newRoom.buildingID + "', '" + newRoom.unitID + "', '" + newRoom.useID + "')";
    return sql;
};

/** ********************************************************
 * loadRoom lädt Raum anhand der ID
 * 
 * @author Dennis Freitag <dennisfreitag@gmail.com>
 * @author Rafal Welk <rafal@project.hatua.de> (Anpassungen)
 * @param {string} id Gebäude mit dieser ID soll geladen werden
 * @return {string} SQL SELECT Statement
 * ********************************************************/
module.exports.loadRoom = function(id) {
    var sql = 
          "SELECT `room`.`roomID`, `number`, `maxoccupancy`, `notes`, `buildingID`, `unitID`, `useID`, COUNT(`residentID`) AS `residents` "
        + "FROM `room` "
        + "INNER JOIN `room_resident` ON `room_resident`.`roomID` =  `room`.`roomID` "
        + "WHERE `room`.`roomID` = " + id;

    return sql;
};

/** ********************************************************
 * loadRoomsByBuildingID lädt Raum anhand der ID
 * 
 * @author Dennis Freitag <dennisfreitag@gmail.com>
 * @param {string} buildingID Räume mit dieser GebäudeID sollen geladen werden
 * @return {string} SQL SELECT Statement
 * ********************************************************/
module.exports.loadRoomsByBuildingID = function(buildingID) {
    var sql = "SELECT * "
            + "FROM room "
            + "WHERE buildingID = " + buildingID;
    return sql;
};

/** ********************************************************
 * editRoom Editiert die zu ändernde Rolle
 * 
 * @author Dennis Freitag <dennisfreitag@gmail.com>
 * @param {Object} newRoom Enthält alle neue Daten
 * @return {string} SQL UPDATE Statement
 * ********************************************************/
module.exports.editRoom = function(newRoom) {
    var sql = "UPDATE room "
            + "SET number = '" + newRoom.number + "', maxoccupancy = " + newRoom.maxoccupancy + ", notes = '" + newRoom.notes + "' " + ", buildingID = " + newRoom.buildingID + ", unitID = " + newRoom.unitID + ", useID = " + newRoom.useID + " "
            + "WHERE roomID = " + newRoom.roomID;
    return sql;
};

/** ********************************************************
 * deleteBuilding Editiert die zu ändernde Rolle
 * 
 * @author Dennis Freitag <dennisfreitag@gmail.com>
 * @param {Object} id Enthält alle neue Daten
 * @return {string} SQL UPDATE Statement
 * ********************************************************/
module.exports.deleteBuilding = function(id) {
    var sql = "DELETE FROM buildings "
            + "WHERE buildingID = " + id;
    return sql;
};

/** ********************************************************
 * deleteRoom Editiert die zu ändernde Rolle
 * 
 * @author Dennis Freitag <dennisfreitag@gmail.com>
 * @param {Object} id Enthält alle neue Daten
 * @return {string} SQL UPDATE Statement
 * ********************************************************/
module.exports.deleteRoom = function(id) {
    var sql = "DELETE FROM room "
            + "WHERE roomID = " + id;
    return sql;
};