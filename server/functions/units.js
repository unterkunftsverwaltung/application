/**
 * Einheiten
 * @module units
 */


/**
 * nodesearch Funktion um im TreeView nach Eltern (übergeordnete Einheit) zu suchen
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param  {array} arrelement Einheiten (Objekte) in einem Array
 * @param  {int} searchkey  UnitID nach welcher gesucht wird
 * @return {object} Gesuchte Einheit oder undefined (nicht gefunden)
 */
module.exports.nodesearch = function(arrelement, searchkey) {
    var obj = arrelement.filter(function(obj) {
        return obj.id == searchkey;
    })[0];

    if (obj !== undefined) {
        return obj;
    } else {
        var obj;
        arrelement.forEach(function(element, index) {
            if (element.nodes) {
                if (obj === undefined) {
                    obj = module.exports.nodesearch(element.nodes, searchkey);
                }
            } 
        });
        return obj;
    } 
}

/**
 * buildUnitsTree Erstellen der Einheiten Hierarchie 
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param  {array} parents  Einheiten ohne übergeordnete Einheit
 * @param  {array} children Alle untergeordneten Einheiten
 * @return {array} Einheiten mit entsprechender Gliederung (untergeordnete Einheiten in Nodes)
 */
module.exports.buildUnitsTree = function(parents, children, onlySelectable) {
    var unitstree = new Array(),
        childrenSort = new Array(),
        orderSearch = new Array(),
        orderNext = new Array(),
        selectable = new Array();

    // Auswählbare Einheiten
    if (onlySelectable !== '') {
        onlySelectable += ',';
        selectable = onlySelectable.split(',');
    }

    /** Übernehmen der Einheiten ohne übergeordnete Einheiten */
    parents.forEach(function(element) {
        // Einheit (Objekt erstellen)
        var unit = new Object;
        unit.text = element.name;
        unit.id = element.unitid;
        unit.tags = new Array;
        unit.tags.push(element.short);
        if (onlySelectable !== '') {
            unit.selectable = false;
            selectable.forEach(function(select) {
                if (parseInt(select) == parseInt(element.unitid)) { unit.selectable = true; }
            });

            if (unit.selectable === false) {
                unit.color = '#aaa';
                unit.backColor = '#f2f2f2';
            }
        }

        unitstree.push(unit);
        orderSearch.push(unit.id);
    });
    
    /** Sortieren, um die Abhängigkeiten der Einheiten sicherzustellen */
    do {
        var orderNext = new Array();
        orderSearch.forEach(function(sortiert) {
            var childrenNext = new Array();
            children.forEach(function(element) {
                if (sortiert == element.member) {
                    childrenSort.push(element);
                    orderNext.push(element.unitid);
                } else {
                    childrenNext.push(element);
                }
            });
            children = childrenNext;
        });
        orderSearch = orderNext;
    } while (orderNext.length > 0);


    /** Laden aller untergeordneten Einheiten */
    childrenSort.forEach(function(element, index, c) {
        // Einheit (Objekt erstellen)
        var unit = new Object;
        unit.text = element.name;
        unit.id = element.unitid;
        unit.tags = new Array;
        unit.tags.push(element.short);
        if (onlySelectable !== '') {
            unit.selectable = false;
            selectable.forEach(function(select) {
                if (parseInt(select) == parseInt(element.unitid)) { unit.selectable = true; }
            });

            if (unit.selectable === false) {
                unit.color = '#aaa';
                unit.backColor = '#f2f2f2';
            }
        }

        // Suchen nach der übergeordneten Einheit (Parent)
        var parent = module.exports.nodesearch(unitstree, element.member);

        // Node ggf. anlegen
        if (!parent.nodes) {
            parent.nodes = new Array();
        }

        // Anhängen der untergeordneten Einheit
        parent.nodes.push(unit);
    });

    return unitstree; 
}

/**
 * addnullUnitsTree Hinzufügen des Eintrags 'Keine'
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param  {object} tree buildUnitsTree Objekt das erweitert werden soll um den Eintrag 'Keine'
 */
module.exports.addnullUnitsTree = function(tree) {
    var unit = {
        text: "Keine",
        id: 'NULL',
        icon: 'glyphicon glyphicon-remove'
    }
    tree.unshift(unit);
    return;
}

/**
 * addnoUnitsTree Hinzufügen des Eintrags 'Keine Einheiten vorhanden'
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param  {object} tree buildUnitsTree Objekt das erweitert werden soll um den Eintrag 'Keine Einheiten vorhanden'
 */
module.exports.addnoUnitsTree = function(tree) {
    var unit = {
        text: "Keine Einheiten vorhanden",
        id: 'NULL',
        icon: 'glyphicon glyphicon-remove',
        state: {
            disabled: true
        }
    }
    tree.unshift(unit);
    return;
}

/**
 * allUnits Alle Einheiten
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @return {string} SQL SELECT Statement
 */
module.exports.allUnits = function() {
    var sql = 
      'SELECT * '
    + 'FROM `units` '
    + 'ORDER BY `member` ASC';
    
    return sql;
}

/**
 * withoutParents Einheiten die keine übergeordnete Einheit haben
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @return {string} SQL SELECT Statement
 */
module.exports.withoutParents = function() {
    var sql = 
      'SELECT unitid, name, short '
        + ' , (SELECT COUNT(*)  '
        + '    FROM `room` '
        + '    WHERE units.unitID = room.unitID) AS rooms '
        + ' , (SELECT COUNT(*)  '
        + '    FROM `residents` '
        + '    WHERE units.unitID = residents.unitID) AS residents '
    + 'FROM `units` '
    + 'WHERE member IS NULL '
    + 'ORDER BY name ASC';
    
    return sql;
}

/**
 * children Einheiten die eine übergeordnete Einheit haben
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @return {string} SQL SELECT Statement
 */
module.exports.children = function() {
    /*var sql =
      'SELECT units.unitid AS unitid, units.name AS name, units.short AS short, units.member AS member, parent.name AS memberName, parent.short AS memberShort '
    + 'FROM `units` '
    + 'INNER JOIN `units` AS parent ON units.member = parent.unitid '
    + 'ORDER BY units.member, units.name ASC';
    */
   
    var sql = 
      'SELECT units.unitid AS unitid, units.name AS name, units.short AS short, units.member AS member, parent.name AS memberName, parent.short AS memberShort '
        + ' , (SELECT COUNT(*)  '
        + '    FROM `room` '
        + '    WHERE units.unitID = room.unitID) AS rooms '
        + ' , (SELECT COUNT(*)  '
        + '    FROM `residents` '
        + '    WHERE units.unitID = residents.unitID) AS residents '
    + 'FROM `units` '
    + 'INNER JOIN `units` AS parent ON units.member = parent.unitid '
    + 'ORDER BY units.member, units.name ASC '

    return sql;
}

/**
 * exists Überprüfen ob diese Einheitsbezeichnung (Kurz und/oder Langform) auf
 *   gleicher Ebene existiert
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param  {object} unit Übergabe der zu kontrollierenden Daten
 *                       unit.name Bezeichnung der Einheit
 *                       unit.short Kurzbezeichnung der Einheit
 *                       unit.member unitID der übergeordneten Einheit
 * @return {string} SQL SELECT Statement
 */
module.exports.exists = function(unit) {
    var sql =
      'SELECT COUNT(*) AS existing FROM `units` '
    + "WHERE (`name` LIKE '" + unit.name + "' OR `short` LIKE '" + unit.short + "') AND `member` = " + unit.member + " ";

    if (unit.unitID != undefined) {
        sql += "AND `unitID` != " + unit.unitID + " "
    }

    sql += 'ORDER BY `unitID` ASC; '

    if (unit.member == 'NULL') {
        sql = sql.replace("=", "IS")
    }

    return sql;
}

/**
 * add Hinzufügen einer neuen Einheit
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param  {object} unit Übergabe der einzutragenden Daten
 *                       unit.name Bezeichnung der Einheit
 *                       unit.short Kurzbezeichnung der Einheit
 *                       unit.member unitID der übergeordneten Einheit
 * @return {string} SQL INSERT INTO Statement
 */
module.exports.add = function(unit) {
    var sql =
      'INSERT INTO `units` (`name`, `short`, `member`) '
    + "VALUES ('" + unit.name + "', '" + unit.short + "', " + unit.member + ");"

    return sql;
}

/**
 * edit Bearbeiten einer Einheit
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param  {object} unit Übergabe der einzutragenden Daten
 *                       unit.name Bezeichnung der Einheit
 *                       unit.short Kurzbezeichnung der Einheit
 *                       unit.member unitID der übergeordneten Einheit
 *                       unit.unitID Einheit die bearbeitet werden soll
 * @return {string} SQL UPDATE Statement
 */
module.exports.edit = function(unit) {
    var sql =
      'UPDATE `units` SET '
    + "`name` = '" + unit.name + "', "
    + "`short` = '" + unit.short + "', "
    + "`member` = '" + unit.member + "' "
    + "WHERE `unitID` = " + unit.unitID + ";"

    if (unit.member == 'NULL') {
        sql = sql.replace("'NULL'", "NULL");
    }

    return sql;
}


/**
 * delete Löschen einer einzelnen Einheit
 *
 * @description Einheiten können nur gelöscht werden, wenn sie nicht in Verwendung sind dazu gehören:
 *               - Räume / Unterkünfte [rooms]   
 *               - Bewohner [residents] 
 *               - Einheiten [units] als übergeordnete Einheit [Feld member bei anderen Einheiten]
 *              Rechte die über die Tabelle [user_units] existieren werden automatisch gelöscht
 * 
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param {int} id unitID der zu löschenden Einheit
 * @return {string} SQL DELETE Statement
 */
module.exports.delete = function(id) {
    var sql = 
      'DELETE FROM `units` '
    + 'WHERE `unitID` = ' + id + ' '
    + 'LIMIT 1;'

    return sql;
}

/**
 * show Abrufen einer einzelnen Einheit
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param {int} id unitID der benötigten Einheit
 * @return {string} SQL SELECT Statement
 */
module.exports.show = function(id) {
    var sql =
      'SELECT * '
    + 'FROM units '
    + 'WHERE unitID = ' + id;

    return sql;
}