/**
 * Berechtigungen
 * @module rights
 */


/**
 * getRights Ermitteln der Benutzerberechtigungen
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param  {array} parentUnits Übergeordnete Einheiten
 * @param  {array} units       Untergeordnete Einheiten
 * @param  {array} roles       Verfügbare Rollen mit Berechtigungen (Binärzahl)
 * @param  {array} permissions Berechtigungen mit Kurznamen
 * @return {obj} Rückgabe der Berechtigungen für den Benutzer
 */
module.exports.getRights = function(parentUnits, units, roles, permissions, callback) {

	var parents = [],
		childSearch = [],
		totalunits = [],
		lookup = {},
     	override_permission = '',
     	objRights = {},
        objViews = {};

    // Ermitteln der übergeordneten Einheiten
	parentUnits.forEach(function(parentElement) {
        parentElement.level = 0;
        parents.push(parentElement);
        childSearch.push(parentElement);
        totalunits.push(parentElement);
    });

	// Ermitteln aller untergeordneten Einheiten nach Level
    do {
        var nextSearch = [];

        childSearch.forEach(function(above) {
            var nextUnits = [];

            units.forEach(function(below) {

                if (above.unitid === below.member) {

                    // Als möglicher Elternteil für den nächsten Suchlauf eintragen
                    nextSearch.push(below);

                    // Level und ParentName anfügen
                    below.level = above.level + 1;                 
                    below.parent = above.name;

                    // Eintragen in die Liste
                    totalunits.push(below);
                    
                } else {
                    // Da kein Elternteil muss es sich um eine untergeordnete Einheit handeln
                    // diese für den nächsten Durchgang eintragen
                    nextUnits.push(below);
                }
            });

            units = nextUnits;
        });

        childSearch = nextSearch;
    } while (nextSearch.length > 0);

    // Hilfsfunktion im Array für die Einheiten
    for (var i = 0; i < totalunits.length; i++) {
    	lookup[totalunits[i].unitid] = i;
    }

    // Wenn keine Einheiten existieren
    // um die globalen Rechte zu ermitteln
    if (totalunits.length === 0) {
        var nounits = {};

        nounits.level = 0;
        totalunits.push(nounits);
    }

    // Eintragen aller Rollen dieses Nutzers
    roles.forEach(function(role) {
    	if (role.unitID === null) {
    		override_permission = role.permissions;
    	} else {
    		var arrunit = lookup[role.unitID];
    		totalunits[arrunit].permission = role.permissions;
    	}
    });

    // Übernehmen aller Permissions
	totalunits.forEach(function(uunit) {

		if (uunit.level === 0 && !uunit.permission) {
			// Übernehmer der Permission für alle Einheiten
			uunit.permission = override_permission;

		} else if (!uunit.permission && uunit.level > 0) {
			// Übernehmen der Permission der übergeordneten Einheit
			var arrunit = lookup[uunit.member];
			uunit.permission = totalunits[arrunit].permission;
		}
	})

	// Übernehmen der Berechtigungen gem. der Rollen und Einheiten
    totalunits.forEach(function(uunit) {

    	for (var i = 0; i < (uunit.permission).length; i++) {
			if ((uunit.permission).substring(i, i + 1) === '1') {

				if (permissions[i].reach === 'global') {
					objRights[permissions[i].short] = true;
				} else {
					if (uunit.unitid !== undefined) {
                        
                        // Berechtigungen mit Einheiten versehen
                        if (objRights[permissions[i].short] === undefined) {
                            objRights[permissions[i].short] = uunit.unitid;
                        } else {
                            objRights[permissions[i].short] += ',' + uunit.unitid;
                        }
                    }
				}
			}
		}
    });

    // Anzeige der Grundmodule und der Bewohner
    totalunits.forEach(function(uunit) { 
        if (uunit.permission !== '') {
            if (objRights['view'] === undefined) {
                objRights['view'] = uunit.unitid;
            } else {
                objRights['view'] += ',' + uunit.unitid;
            }
        }
    });

    if (objRights['view'] === undefined) {
        objRights['view'] = true;
    }

    return objRights;
}


/**
 * permissions Alle aktuellen Berechtigungen
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @return {string} SQL SELECT Statement
 */
module.exports.permissions = function() {
	var sql = 
      'SELECT * '
    + 'FROM `permissions` '
    + 'ORDER BY `permissionID` ASC ';

    return sql;
}

/**
 * sidebar Erstellen der Menüeinträge je nach Berechtigung
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param  {obj}    data   JSON sidebar.json mit den Einträgen
 * @param  {array}  rights Berechtigungen des Benutzers zum abgleich
 * @return {obj} Bereinigte Menüstruktur, die der Nutzer sehen darf
 */
module.exports.sidebar = function(data, rights, callback) {
    for (element in data) {

        if (data[element].collapse) {
            // Verschachtelte Menüs auf Rechte überprüfen
            // Keine Rechte vorhanden, dann den kompletten Menüpunkt löschen
            for(sub in data[element].collapse) {
                var thispermission = [], 
                    found = 0;

                thispermission = (data[element].collapse[sub].permissions).split("|");
                
                // Überprüfen ob min. 1x Berechtigung zutrifft                                        
                for (var i = 0; i < thispermission.length; i++) {
                    if (rights[thispermission[i]]) found++;
                }

                // Sollte keine Berechtigung zutreffen, Menüpunkt löschen
                if (found === 0) {
                    delete data[element].collapse[sub];
                }
            }

            if (Object.keys(data[element].collapse).length === 0) {
                delete data[element];
            }
        } else {
            // Menüs auf der oberen Ebene ohne Verschachtelung überprüfen
            var thispermission = [], 
                found = 0;

            thispermission = (data[element].permissions).split("|");
            
            // Überprüfen ob min. 1x Berechtigung zutrifft 
            for (var i = 0; i < thispermission.length; i++) {
                if (rights[thispermission[i]]) found++;
            }

            // Sollte keine Berechtigung zutreffen, Menüpunkt löschen
            if (found === 0) {
                delete data[element];
            }
        }
    }

    callback(data);
}