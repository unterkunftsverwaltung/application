/**
 * roomBook
 * @module roomBook
 * 
 */

/** ********************************************************
 * loadAllResidents lädt alle Bewohner 
 * 
 * @author Dennis Freitag <dennisfreitag@gmail.com>
 * @return {string} SQL SELECT Statement
 * ********************************************************/
module.exports.loadAllResidents = function () {
    var sql = "SELECT * "
            + "FROM residents "
            + "ORDER BY surname, forename ASC";
    return sql;
};

/** ********************************************************
 * loadResident lädt alle Bewohner 
 * 
 * @author Dennis Freitag <dennisfreitag@gmail.com>
 * @param {int} id id des Bewohners, welcher zugewiesen werden soll
 * @return {string} SQL SELECT Statement
 * ********************************************************/
module.exports.loadResident = function (id) {
    var sql = "SELECT * "
            + "FROM residents "
            + "WHERE residentID = " + id + " "
            + "ORDER BY surname, forename ASC";
    return sql;
};

/** ********************************************************
 * bookResident fügt eine Buchung der Datenbank hinzu
 * 
 * @author Dennis Freitag <dennisfreitag@gmail.com>
 * @param {object} booking Behinhaltet die Daten für eine Buchung
 * @return {string} SQL INSERT Statement
 * ********************************************************/
module.exports.bookResident = function(booking) {
    var sql = "INSERT INTO room_resident (roomID, residentID, until) "
            + "VALUES ('" + booking.roomID + "', '" + booking.residentID + "' , ";

    if (booking.until === '') {
        sql += "NULL)";
    } else {
        sql += "'" + booking.until + "')";
    }
            
    return sql;
};
/**
 * loadRoomResidentCount lädt Anzahl der Räume in room_resident
 * 
 * @author Dennis Freitag <dennisfreitag@gmail.com>
 * @param {type} roomID
 * @returns {module.exports.loadRoomResidentCount.sql|String}
 */
module.exports.loadRoomResidentCount = function(roomID) {
    var sql = "SELECT COUNT(*) as anzahl "
            + "FROM room_resident "
            + "WHERE roomID = " + roomID;
    return sql;
};

/**
 * loadResidentsFromRoom lädt BewohnerID aus room_resident
 * 
 * @author Dennis Freitag <dennisfreitag@gmail.com>
 * @param {type} roomID
 * @returns {String|module.exports.loadResidentsFromRoom.sql}
 */
module.exports.loadResidentsFromRoom = function(roomID) {
    var sql = "SELECT residentID "
            + "FROM room_resident "
            + "WHERE roomID = " + roomID;
    return sql;
};

/**
 * loadAllRoomResident lädt alle Einträge aus room_resident
 * 
 * @author Dennis Freitag <dennisfreitag@gmail.com>
 * @returns {module.exports.loadAllRoomResident.sql|String}
 */
module.exports.loadAllRoomResident = function() {
    var sql = "SELECT * "
            + "FROM room_resident ";
    return sql;
};

/**
 * loadAllRoomsAndBuildings
 * 
 * @author Dennis Freitag <dennisfreitag@gmail.com>
 * @returns {module.exports.loadAllRoomsAndBuildings.sql|String}
 */
module.exports.loadAllRoomsAndBuildings = function() {
    var sql = "SELECT room.roomID, room.number as roomNumber, room.maxoccupancy, room.unitID, buildings.buildingID, buildings.number as buildingNumber "
            + "FROM room "
            + "INNER JOIN buildings "
            + "ON room.buildingID = buildings.buildingID ";
    return sql;
};

/**
 * loadEmptyRooms
 * 
 * @author Dennis Freitag <dennisfreitag@gmail.com>
 * @returns {module.exports.loadEmptyRooms.sql|String}
 */
module.exports.loadEmptyRooms = function() {
    var sql = "SELECT room.roomID, room.number as roomNumber, room.maxoccupancy, room.unitID, buildings.buildingID, buildings.number as buildingNumber "
            + "FROM room "
            + "INNER JOIN buildings ON buildings.buildingID = room.buildingID "
            + "WHERE room.roomID NOT IN ("
            + "SELECT roomID "
            + "FROM room_resident)";
    return sql;
};

/**
 * loadRoomsByGender
 * 
 * @author Dennis Freitag <dennisfreitag@gmail.com>
 * @param {type} gender
 * @returns {module.exports.loadRoomsByGender.sql|String}
 */
module.exports.loadRoomsByGender = function(resident) {
    var sql = "SELECT room.roomID "
            + "FROM room "
            + "INNER JOIN room_resident "
            + "ON room.roomID = room_resident.roomID "
            + "INNER JOIN residents "
            + "ON room_resident.residentID = residents.residentID "
            + "WHERE residents.sex = '" + resident.sex + "' AND NOT residents.residentID = '" + resident.residentID + "'";
    return sql;
};

/**
 * fillInfo
 * 
 * @author Dennis Freitag <dennisfreitag@gmail.com>
 * @param {type} roomID
 * @returns {module.exports.fillInfo.sql|String}
 */
module.exports.fillInfo = function(roomID) {
    var sql = "SELECT residents.residentID, residents.forename, residents.surname, residents.unitID, residents.sex, DATE_FORMAT(`until`,'%d.%m.%Y') AS `until`, room.notes "
            + "FROM residents "
            + "INNER JOIN room_resident ON residents.residentID = room_resident.residentID "
            + "INNER JOIN room ON room_resident.roomID = room.roomID "
            + "WHERE room.roomID = " + roomID;
    return sql;
};

/**
 * usedRooms
 * 
 * @author Dennis Freitag <dennisfreitag@gmail.com>
 * @returns {module.exports.usedRooms.sql|String}
 */
module.exports.usedRooms = function() {
    var sql = "SELECT room.roomID, room.number as roomNumber, room.maxoccupancy, room.unitID, buildings.buildingID, buildings.number as buildingNumber, COUNT(room.maxoccupancy) as currentUsing "
            + "FROM room INNER JOIN room_resident ON room_resident.roomID = room.roomID "
            + "INNER JOIN buildings ON buildings.buildingID = room.buildingID "
            + "GROUP BY room.roomID";
    
    return sql;
};
