/**
 * Benutzer
 * @module users
 */

/**
 * load Alle Benutzer einlesen
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @return {string} SQL SELECT Statement
 */
module.exports.load = function() {
	var sql = 
      'SELECT * '
    + 'FROM `user` '
    + 'ORDER BY `username` ASC ';

    return sql;
}

/**
 * exists Überprüfen ob der Benutzername existiert
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param  {object} user Übergabe der zu kontrollierenden Daten
 *                       user.username Benutzername
 * @return {string} SQL SELECT Statement
 */
module.exports.exists = function(user) {
    var sql =
      'SELECT COUNT(*) AS existing FROM `user` '
    + "WHERE `username` LIKE '" + user.username + "' ";

    if (user.userID !== undefined) {
        sql += "AND `userID` != " + user.userID + " "
    }

    return sql;
}

/**
 * add Hinzufügen eines neuen Benutzers
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param  {object} user Übergabe der einzutragenden Daten
 *                       user.username Benutzername
 *                       user.forename Vorname
 *                       user.surname Nachname
 *                       user.rank RankID
 *                       user.password Passwort
 * @return {string} SQL INSERT INTO Statement
 */
module.exports.add = function(user) {
    var sql =
      'INSERT INTO `user` (`username`, `forename`, `surname`, `rankID`, `password`) '
    + "VALUES ('" + user.username + "', '" + user.forename + "', '" + user.surname + "', '" + user.rank + "', MD5('" + user.password + "'));"

    return sql;
}

/**
 * edit Bearbeiten eines Benutzers
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param  {object} user Übergabe der einzutragenden Daten
 *                       user.userID Benutzernummer
 *                       user.username Benutzername
 *                       user.forename Vorname
 *                       user.surname Nachname
 *                       user.rank RankID
 *                       user.password ggf. neues Passwort wenn diese nicht leer ist
 * @return {string} SQL UPDATE Statement
 */
module.exports.edit = function(user) {
    var sql =
      'UPDATE `user` SET ';

    if (user.editFunc !== 'no-user') {
        sql +=
          "`username` = '" + user.username + "', "
        + "`forename` = '" + user.forename + "', "
        + "`surname` = '" + user.surname + "', "
        + "`rankID` = '" + user.rank + "' ";
    }

    if (user.editFunc !== 'no-reset' && user.password !== '' ) {
      if (user.editFunc !== 'no-user') {
        sql += ", ";
      }
      sql += "`password` = MD5('" + user.password + "') ";
    }

    sql += "WHERE `userID` = " + user.userID;

    return sql;
}

/**
 * getID Ermitteln der userID nach dem Benutzernamen
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param  {object} user Übergabe der zu kontrollierenden Daten
 *                       user.username Benutzername
 * @return {string} SQL SELECT Statement
 */
module.exports.getID = function(user) {
    var sql =
      'SELECT `userID` FROM `user` '
    + "WHERE `username` LIKE '" + user.username + "'";

    return sql;
}

/**
 * show Abrufen eines einzelnen Benutzers
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param {int} id userID des benötigten Benutzers
 * @return {string} SQL SELECT Statement
 */
module.exports.show = function(id) {
    var sql =
      'SELECT * '
    + 'FROM `user` '
    + 'WHERE `userID` = ' + id;

    return sql;
}

/**
 * delete Löschen eines einzelnen Benutzers
 *
 * @description Benutzer werden mit sämtlichen Rechten entfernt
 *              Rechte die über die Tabelle [user_units] existieren werden automatisch gelöscht
 * 
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param {int} id unitID des zu löschenden Benutzers
 * @return {string} SQL DELETE Statement
 */
module.exports.delete = function(id) {
    var sql = 
      'DELETE FROM `user` '
    + 'WHERE `userID` = ' + id + ' '
    + 'LIMIT 1;'

    return sql;
}

/**
 * roleAdd Hinzufügen der Rollen eines Benutzers
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param  {string} user Benutzername als userID
 * @param  {object} role Übergabe der einzutragenden Daten
 *                       role.unit ID der Einheit
 *                       role.role ID der Rolle
 * @return {string} SQL INSERT INTO Statement
 */
module.exports.roleAdd = function(user, role) {
    var sql =
      'INSERT INTO `user_units` (`userID`, `unitID`, `roleID`) '
    + "VALUES ('" + user + "', '" + role.unit + "', '" + role.role + "');"

    if (role.unit == 'NULL') {
        sql = sql.replace("'NULL'", "NULL");
    }

    return sql;
}

/**
 * roleDel Löschen aller Rollen eines Benutzers
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param  {string} user Benutzerdaten inkl. der ID
 * @return {string} SQL DELETE Statement
 */
module.exports.roleDel = function(user) {
    var sql =
      'DELETE FROM `user_units` WHERE `userID` = ' + user.userID;

    return sql;
}

/**
 * roleShow Abrufen der Rollen eines einzelnen Benutzers
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param {int} id userID des benötigten Benutzers
 * @return {string} SQL SELECT Statement
 */
module.exports.roleShow = function(id) {
    var sql =
      'SELECT * '
    + 'FROM `user_units` '
    + 'WHERE `userID` = ' + id + ' '
    + 'ORDER BY `unitID` ASC';

    return sql;
}

/**
 * check Überprüfen ob Benutzername und Passwort existieren
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param {obj} user Benutzername und Passwort
 * @return {string} SQL SELECT Statement
 */
module.exports.check = function(user) {
    var sql =
      "SELECT `user`.`userID`, `user`.`username`, `user`.`forename`, `user`.`surname`, `ranks`.`label` AS `rank`, `ranks`.`short`, `user`.`rankID` "
    + "FROM `user` "
    + "INNER JOIN `ranks` ON `user`.`rankID` = `ranks`.`rankID` "
    + "WHERE `username` LIKE '" + user.username + "' AND `password` LIKE '" + user.md5password + "' ";

    return sql;
}

/**
 * roleShow Abrufen der Rollen eines einzelnen Benutzers
 *          + mit Berechtigungen
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param {int} id userID des benötigten Benutzers
 * @return {string} SQL SELECT Statement
 */
module.exports.roleShowPerm = function(id) {
    var sql =
      'SELECT `user_units`.`userID`, `user_units`.`unitID`, `user_units`.`roleID`, `roles`.`name`, `roles`.`permissions` '
    + 'FROM `user_units` '
    + 'INNER JOIN `roles` ON `roles`.`roleID` = `user_units`.`roleID` '
    + 'WHERE `user_units`.`userID` = ' + id + ' '
    + 'ORDER BY `unitID` ASC ';

    return sql;
}