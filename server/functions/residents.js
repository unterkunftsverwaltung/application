/**
 * Bewohner
 * @module residents
 */

/**
 * load Alle Bewohner einlesen (die der Benutzer sehen darf)
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param {string} view unitIDs mit Komma getrennt die der Benutzer sehen darf
 * @return {string} SQL SELECT Statement
 */
module.exports.load = function(order, view) {
	var sql = 
	  'SELECT `residentID`, `forename`, `surname`, `sex`, `notes`, `residents`.`rankID`, `ranks`.`label` AS `ranklabel`, `ranks`.`short` AS `rankshort`, `residents`.`unitID`, `units`.`name` AS `unitname`, `units`.`short` AS `unitshort`, '
		+   '(SELECT COUNT(*) '
 		+    'FROM `keys` '
 		+    'WHERE `residents`.`residentID` = `keys`.`residentID`) AS `keys` '   
	+ 'FROM `residents` '
	+ 'INNER JOIN `ranks` ON `residents`.`rankID` = `ranks`.`rankID` '
	+ 'INNER JOIN `units` ON `residents`.`unitID` = `units`.`unitID` '
	+ 'WHERE `residents`.`unitID` IN (' + view + ') ';
	
	if (order === 'resident') { 
		sql += 'ORDER BY `surname` ASC, `forename` ASC ';
	} else if (order === 'unit') {
		sql += 'ORDER BY `residents`.`unitID` ASC, `surname` ASC, `forename` ASC ';
	}

    return sql;
}

/**
 * allrooms Alle Räume mit Bewohnern einlesen (die der Benutzer sehen darf)
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param {string} view unitIDs mit Komma getrennt die der Benutzer sehen darf
 * @return {string} SQL SELECT Statement
 */
module.exports.allrooms = function(view) {
	var sql = 
	  'SELECT `rrID`, `room_resident`.`residentID`, `room_resident`.`roomID`, `buildings`.`buildingID`, `buildings`.`number` AS `buildingnumber`, `buildings`.`alias`, `room`.`number` AS `roomnumber`, `room`.`maxoccupancy`, `buildings`.`notes` AS `buildingnotes`, `room`.`notes` AS `roomnotes`, `room`.`unitID` AS `roomunit`, `residents`.`unitID` AS `residentunit`, DATE_FORMAT(`until`,"%d.%m.%Y") AS `until` '
	+ 'FROM `room_resident` '
	+ 'INNER JOIN `room` ON `room_resident`.`roomID` = `room`.`roomID` '
	+ 'INNER JOIN `buildings` ON `room`.`buildingID` = `buildings`.`buildingID` '
	+ 'INNER JOIN `residents` ON `residents`.`residentID` = `room_resident`.`residentID` '
	+ 'WHERE `residents`.`unitID` IN (' + view + ') '
	+ 'ORDER BY `residentID` ASC ';

    return sql;
}

/**
 * exists Feststellen ob ein Bewohner mit dem gleichen Namen existiert
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param {string} resident Bewohner der angelegt werden soll
 * @return {string} SQL SELECT Statement
 */
module.exports.exists = function(resident) {
	var sql = 
	  'SELECT * '
	+ 'FROM `residents` '
	+ "WHERE `forename` LIKE '" + resident.forename + "' AND `surname` LIKE '" + resident.surname + "' AND `sex` LIKE '" + resident.sex + "' AND `unitID` = " + resident.unitID + " AND ";

	if (resident.notes === '') {
		sql += "(`notes` LIKE '" + resident.notes + "' OR `notes` IS NULL) ";
	} else {
		sql += "`notes` LIKE '" + resident.notes + "' ";
	}

	if (resident.residentID) {
		sql += 
		"AND `residentID` <> " + resident.residentID;
	}

    return sql;
}

/**
 * add Hinzufügen eines neuen Bewohners
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param  {object} resident Übergabe der einzutragenden Daten
 * @return {string} SQL INSERT INTO Statement
 */
module.exports.add = function(resident) {
    var sql =
      'INSERT INTO `residents` (`forename`, `surname`, `rankID`, `sex`, `notes`, `unitID`) '
    + "VALUES ('" + resident.forename + "', '" + resident.surname + "', '" + resident.rankID + "', '" + resident.sex + "', '" + resident.notes + "', '" + resident.unitID + "');"

    return sql;
}

/**
 * loadResident Auslesen eines Bewohners
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param  {int} resident Übergabe der residentID
 * @return {string} SQL SELECT Statement
 */
module.exports.loadResident = function(resident) {
    var sql =
      'SELECT * '
	+ 'FROM `residents` '
	+ 'WHERE `residentID` = ' + resident;

    return sql;
}

/**
 * edit Bearbeiten eines Bewohners
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param  {object} resident Übergabe der einzutragenden Daten
 * @return {string} SQL UPDATE Statement
 */
module.exports.edit = function(resident) {
    var sql =
      'UPDATE `residents` SET '
    + "`forename` = '" + resident.forename + "', "
    + "`surname` = '" + resident.surname + "', "
    + "`rankID` = '" + resident.rankID + "', "
    + "`sex` = '" + resident.sex + "', "
    + "`notes` = '" + resident.notes + "', "
    + "`unitID` = '" + resident.unitID + "' "
	+ "WHERE `residentID` = " + resident.residentID;

    return sql;
}

/**
 * delete Löschen eines Bewohners (Räume werden per ON DELETE entfernt)
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param  {int} resident Übergabe der residentID Daten
 * @return {string} SQL DELETE Statement
 */
module.exports.delete = function(resident) {
    var sql =
      'DELETE FROM `residents` '
    + 'WHERE `residentID` = ' + resident;

    return sql;
}

/**
 * allRoomsUnit Alle Räume (die der Benutzer sehen darf)
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param {string} view unitIDs mit Komma getrennt die der Benutzer sehen darf
 * @return {string} SQL SELECT Statement
 */
module.exports.allRoomsUnit = function(view) {
	var sql = 
	  'SELECT `buildingID`, `roomID`, `number`, `maxoccupancy`, `notes`,  `unitID`, `useID`, '
	   + '(SELECT COUNT(*) '
	   + 'FROM `room_resident` '
	   + 'WHERE `room_resident`.`roomID` = `room`.`roomID`) AS `residents` '
	+ 'FROM `room` '
	+ 'WHERE `unitID` IN (' + view + ') '
	+ 'ORDER BY `buildingID` ASC, `number`*1 ASC ';

    return sql;
}

/**
 * allRoomResidents Alle Bewohner die Räume belegen
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param {string} view unitIDs mit Komma getrennt die der Benutzer sehen darf
 * @return {string} SQL SELECT Statement
 */
module.exports.allRoomResidents = function(view) {
	var sql = 
      'SELECT `rrID`, `room_resident`.`roomID`, `residents`.`residentID`, `residents`.`forename`, `residents`.`surname`, `residents`.`rankID`, '
    + '`ranks`.`label` AS `ranklabel`, `ranks`.`short` AS `rankshort`, `residents`.`sex`, `residents`.`notes`, `residents`.`unitID`, `units`.`name` AS `unitname`, '
    + '`units`.`short` AS `unitshort`, DATE_FORMAT(`until`,"%d.%m.%Y") AS `until`,  '
	    + '(SELECT COUNT(*)  '
 	    + ' FROM `keys`  '
 	    + ' WHERE `residents`.`residentID` = `keys`.`residentID` AND `room_resident`.`roomID` = `keys`.`roomID`) AS `keys` '
    + 'FROM `room_resident`  '
    + 'INNER JOIN `residents` ON `residents`.`residentID` = `room_resident`.`residentID` '
    + 'INNER JOIN `ranks` ON `residents`.`rankID` = `ranks`.`rankID`  '
    + 'INNER JOIN `units` ON `residents`.`unitID` = `units`.`unitID` '
    + 'ORDER BY `room_resident`.`roomID` ASC, `residents`.`surname` ASC, `residents`.`forename` ASC '

    return sql;
}

/**
 * loadResidentRooms Unterkünfte eines Bewohner anzeigen mit all seinen Mitbewohnern
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param {int} residentID des Bewohner der ausgewählt wurde
 * @return {string} SQL SELECT Statement
 */
module.exports.loadResidentRooms = function(residentID) {
	var sql = 
	  'SELECT `rrID`, `buildings`.`buildingID`, `room_resident`.`roomID`, `buildings`.`number` AS `buildingnumber`, '
	+ '`room`.`number` AS `roomnumber`, `buildings`.`alias` AS `buildingalias`, `buildings`.`notes` AS `buildingnotes`, '
	+ '`room`.`maxoccupancy`, `room`.`notes` AS `roomnotes`, `room`.`unitID` AS `roomunit`, DATE_FORMAT(`until`,"%d.%m.%Y") AS `until`, `room_resident`.`residentID`, '
	+ '`forename`, `surname`, `sex`, `residents`.`notes` AS `residentnotes`, `residents`.`rankID`, `ranks`.`label` AS `ranklabel`, '
	+ '`ranks`.`short` AS `rankshort`, `residents`.`unitID` AS `residentunit`, `units`.`name` AS `unitname`, `units`.`short` AS `unitshort`, '
		+ '(SELECT COUNT(*) '
		+ 'FROM `keys` '
		+ 'WHERE `room_resident`.`residentID` = `keys`.`residentID` AND `room_resident`.`roomID` = `keys`.`roomID`) AS `keys` '
	+ 'FROM `room_resident`  '
	+ 'INNER JOIN `residents` ON `residents`.`residentID` = `room_resident`.`residentID`  '
	+ 'INNER JOIN `ranks` ON `residents`.`rankID` = `ranks`.`rankID`  '
	+ 'INNER JOIN `units` ON `residents`.`unitID` = `units`.`unitID`  '
	+ 'INNER JOIN `room` ON `room_resident`.`roomID` = `room`.`roomID` '
	+ 'INNER JOIN `buildings` ON `buildings`.`buildingID` = `room`.`buildingID` '
	+ 'WHERE `room_resident`.`roomID` IN (SELECT `roomID` FROM `room_resident` WHERE `residentID` = ' + residentID + ') '
	+ 'ORDER BY `buildingnumber` ASC, `roomnumber` ASC, `surname` ASC, `forename` ASC ';

    return sql;
}

/**
 * chgUntil Belegungsdauer eines Bewohner in einem bestimmten Raum ändern oder entfernen
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param  {int} rrID Übergabe der Raumzuweisung des Benutzers
 * @param {string} date Neues oder leeres Datum
 * @return {string} SQL UPDATE Statement
 */
module.exports.chgUntil = function(rrID, date) {
    var sql = '';

    if (date === '') {
    	sql = "UPDATE `room_resident` SET `until` = NULL WHERE `room_resident`.`rrID` = " + rrID;	
    } else {
    	sql = "UPDATE `room_resident` SET `until` = '" + date + "' WHERE `room_resident`.`rrID` = " + rrID;
    }
    

    return sql;
}

/**
 * removeRoom Unterkunft freigeben
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param  {int} rrID Übergabe der residentID Daten
 * @return {string} SQL DELETE Statement
 */
module.exports.removeRoom = function(rrID) {
    var sql =
      'DELETE FROM `room_resident` '
    + 'WHERE `rrID` = ' + rrID;

    return sql;
}