/**
 * Dienstgrade
 * @module ranks
 */


/** ********************************************************
 * load: lädt alle Dienstgrade
 * 
 * @author Dennis Freitag
 * @return {string} SQL SELECT Statement
 * ********************************************************/
module.exports.load = function() {
    var sql = "SELECT * "
            + "FROM `ranks` "
            + "ORDER BY `label` ASC";
    return sql;
};

/** ********************************************************
 * edit: Ändert Dienstgrad
 * 
 * @author Dennis Freitag
 * @param {Object}  rankObject
 *                  rankObject.rankID = rankID des Dienstgrads
 *                  rankObject.label  = Dienstgradbezeichnung lang
 *                  rankObject.short  = Dienstgradbezeichnung kurz
 * @return {string} SQL UPDATE Statement
 * ********************************************************/
module.exports.edit = function(rankObject) {
    var sql = "UPDATE ranks "
            + "SET label = '" + rankObject.label + "', short = '" + rankObject.short + "' "
            + "WHERE rankID = " + rankObject.rankID;
    return sql;
};

/** ********************************************************
 * add: Fügt Dienstgrad hinzu
 * 
 * @author Dennis Freitag
 * @param {Object}  rankObject
 *                  rankObject.rankID = rankID des Dienstgrads
 *                  rankObject.label  = Dienstgradbezeichnung lang
 *                  rankObject.short  = Dienstgradbezeichnung kurz
 * @return {string} SQL INSERT Statement
 * ********************************************************/
module.exports.add = function(rankObject) {
    var sql = "INSERT INTO ranks (label, short) "
            + "VALUES ('" + rankObject.label + "', '" + rankObject.short + "')";
    return sql;
};

/**
 * show Abrufen eines einzelnen Dienstgrades
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param {int} id rankID des benötigten Dienstgrades
 * @return {string} SQL SELECT Statement
 */
module.exports.show = function(id) {
    var sql =
      'SELECT * '
    + 'FROM `ranks` '
    + 'WHERE `rankID` = ' + id;

    return sql;
}

/**
 * delete Löschen eines Dienstgrades
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param  {int} id rankID des zu löschenden Dienstgrades
 * @return {string} SQL DELETE Statement
 */
module.exports.delete = function(id) {
    var sql =
      'DELETE FROM `ranks` '
    + 'WHERE `rankID` = ' + id;

    return sql;
}

/**
 * residentRanks Anzeigen aller Dienstgrade die durch Bewohner verwendet werden
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @return {string} SQL SELECT Statement
 */
module.exports.residentRanks = function() {
    var sql =
      'SELECT `rankID` '
    + 'FROM `residents` '
    + 'GROUP BY `rankID`';

    return sql;
}

/**
 * userRanks Anzeigen aller Dienstgrade die durch Benutzer verwendet werden
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @return {string} SQL SELECT Statement
 */
module.exports.userRanks = function() {
    var sql =
      'SELECT `rankID` '
    + 'FROM `user` '
    + 'GROUP BY `rankID`';

    return sql;
}

/**
 * withoutID Ermitteln aller Dienstgrade ohne die übergebene ID
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param {int} id rankID des nicht auszulassenden Dienstgrades
 * @return {string} SQL SELECT Statement
 */
module.exports.withoutID = function(id) {
    var sql =
      'SELECT * '
    + 'FROM `ranks` '
    + 'WHERE `rankID` != ' + id;

    return sql;
}