/* 
 * Unterkunftsverwaltung
 */

/**
 * Benutzereinstellungen
 * @var {port} Port über den die Webanwendung erreichbar ist
 * @var {configFile} Speicherort der Konfigurationsdatei ggf. Standardpfad gem Installation: 'config/config.json'
 */

var port            = 8080,
    configFile      = 'config/config.json';

/**
 * Version
 */

var version         = {
                        number: 0.7,
                        name: "Final Edition"
                    };

/**
 * Debugging
 */

var showerrors      = true,
    mysqlquerys     = true,
    mysqlerrors     = true;

/**
 * Laden der benötigten Komponenten
 */
var express         = require('express'),
    app             = express(),
    server          = require('http').createServer(app),
    io              = require('socket.io').listen(server),
    session         = require("express-session")({secret: "EightBitsForProject", resave: true, saveUninitialized: true}),
    sharedsession   = require("express-socket.io-session"),
    md5             = require("md5"),
    mysql           = require('mysql'),
    jsonfile        = require('jsonfile'),
    path            = require('path'),
    colors          = require('colors'),
    ip              = require('ip'),
    exec            = require('child_process').exec,
    connection;

/**
 * Funktionen für die einzelnen Handler laden
 */

var rights          = require('./functions/rights'),  
    units           = require('./functions/units'),
    ranks           = require('./functions/ranks'),
    roles           = require('./functions/roles'),
    users           = require('./functions/users'),
    unterkunft      = require('./functions/unterkunft'),
    residents       = require('./functions/residents'),
    roomBook        = require('./functions/roomBook');

/**
 * Startmeldung auf der Konsole ausgeben
 */
process.stdout.write('\033c');
console.log(('\n\nUnterkunftsverwaltung - v' + (version.number).toString() + ' "' + version.name + '"\n').cyan.bold);

/**
 * Server und Session starten
 * Definition des Standardpfads
 * Routenkonfiguration zur index.html
 */
server.listen(port);
app.use(session);
io.use(sharedsession(session));
app.use(express.static('./'));

app.get('/', function(req, res) {
    res.sendFile(path.join(__dirname, '../client', 'index.html'));
});

console.log(('Anwendungs-Server über http://' + ip.address() + ':' + port + ' erreichbar.').white.bold);


/** 
 * Laden der config.json und Herstellen der Verbindung zur Datenbank
 */

jsonfile.readFile(configFile, function(err, data) {
    if (err) {
        console.log('ERROR:'.bgRed.white.bold + (' Konfigurationsdatei nicht gefunden. ' + (showerrors ? '(' + err + ')' : '')).white.bold);
        console.log('       Bitte führen Sie die Installation aus um die Konfigurationsdatei anzulegen'.yellow.bold);
        process.exit(1);
    }

    if (data.host === 'localhost') {
        data.host = '127.0.0.1';
    }

    connection = mysql.createConnection({
        host: data.host,
        user: data.user,
        port: data.port,
        password: data.pw,
        database: data.dbname
    });

    connection.on('error', function(err) {
        io.sockets.emit('errorHandlerClose', 'Verbindung zur MySQL Datenbank verloren');
        console.log('\nERROR:'.bgRed.white.bold + (' Datenbank nicht verbunden ').white.bold);
        console.log('       Bitte stellen Sie sicher das der MySQL Server ausgeführt wird und starten Sie den Anwendungs-Server neu'.yellow.bold);
        process.exit(1);
    });

    connection.connect(function(err) {
        if (err) {

            io.sockets.emit('errorHandlerClose', 'Keine Verbindung zur MySQL Datenbank');
            console.log('ERROR:'.bgRed.white.bold + (' Datenbank nicht verbunden ' + (showerrors ? '(' + err + ')' : '')).white.bold);
            console.log('       Bitte stellen Sie sicher das der MySQL Server ausgeführt wird und/oder die Zugangsdaten noch übereinstimmen'.yellow.bold);
            process.exit(1);

        } else {

            connection.query("SELECT * FROM `permissions` WHERE `permissionID` = 1 AND `short` LIKE 'admin'", function(err, rows) {
                
                if (rows[0] === undefined) {
                    io.sockets.emit('errorHandlerClose', 'Falsche oder unvollständige MySQL Datenbank');

                    console.log('ERROR:'.bgRed.white.bold + (' Falsche oder unvollständige Datenbank').white.bold);
                    console.log('       Bitte stellen Sie sicher das die richtige Datenbank ausgewählt und die Datenbankstruktur installiert wurde'.yellow.bold);
                    process.exit(1);
                } else {
                    console.log('Verbindung zur Datenbank hergestellt'.white.bold);

                    // PROTOCOL CONNECTION LOST verhindern
                    // http://stackoverflow.com/questions/20210522/nodejs-mysql-error-connection-lost-the-server-closed-the-connection
                    setInterval(function () { connection.query('SELECT 1'); }, 120000);
                }

            });
        }
    });
});

/**
 * Socket IO Handler
 */

io.sockets.on('connection', function(socket) {

    socket.join(socket.handshake.sessionID);

    // Benutzeranmeldung durchgeführt?
    if (!socket.handshake.session.user) {
        io.sockets.in(socket.handshake.sessionID).emit('loginscreen');
    } else {
        io.sockets.in(socket.handshake.sessionID).emit('userpermissions', socket.handshake.session.user, socket.handshake.session.rights, version);
        setTimeout(function() {
            io.sockets.in(socket.handshake.sessionID).emit('loadSidebar', socket.handshake.session.sidebar);
            io.sockets.in(socket.handshake.sessionID).emit('appscreen');
        }, 100);
    }

    // ********************************************
    // Login Handler
    // ********************************************

    socket.on('login', function(loginData, callback) {
        
        // Umwandeln des Passworts in MD5
        loginData.md5password = md5(loginData.password);

        // Überprüfen mit der Datenbank
        dbquery({login: users.check(loginData)}, socket, function(result) {
            
            if (!result.login[0]) {
                // Keinen übereinstimmenden Eintrag gefunden
                callback(false);
            } else {
                // Benutzername und Passwort stimmen überein
                var userData = result.login[0]; 

                dbquery({
                    parents: units.withoutParents(),
                    children: units.children(),
                    roles: users.roleShowPerm(userData.userID),
                    permissions: rights.permissions()

                }, socket, function(result) {

                    if (result !== undefined) {
                        // Übergeben der Benutzerdaten an die Session
                        socket.handshake.session.user = userData;
                        
                        // Ermitteln der Benutzerrechte
                        socket.handshake.session.rights = rights.getRights(result.parents, result.children, result.roles, result.permissions);

                        // Speichern welche Gebäude geöffnet sind
                        socket.handshake.session.buildingToggle = {};
                        
                        // Erstellen der Sidebar-Navigation unter Beachtung der Rechte
                        jsonfile.readFile('config/sidebar.json', function(err, data) {
                            if (err) {
                                console.log('ERROR:'.bgRed.white.bold + (' Systemdatei für die Navigation nicht gefunden. ' + (showerrors ? '(' + err + ')' : '')).white.bold);
                                process.exit(1);
                            }

                            rights.sidebar(data, socket.handshake.session.rights, function(sidebar) {
                                socket.handshake.session.sidebar = sidebar;
                                socket.handshake.session.save(); // Additional Save Async
                                io.sockets.in(socket.handshake.sessionID).emit('loadSidebar', sidebar);
                            });
                        });                        
                        
                        // Speichern der Session
                        socket.handshake.session.save();

                        // Benutzeroberfläche freigeben
                        io.sockets.in(socket.handshake.sessionID).emit('userpermissions', userData, socket.handshake.session.rights, version);
                        io.sockets.in(socket.handshake.sessionID).emit('login-ok', userData);

                    } 
                });
            }
        });
    });

    // Aktualisieren der Rechte beim Client, da Änderungen aufgetreten sind.
    socket.on('getpermissions', function(callback) {
        if (socket.handshake.session.user !== undefined) {

            dbquery({
                parents: units.withoutParents(),
                children: units.children(),
                roles: users.roleShowPerm(socket.handshake.session.user.userID),
                permissions: rights.permissions()

            }, socket, function(result) {
                if (result !== undefined) {

                    // Ermitteln der Benutzerrechte
                    socket.handshake.session.rights = rights.getRights(result.parents, result.children, result.roles, result.permissions);

                    // Erstellen der Sidebar-Navigation unter Beachtung der Rechte
                    jsonfile.readFile('config/sidebar.json', function(err, data) {
                        if (err) {
                            console.log('ERROR:'.bgRed.white.bold + (' Systemdatei für die Navigation nicht gefunden. ' + (showerrors ? '(' + err + ')' : '')).white.bold);
                            process.exit(1);
                        }

                        rights.sidebar(data, socket.handshake.session.rights, function(sidebar) {

                            
                            io.sockets.in(socket.handshake.sessionID).emit('loadSidebar', sidebar);
                             
                            socket.handshake.session.sidebar = sidebar;
                            socket.handshake.session.save(); // Additional Save Async

                        });
                    });

                    // Speichern der Session
                    socket.handshake.session.save();

                    // Benutzerrechte an die Clients verteilen
                    io.sockets.in(socket.handshake.sessionID).emit('userpermissions', socket.handshake.session.user, socket.handshake.session.rights, version);

                    callback(true);
                }
            });
        }
    });

    // Abmelden + Session beenden
    socket.on('logout', function() {
        delete socket.handshake.session.user;
        delete socket.handshake.session.rights;
        delete socket.handshake.session.sitebar;
        delete socket.handshake.session.buildingToggle;
        socket.handshake.session.save();

        io.sockets.in(socket.handshake.sessionID).emit('logout');
    });

    // Routing
    socket.on('routing', function(callback) {
        jsonfile.readFile('config/routing.json', function(err, data) {
            if (err) {
                console.log('ERROR:'.bgRed.white.bold + (' Systemdatei fürs Routing nicht gefunden. ' + (showerrors ? '(' + err + ')' : '')).white.bold);
                process.exit(1);
            }
            callback(data);
        });
    });

    // ********************************************
    //  Rank Handler
    // ********************************************
    socket.on('loadRankData', function(callback) {
        dbquery({ ranks: ranks.load() }, socket, function(result) { callback(result.ranks); });
    });

    socket.on('loadRank', function(id, callback) {
        dbquery({ ranks: ranks.show(id) }, socket, function(result) { callback(result.ranks[0]); });
    });

    socket.on('editRank', function(rankObject, callback) {
        dbquery({ edit: ranks.edit(rankObject) }, socket, function(result) {
            if (result !== undefined) { callback(true); }
        });
    });

    socket.on('addRank', function(rankObject, callback) {
        dbquery({ add: ranks.add(rankObject) }, socket, function(result) {
            if (result !== undefined) { callback(true); }
        });
    });

    socket.on('deleteRank', function(id, callback) {
        dbquery({ delete: ranks.delete(id) }, socket, function(result) {
            if (result !== undefined) { callback(true); }
        });
    });

    socket.on('loadRankIdFromResidents', function(callback) {
        dbquery({ ranks: ranks.residentRanks() }, socket, function(result) { callback(result.ranks); });
    });
    
    socket.on('loadRankIdFromUser', function(callback) {
        dbquery({ ranks: ranks.userRanks() }, socket, function(result) { callback(result.ranks); });
    });
    
    socket.on('loadRanksWithoutId', function(id, callback) {
        dbquery({ ranks: ranks.withoutID(id) }, socket, function(result) { callback(result.ranks); });
    });

    // ********************************************
    //  Unit Handler
    // ********************************************
    socket.on('loadUnits', function(selectnone, onlySelectable, callback) {
        dbquery({ 
            parents: units.withoutParents(),
            children: units.children()
        }, socket, function(result) { 
            var treeview = units.buildUnitsTree(result.parents, result.children, onlySelectable);
            if (selectnone) {
                units.addnullUnitsTree(treeview);
            } else {
                if (treeview.length === 0) { units.addnoUnitsTree(treeview); }
            }

            callback(treeview);
        });
    });


    socket.on('loadUnitData', function(callback) {
        dbquery({ unit: units.children() }, socket, function(result) { callback(result.unit); });
    });

    socket.on('loadUnitsWithoutParent', function(callback) {
        dbquery({ unit: units.withoutParents() }, socket, function(result) { callback(result.unit); });
    });

    socket.on('loadUnit', function(data, callback) {
        dbquery({ unit: units.show(data) }, socket, function(result) { callback(result.unit[0]); });
    });

    socket.on('addUnit', function(data, callback) {
        dbquery({ unit: units.exists(data) }, socket, function(result) { 
            if (result.unit[0].existing > 0) {
                callback(false);
                return;
            } else {
                dbquery({ unit: units.add(data) }, socket, function(result) { 
                    if (result !== undefined) { callback(true); io.sockets.emit('reloadpermissions'); }
                });
            }
        });
    });

    socket.on('editUnit', function(data, callback) {
        dbquery({ unit: units.exists(data) }, socket, function(result) { 
            if (result.unit[0].existing > 0) {
                callback(false);
                return;
            } else {
                dbquery({ unit: units.edit(data) }, socket, function(result) { 
                    if (result !== undefined) { callback(true); io.sockets.emit('reloadpermissions'); }
                });
            }
        });
    });

    socket.on('deleteUnit', function(data, callback) {
        dbquery({ unit: units.delete(data) }, socket, function(result) {
            if (result === undefined) { callback(false); } else { callback(true); io.sockets.emit('reloadpermissions');}
        });
    });

    // ********************************************
    //  Roles Handler
    // ********************************************
    socket.on('loadPermissions', function(callback) {
        dbquery({ permissions: rights.permissions() }, socket, function(result) { callback(result.permissions); });
    });

    socket.on('loadRoles', function(callback) {
        dbquery({ roles: roles.load() }, socket, function(result) { callback(result.roles); });
    });

    socket.on('loadRoleIDFromUserUnits', function(callback) {
        dbquery({ roles: roles.inUse() }, socket, function(result) { callback(result.roles); });
    });
    
    socket.on('loadRoleId', function(id, callback) {
        dbquery({ role: roles.loadId(id) }, socket, function(result) { callback(result.role[0]); });
    });

    socket.on('addRole', function(newRole, callback) {
        dbquery({ exists: roles.exists(newRole) }, socket, function(result) { 
            if ((result.exists).length > 0) {
                callback(result);
            } else {
                 dbquery({ role: roles.add(newRole) }, socket, function(result) { callback(result); });
            }
        });
    });

    socket.on('editRole', function(newRole, callback) {
        dbquery({ exists: roles.exists(newRole) }, socket, function(result) { 
            if ((result.exists).length > 0) {
                callback(result);
            } else {
                dbquery({ role: roles.edit(newRole) }, socket, function(result) { callback(result); io.sockets.emit('reloadpermissions'); });
            }
        });
    });

    socket.on('deleteRole', function(id, callback) {
        dbquery({ delete: roles.delete(id) }, socket, function(result) { callback(true); });
    });
    
    // ********************************************
    // Unterkunft Handler
    // ********************************************
    socket.on('loadAllBuildings', function(callback) {
       dbquery({ buildings: unterkunft.loadAllBuildings() }, socket, function(result) { callback(result.buildings); });
    });
    
    socket.on('loadAllRooms', function(callback) {
       dbquery({ rooms: unterkunft.loadAllRooms() }, socket, function(result) { callback(result.rooms); });
    });
    
    socket.on('addBuilding', function(newBuilding, callback) {
        dbquery({ add: unterkunft.addBuilding(newBuilding) }, socket, function(result) {
            if (result === undefined) { callback(false); } else { callback(true); }
        });
    });
    
    socket.on('loadBuilding', function(id, callback) {
        dbquery({ load: unterkunft.loadBuilding(id) }, socket, function(result) {
            if (result === undefined) { callback(false); } else { callback(result.load[0]); }
        });
    });
    
    socket.on('editBuilding', function(newBuilding, callback) {
        dbquery({ edit: unterkunft.editBuilding(newBuilding) }, socket, function(result) {
            if (result === undefined) { callback(false); } else { callback(true); }
        });
    });
    
    socket.on('loadRoomResident', function(callback) {
        dbquery({ rooms: unterkunft.loadRoomResident() }, socket, function(result) { callback(result.rooms); });
    });
    
    socket.on('loadRoomFromBuilding', function(buildingNumber, callback) {
        dbquery({ rooms: unterkunft.loadRoomFromBuilding(buildingNumber) }, socket, function(result) { callback(result.rooms); });
    });
    
    socket.on('addRoom', function(newRoom, callback) {
        dbquery({ add: unterkunft.addRoom(newRoom) }, socket, function(result) {
            if (result === undefined) { callback(false); } else { callback(true); }
        });
    });
    
    socket.on('loadRoom', function(roomID, callback) {
        dbquery({ room: unterkunft.loadRoom(roomID) }, socket, function(result) { callback(result.room[0]); });
    });
    
    socket.on('loadRoomsByBuildingID', function(buildingID, callback) {
        dbquery({ rooms: unterkunft.loadRoomsByBuildingID(buildingID) }, socket, function(result) { callback(result.rooms); });
    });
    
    socket.on('editRoom', function(newRoom, callback) {
        dbquery({ edit: unterkunft.editRoom(newRoom) }, socket, function(result) {
            if (result === undefined) { callback(false); } else { callback(true); }
        });
    });
    
    socket.on('deleteBuilding', function(id, callback) {
        dbquery({ delete: unterkunft.deleteBuilding(id) }, socket, function(result) {
            if (result === undefined) { callback(false); } else { callback(true); }
        });
    });
    
    socket.on('deleteRoom', function(id, callback) {
        dbquery({ delete: unterkunft.deleteRoom(id) }, socket, function(result) {
            if (result === undefined) { callback(false); } else { callback(true); }
        });
    });

    socket.on('BuildingToggle', function(callback) {
        callback(socket.handshake.session.buildingToggle);
    });

    socket.on('BuildingToggleIn', function(id) {
        socket.handshake.session.buildingToggle[id] = id;
        socket.handshake.session.save(); // Additional Save Async
    });

    socket.on('BuildingToggleOut', function(id) {
        delete socket.handshake.session.buildingToggle[id];
        socket.handshake.session.save(); // Additional Save Async
    });
    
    // ********************************************
    // Users Handler
    // ********************************************
    socket.on('loadUsers', function(callback) {
        dbquery({ user: users.load() }, socket, function(result) { callback(result.user); });
    });

    socket.on('showUser', function(data, callback) {
        dbquery({ user: users.show(data) }, socket, function(result) { callback(result.user[0]); });
    });

    socket.on('showUserRoles', function(data, callback) {
        dbquery({ user: users.roleShow(data) }, socket, function(result) { callback(result.user); });
    });

    socket.on('addUser', function(data, callback) {
        dbquery({ exists: users.exists(data) }, socket, function(result) {
            if (result.exists[0].existing > 0) {
                callback(false);
                return;
            } else {
                dbquery({ 
                    add: users.add(data),
                    id: users.getID(data)
                }, socket, function(result) {
                    if (result !== undefined) {
                        for (i = 0; i < data.roles.length; i++) {
                            dbquery({ user: users.roleAdd(result.id[0].userID, data.roles[i]) }, socket, function(result) {});
                        }

                        callback(true);
                        io.sockets.emit('reloadpermissions');
                    }
                });
            }
        });
    });

    socket.on('editUser', function(data, callback) {
        dbquery({ exists: users.exists(data) }, socket, function(result) {
            if (result.exists[0].existing > 0) {
                callback(false);
                return;
            } else {
                dbquery({ edit: users.edit(data) }, socket, function(result) {
                    if (result !== undefined) {
                        if (data.editFunc !== 'no-user') {
                            dbquery({ roleDel: users.roleDel(data) }, socket, function(result) {
                                for (i = 0; i < data.roles.length; i++) {
                                    dbquery({ role: users.roleAdd(data.userID, data.roles[i]) }, socket, function(result) {});
                                }
                            }); 
                        }

                        callback(true);
                        setTimeout(function() {io.sockets.emit('reloadpermissions'); }, 1000);
                    }
                });
            }
        });
    });

    socket.on('deleteUser', function(data, callback) {
        dbquery({ delete: users.delete(data) }, socket, function(result) {
            if (result === undefined) { callback(false); } else { callback(true); }
        });
    });
    
    // ********************************************
    // roomBook Handler
    // ********************************************
    socket.on('loadAllResidents', function(callback) {
        dbquery({ residents: roomBook.loadAllResidents() }, socket, function(result) { callback(result.residents); });
    });
    
    socket.on('loadResident', function(id, callback) {
        dbquery({ resident: roomBook.loadResident(id) }, socket, function(result) { callback(result.resident[0]); });
    });
    
    socket.on('loadAllRoomsAndBuildings', function(callback) {
        dbquery({ resident: roomBook.loadAllRoomsAndBuildings() }, socket, function(result) { callback(result.resident); });
    });
    
    socket.on('allUnits', function(callback) {
        dbquery({ units: units.allUnits() }, socket, function(result) { callback(result.units); });
    });
    
    socket.on('loadEmptyRooms', function(callback) {
        dbquery({ emptyRoom: roomBook.loadEmptyRooms() }, socket, function(result) { callback(result.emptyRoom); });
    });
    
    socket.on('loadRoomsByGender', function(resident, callback) {
        dbquery({ rooms: roomBook.loadRoomsByGender(resident) }, socket, function(result) { callback(result.rooms); });
    });
    
    socket.on('fillInfo', function(roomID, callback) {
        dbquery({ rooms: roomBook.fillInfo(roomID) }, socket, function(result) { callback(result.rooms); });
    });
    
    socket.on('usedRooms', function(callback) {
        dbquery({ rooms: roomBook.usedRooms() }, socket, function(result) { callback(result.rooms); });
    });
    
    socket.on('unitShow', function(unitID, callback) {
        dbquery({ unit: units.show(unitID) }, socket, function(result) { callback(result.unit[0]); });
    });
    
    socket.on('bookResident', function(booking, callback) {
        dbquery({ booking: roomBook.bookResident(booking) }, socket, function(result) { callback(result.booking); });
    });
        
    socket.on('loadRoomResidentCount', function(roomID, callback) {
        dbquery({ resident: roomBook.loadRoomResidentCount(roomID) }, socket, function(result) { callback(result.resident); });
    });
    
    socket.on('loadResidentsFromRoom', function(roomID, callback) {
        dbquery({ resident: roomBook.loadResidentsFromRoom(roomID) }, socket, function(result) { callback(result.resident); });
    });
    
     socket.on('loadAllRoomResident', function(roomID, callback) {
        dbquery({ resident: roomBook.loadAllRoomResident(roomID) }, socket, function(result) { callback(result.resident); });
    });
    
    /**
     * Bewohner (residents)
     */
    socket.on('loadResidents', function(order, callback) {
        dbquery({ 
            
            residents: residents.load(order, socket.handshake.session.rights.view),
            rooms: residents.allrooms(socket.handshake.session.rights.view),
            units: units.allUnits()

        }, socket, function(result) {
            callback(result);
        });
    });

    socket.on('addResident', function(data, callback) {
        dbquery({ exists: residents.exists(data) }, socket, function(result) { 
            if ((result.exists).length > 0) {
                callback(result);
            } else {
                dbquery({ resident: residents.add(data) }, socket, function(result) { callback(result); });
            }
        });
    });

    socket.on('editResident', function(data, callback) {
        dbquery({ exists: residents.exists(data) }, socket, function(result) { 
            if ((result.exists).length > 0) {
                callback(result);
            } else {
                dbquery({ resident: residents.edit(data) }, socket, function(result) { callback(result); });
            }
        });
    });

    socket.on('loadResident', function(data, callback) {
        dbquery({ resident: residents.loadResident(data) }, socket, function(result) { callback(result.resident[0]); });
    });

    socket.on('deleteResident', function(data, callback) {
        dbquery({ delete: residents.delete(data) }, socket, function(result) {
            if (result === undefined) { callback(false); } else { callback(true); }
        });
    });

    /**
     * Unterkünfte (accommodations)
     */
    
    socket.on('loadAccommodations', function(callback) {
        dbquery({ 
            
            buildings: unterkunft.loadAllBuildings(),
            rooms: residents.allRoomsUnit(socket.handshake.session.rights.view),
            residents: residents.allRoomResidents(), 
            units: units.allUnits()

        }, socket, function(result) {
            callback(result);
        });
    });

    socket.on('loadAccommodationsResident', function(residentID, callback) {
        dbquery({ 
            
            residents: residents.loadResidentRooms(residentID),
            resident: residents.loadResident(residentID),
            units: units.allUnits()

        }, socket, function(result) {
            callback(result);
        });
    });

    socket.on('residentUntil', function(rrID, date, callback) {
        dbquery({ change: residents.chgUntil(rrID, date) }, socket, function(result) {
            if (result === undefined) { callback(false); } else { callback(true); }
        });
    });

    socket.on('removeResidentRoom', function(rrID, callback) {
        dbquery({ delete: residents.removeRoom(rrID) }, socket, function(result) {
            if (result === undefined) { callback(false); } else { callback(true); }
        });
    });

});




/**
 * dbquery MySQL Abfrage Funktion mit automatischer Fehlerbehandlung und ggf. -meldung
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param  {arr}   querys   Übergeben der MySQL Queries die in der Datenbank ermittelt werden sollen
 * @param  {socket.io} socket  Aktuell genutzten Socket übergeben
 * @param  {Function} callback Rückgabe der Ergebnisse mit den jeweiligen Namen als Object
 */
function dbquery(querys, socket, callback) {
    var results = {},
        errors = [];

    // Alle MySQL-Querys durchführen
    Object.keys(querys).forEach(function(key) {
        
        connection.query(querys[key], function(err, rows) {

            // Fehler oder erfolgreiche Abfrage
            if (err) {
                if (mysqlerrors) { 
                    console.log('\nMYSQL QUERY:'.bgRed.white.bold);
                    console.log(colors.white.bold(' ' + querys[key]));
                    console.log('MYSQL ERROR:'.bgRed.white.bold);
                    console.log(colors.yellow.bold(' ' + err));
                }

                results[key] = undefined;

                // Fehlermeldungen sammeln
                err.query = querys[key];
                errors.push(err);

            } else {
                if (mysqlquerys) { 
                    console.log('\nMYSQL QUERY:'.bgBlue.white.bold);
                    console.log(colors.white.bold(' ' + querys[key]));
                }

                results[key] = rows;
            }

            // Übergabe aller Query-Results aus der Datenbank oder bei Fehler(n) zurückgeben mit 'undefined'
            if (Object.keys(results).length === Object.keys(querys).length) {
                if (errors.length === 0) {
                    callback(results);
                } else {
                    callback(undefined);
                    if (mysqlerrors) { 
                        socket.emit('errorHandlerDB', errors);
                    }
                }
            }
        });
    });
}