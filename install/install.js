/* 
 * Unterkunftsverwaltung
 * Installation
 *
 */

/* 
 * Variablen und Moduldefinition
 */

var     express = require('express'),
        app = express(),
        server = require('http').createServer(app),
        io = require('socket.io').listen(server),
        mysql = require('mysql'),
        fs = require('fs'),
        jsonfile = require('jsonfile'),
        colors = require('colors/safe'),
        exec = require('child_process').exec,
        ip = require('ip'),

        // Konfigurationsdatei
        configFile = 'config/config.json',
        // Server-Port 
        port = 3000,
        // Application-Port
        appPort = 8080,                  
        // Statusupdates auf die Konsole ausgeben (Funktion consolestatus)
        show_updates = true,             
        // Original Fehlermeldungen vollständig ausgeben
        show_error = true,
        // Anwendung (server.js)
        application = null;

/*
 * - Starten des Servers
 * - Statusmeldungen in Konsole
 */ 
server.listen(port);
process.stdout.write('\033c');
console.log(colors.white.bold('\n\nUnterkunftsverwaltung\n'));
console.log(colors.white.bold('Installations-Server auf Port ' + port + ' gestartet.'));
console.log(colors.white.bold('Bitte die Installation über http://localhost:' + port + ' oder http://'+ ip.address() + ':' + port + ' fortsetzen.\n'));
require("openurl").open("http://localhost:" + port);

/*
 * Definition des Standardpfads
 * Routenkonfiguration 
 */
app.use(express.static('./'));
app.get('/', function(req, res) {
    res.sendFile(__dirname + '/viewInstall.html');
});


/**
 * SOCKET ON: connection
 * - Auf eingehende Socketverbindungen hören / warten
 */
io.sockets.on('connection', function(socket) {

    // Verbindung zwischen Server und Client hergestellt
    consolestatus("Socketverbindung hergestellt", 'on');
    
    //Laden der Konfigurationsdatei (config.json) und Übermitteln an den Client
    jsonfile.readFile(configFile, function(err, data) {
        if (err) {
            consolestatus('Keine Konfigurationsdatei vorhanden', 'info');
        } else {
            consolestatus('Konfigurationsdatei wurde gefunden und geladen', 'info');
        }
        socket.emit('dataLoaded', data);
    });

    /*
     * SOCKET ON: dbcheck
     * Testen der Datenbank Verbindung
     *  - Überprüfen ob eine Verbindung hergestellt werden kann (Zugangsdaten OK?)
     *  - Überprüfen ob die Datenbank existiert (leer oder mit Daten?) oder ob diese angelegt werde muss
     */
    socket.on('dbcheck', function(databaseData, callback) {
        consolestatus("Datenbankverbindung wird überprüft", 'on');

        // Konfiguration der Datenbank
        var dbconfig = {
            host: databaseData.host,
            user: databaseData.user,
            port: databaseData.port,
            password: databaseData.pw,
            multipleStatements: true
        };

        // localhost in 127.0.0.1 umwandeln
        if (dbconfig.host.toLowerCase() === 'localhost') {
            dbconfig.host = '127.0.0.1';
        }

        // MySQL Verbindung einrichten
        var connection = mysql.createConnection(dbconfig);

        // Verbindung herstellen
        connection.connect(function(err) {
            if (err) {
                consolestatus("Datenbankverbindung konnte nicht hergestellt werden", 'fail');
                consolestatus(err, "error");
                callback('no-connection');
                return;

            } else {
                // Überprüfen ob die Datenbank existiert
                connection.query('SHOW TABLES FROM `' + databaseData.dbname + '`;', function(err, rows, fields) {
                    if(err) {
                        // Datenbank existiert nicht und wird angelegt
                        consolestatus("Datenbank muss angelegt werden", "info");
                        callback('new-database');
                    } else if (rows == '') {
                        // Datenbank existiert und ist leer
                        consolestatus("Datenbank vorhanden und leer", 'info');
                        callback('empty-database');
                    } else {
                        // Datenbank existiert und enthält Tabellen
                        consolestatus("Befüllte Datenbank vorhanden", 'warn');
                        callback('used-database');
                    }
                });

                return;
            }
        });
    });

    /**
     * writeConfigFile Konfigurationsdatei speichern
     *
     * @author Rafal Welk <rafal@project.hatua.de>
     * @param  {obj} data Zugangsdaten
     */
    function writeConfigFile(data) {

        var obj = {
            host: data.host,
            port: data.port,
            dbname: data.dbname,
            user: data.user,
            pw: data.pw,
            admin: data.admin,
            adminForename: data.adminForename,
            adminSurname: data.adminSurname
        };

        jsonfile.writeFileSync(configFile, obj);
        consolestatus("Konfigurationsdatei gespeichert", "info");
    }

    /*
     * SOCKET ON: install
     * Start der Installation
     *  - Hierbei wird unterschieden ob nur die Konfiguration gespeichert werden kann
     *  - Oder ob alle mySQL Statements eingetragen werden müssen inkl. anlegen der Datenbank
     */
    socket.on('install', function(loginData, callback) {
        consolestatus("Installation wurde gestartet", "on");

        // localhost in 127.0.0.1 umwandeln
        if (loginData.host.toLowerCase() === 'localhost') {
            loginData.host = '127.0.0.1';
        }

        // Konfiguration aktualisieren
        // Konfigurationsdatei speichern und Administrator zurücksetzen
        if (loginData.special == 'onlyconfig') {
            consolestatus("Konfiguration wird aktualisieren", "info");

            // Datenbankverbindung herstellen
            var dbconfig = {
                host: loginData.host,
                user: loginData.user,
                port: loginData.port,
                password: loginData.pw,
                database: loginData.dbname
            }; 
            
            var connection = mysql.createConnection(dbconfig); 
            connection.connect(function(err) {
                if (err) {
                    var errorMsg = 'Installation fehlgeschlagen<small title="' + err + '">Datenbankverbindung fehlgeschlagen</small>';
                    socket.emit("installFailed", errorMsg);
                    return;
                } else {

                    connection.query("SELECT COUNT(*) AS anzahl FROM `permissions` WHERE `permissionID` = 1 AND `short` LIKE 'admin'", function(err, rows) {
                        if (rows[0].anzahl === 0) {
                            var errorMsg = 'Installation fehlgeschlagen<small title="Keine Datenbank der Anwendung Unterkunftsverwaltung">Falsche oder unvollständige MySQL-Datenbank</small>';
                            socket.emit("installFailed", errorMsg);
                            return;
                        } else {
                            var adminUpdateSQL = "UPDATE `user` SET "
                                    + "`username` = '" + loginData.admin + "', "
                                    + "`password` = MD5('" + loginData.adminpw + "'), "
                                    + "`forename` = '" + loginData.adminForename + "', "
                                    + "`surname` = '" + loginData.adminSurname + "' "
                                    + "WHERE `user`.`userID` = 1;";

                            connection.query(adminUpdateSQL, function(err) {
                                if (err) {
                                    var errorMsg = 'Installation fehlgeschlagen<small title="' + err + '">Datenbank kann nicht gelöscht werden</small>';
                                    socket.emit("installFailed", errorMsg);
                                    return;
                                } else {
                                    consolestatus("Administrator zurückgesetzt", "info");
                                    writeConfigFile(loginData);
                                    if (application === null) {
                                        socket.emit("onlyConfigFile", true);
                                    } else {
                                        socket.emit("onlyConfigFile", false);
                                    }
                                    return;
                                }
                            });
                        }
                    });
                    
                }
            });
        
        // oder vollständige Installation
        } else {

            // Schritt 1: Datenbankverbindung herstellen
            // Schritt 2: Datenbank anlegen
            var dbconfig = {
                host: loginData.host,
                user: loginData.user,
                port: loginData.port,
                password: loginData.pw,
                multipleStatements: true
            };

            var connection = mysql.createConnection(dbconfig);

            connection.connect(function(err) {
                if (err) {
                    var errorMsg = 'Installation fehlgeschlagen<small title="' + err + '">Datenbankverbindung fehlgeschlagen</small>';
                    socket.emit("installFailed", errorMsg);
                    return;
                } else {

                    // Speichern der Zugangsdaten
                    consolestatus("1. Konfigurationsdatei wird gespeichert", "info");
                    writeConfigFile(loginData);

                    // Datenbank vorhanden
                    consolestatus("2. Datenbank wird angelegt", "info");

                    // Datenbank löschen wenn sie existiert
                    connection.query('DROP DATABASE IF EXISTS `' + loginData.dbname + '`;', function(err) {
                        if (err) {
                            var errorMsg = 'Installation fehlgeschlagen<small title="' + err + '">Datenbank kann nicht gelöscht werden</small>';
                            socket.emit("installFailed", errorMsg);
                            return;
                        }
                    });

                    // Datenbank anlegen
                    connection.query('CREATE DATABASE `' + loginData.dbname + '` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;', function(err) {
                        if (err) {
                            var errorMsg = 'Installation fehlgeschlagen<small title="' + err + '">Datenbank kann nicht erstellt werden</small>';
                            socket.emit("installFailed", errorMsg);
                            return;
                        }
                    });

                    // Datenbank auswählen
                    connection.query('USE `' + loginData.dbname + '`;', function(err) {
                        if (err) {
                            var errorMsg = 'Installation fehlgeschlagen<small title="' + err + '">Datenbank kann nicht ausgewählt werden</small>';
                            socket.emit("installFailed", errorMsg);
                            return;
                        } else {
                            
                            // SQL Statements ermitteln und weiterleiten
                            generateSqlData(['config/unterkunft.sql', 'config/insert.sql', 'config/testdata.sql'], loginData, function(queries) {
                                
                                // Anzahl der SQL Statesments
                                var total = queries[0].length + queries[1].length + queries[3].length;
                                if (loginData.special === 'testdata') {
                                    total += queries[2].length;
                                }

                                // Datenstruktur erstellen
                                consolestatus("3. Datenstruktur erstellen", "info");
                                socket.emit("installState", 'Installiere Datenstruktur');
                                runSqlData(loginData, queries[0], 0, total, function(result) {
                                    
                                    // Grunddaten eintragen
                                    consolestatus("4. Grunddaten eintragen", "info");
                                    socket.emit("installState", 'Grunddaten werden eingetragen');
                                    runSqlData(loginData, queries[1], queries[0].length, total, function(result) {

                                        // Administrator anlegen
                                        consolestatus("5. Administrator eintragen", "info");
                                        socket.emit("installState", 'Administrator wird angelegt');
                                        runSqlData(loginData, queries[3], queries[0].length + queries[1].length, total, function(result) {

                                            // ggf. Testdaten eintragen
                                            if (loginData.special === 'testdata') {
                                                consolestatus("6. Testdaten eintragen", "info");
                                                socket.emit("installState", 'Testdaten werden eingetragen');
                                                runSqlData(loginData, queries[2], queries[0].length + queries[1].length + queries[3].length, total, function(result) {
                                                    consolestatus("Installation (inkl. Testdaten) abgeschlossen", "info");
                                                    if (application === null) {
                                                        socket.emit("installFinish", true);
                                                    } else {
                                                        socket.emit("installFinish", false);
                                                    }
                                                });
                                            } else {
                                                consolestatus("Installation abgeschlossen", "info");
                                                if (application === null) {
                                                    socket.emit("installFinish", true);
                                                } else {
                                                    socket.emit("installFinish", false);
                                                }
                                            }
                                        });
                                    });
                                });
                            });
                        }
                    });
                }
            });
        }

        /**
         * runSqlData Ausführen der SQL Statements und Anzeigen des aktuellen
         *   Status beim Client (Webseite)
         *
         * @author Rafal Welk <rafal@project.hatua.de>
         * @param  {obj}   lgData   Zugangsdaten mySQL
         * @param  {array}   queries  SQL Statements
         * @param  {int}   start    Aktueller Eintrag von den gesamten Statements
         * @param  {int}   total    Gesamte Anzahl der SQL Einträge
         * @param  {Function} callback Rückgabe des Wertes true wenn erfolgreich
         * @extends {statusMath}
         */
        function runSqlData(lgData, queries, start, total, callback) {

            var errorMsg = null,
                lastStatus = null,
                config = {
                    host: lgData.host,
                    user: lgData.user,
                    port: lgData.port,
                    password: lgData.pw,
                    database: lgData.dbname,
                    multipleStatements: true
                };

            var connection = mysql.createConnection(config);

            // Ausführen aller SQL Statements
            queries.forEach(function (query, index, array) {
                connection.query(query, function(err) {

                    if (err) {
                        if (errorMsg === null) {
                            errorMsg = 'Installation fehlgeschlagen<small title="' + err + '(SQL Query: ' + query + ' )'+ '">Datenbankeinträge fehlerhaft</small>';
                            socket.emit("installFailed", errorMsg);
                            consolestatus(err, "error");
                            consolestatus("Installation fehlgeschlagen", "fail");
                            return;
                        }
                    } else {
                        if (index === array.length - 1) {
                             callback(true);
                        }

                        // Prozentanzeige errechnen
                        lastStatus = statusMath(start + index + 1, total, lastStatus);
                    }
                });
            });  
        }

    });  
    
    /*
     * SOCKET ON: startApp
     * Starten der Anwendung
     */
    socket.on('startApp', function(callback) {

        application = exec('node server/server.js', {maxBuffer: 1024 * 1024 * 1024}, function(err, stdout, stderr) {
            if (err !== null) {
                consolestatus("Anwendung konnte nicht gestartet werden", "fail");
                consolestatus(err, "error");
                process.exit(1);
                return;
            } 
        });
        
        application.stdout.on('data', function(data) {
            console.log(data);
        });

        application.stderr.on('data', function(data) {
            console.log(data);
        });

        consolestatus("Anwendung gestartet", "info");
        callback({ip: ip.address(), port: appPort});
    });


    /**
     * statusMath Berechnung der aktuellen Prozentanzeige bei der Installation
     *   und senden dieser Information an den Client (Webseite)
     *
     * @author Rafal Welk <rafal@project.hatua.de>
     * @param  {int} actual    Aktueller SQL Eintrag
     * @param  {int} total     Gesamte Anzahl der SQL Einträge
     * @param  {int} oldStatus Letzter Stand, damit nicht unnötige SOCKET EMITS verschickt werden 
     * @return {int} neuer Status wird zurückgegeben
     */
    function statusMath(actual, total, oldStatus) {
        var newStatus = Math.floor((actual + 1) / total * 100);
        if ((newStatus !== oldStatus) && (newStatus <= 100)) {
            socket.emit("installStatus", newStatus);
        }
        return newStatus;
    }

    /**
     * generateSqlData Erzeugen der SQL Statements aus SQL-Dateien und speichern
     *   dieser in einem Array. Zusätzlich wird auch hier der SQL INSERT für den
     *   Administrator hinzugefügt
     *
     * @author Rafal Welk <rafal@project.hatua.de>
     * @param  {array}   filePaths Dateipfade für die Testdaten und die Datenstruktur
     * @param  {obj}   lgData    Zugangsdaten für den Administrator
     * @param  {Function} callback  Rückgabe der SQL Statements 
     */
    function generateSqlData(filePaths, lgData, callback) {
        var queries = [];
        var queriesNew = [];

        filePaths.forEach(function(filePath) {

            queriesNew = fs.readFileSync(filePath)
                .toString()
                .replace(/(\r\n|\n|\r)/gm, " ")                     // neue Zeilen entfernen
                .replace(/\s+/g, ' ')                               // überschüssige Leerzeichen entfernen
                .split(";")                                         // in Statements teilen
                .map(Function.prototype.call, String.prototype.trim)
                .filter(function(el) {
                    return el.length !== 0;                         // leere Zeilen entfernen
            });

            queries.push(queriesNew);
        });

        // Administrator hinzufügen
        var queriesNew = ["INSERT INTO `user` (`userID`, `username`, `password`, `forename`, `surname`, `rankID`) VALUES (1, '" + lgData.admin + "', MD5('" + lgData.adminpw + "'), '" + lgData.adminForename + "', '" + lgData.adminSurname + "', 1)"];
        queries.push(queriesNew);

        callback(queries);
    }

    /**
     * consolestatus Funktion zur Statusausgabe auf der Konsole
     *
     * @author Rafal Welk <rafal@project.hatua.de>
     * @param  {string} message Meldung die angezeigt werden
     * @param  {string} color   Kategorie, die auch farblich hervorgehoben wird
     */
    function consolestatus(message, color) {
        if (show_updates === true) {
            if (color == 'info') {
                console.log(colors.white.bgCyan.bold('> INFO:') + '\t\t ' + colors.cyan.bold(message));

            } else if (color == 'on') {
                console.log(colors.white.bgBlue.bold('> SOCKET ON:') + '\t ' + colors.blue.bold(message));

            } else if (color == 'fail') {
                console.log(colors.white.bgMagenta.bold('> FAIL:') + '\t\t ' + colors.magenta.bold(message));

            } else if (color == 'warn') {
                console.log(colors.white.bgYellow.bold('> WARN:') + '\t\t ' + colors.yellow.bold(message));

            }
        } 

        if ((show_error === true) && (color == 'error')) {
            console.log(colors.white.bgRed.bold('> ERROR:') + '\n' + colors.red.bold(message));
        }
    }

});