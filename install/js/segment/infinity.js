var outer = document.querySelector('.infinity-path'),
    segment = new Segment(outer, 0, 0.1),
    success = document.querySelector('.success-path'),
	successSegment = new Segment(success, 0, 0.1),

    error = document.querySelector('.error-path'),
    errorSegment = new Segment(error, 0, 0.1),
    error2 = document.querySelector('.error-path2'),
    errorSegment2 = new Segment(error2, 0, 0.1),

    infinite = null,
    successMsg = 'Installation abgeschlossen';
    errorMsg = 'Installation fehlgeschlagen';

function infinityAnimation(segment){
    var self = this;
    if (infinite === null) {
        segment.draw('100%', '150%', 1, {circular:true, callback: function(){
            infinityAnimation.call(self, segment);
        }});
    } else {
        segment.draw('100%', '100%', 0.5, {circular:true, callback: function(){
            $(".infinity-path").hide();
            if (infinite === 'success') {
                $(".success-path").show();
                successAnimation(successSegment);
            } else {
                $(".error-path").show();
                $(".error-path2").show();
                errorAnimation(errorSegment, errorSegment2);
            }
        }});
    }
}

function successAnimation(segment){
    $(".statusbar").fadeOut(200);
    $(".statusbg").fadeOut(200);

    segment.draw('100% - 50', '100%', 0.4, {callback: function() {
        $('#installpage').addClass('success');
        $(".success-path").css('stroke', 'white');
        $(".statuspercent").fadeOut();
        $(".btn-completed").fadeIn();
    	$(".status").queue(function(next) {
    		$(this).addClass("animated tada success").html(successMsg).fadeIn();
    		next();
    	});
    }});
}

function errorAnimation(segment, segment2){
    $(".statusbar").fadeOut(200);
    $(".statusbg").fadeOut(200);

    segment.draw('100% - 42.5', '100%', 0.4);
    segment2.draw('100% - 42.5', '100%', 0.4, {callback: function() {
        $('#installpage').addClass('failed');
        $(".error-path").css('stroke', 'white');
        $(".error-path2").css('stroke', 'white');
        $(".statuspercent").fadeOut();
        $(".btn-failed").fadeIn();
        $(".status").queue(function(next) {
            $(this).addClass("animated tada failed").html(errorMsg).fadeIn();
            next();
        });
    }});
}

function changePage() {
    $("body").addClass("scroll").queue(function(next) {

        $("#config").addClass("animated fadeOut");
        $("#installpage").delay(200).addClass("animated fadeIn");
        $("#loading").delay(500).queue(function(next) {
            $(".infinity-path").show();
            infinityAnimation(segment);
            $(this).show().addClass("animated fadeIn");
            next();
        });
    });
}