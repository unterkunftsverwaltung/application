/**
 * Unterkunftsverwaltung - Installtion
 */

var $inputHost = $('#inputHost'),
    $inputPort = $('#inputPort'),
    $inputUser = $('#inputUser'),
    $inputPw = $('#inputPw'),
    $inputDb = $('#inputDb'),
    $inputOverwrite = $('#inputOverwrite'),
    $labelOverwrite = $('#labelOverwrite'),

    $inputAdmin = $('#inputAdmin'),
    $inputAdminPw = $('#inputAdminPw'),
    $inputAdminPwAgain = $('#inputAdminPwAgain'),
    $inputAdminForename = $('#inputAdminForename'),
    $inputAdminSurname = $('#inputAdminSurname'),

    $inputSpecial = $('#inputSpecial'),
    $onlyConfig = $('.onlyconfig'),

    $inputSubmit = $('#inputSubmit'),
    $inputReset = $('#inputReset');

$(function() {
	var socket = io.connect();
        
        /*
         * Einträge der Konfigurationsdatei übernehmen
         *  - Keine Konfigurationsdatei vorhanden: Option 'Nur Konfigurationsdatei speichern' deaktivieren
         *  - Konfigurationsdatei vorhanden: Option 'Nur Konfigurationsdatei speichern' auswählen
         *  - Installationbildschirm zu sehen? Serverneustart! Seite neu laden
         */
        socket.on('dataLoaded', function(data) {
            // Installation auf dem Server neu gestartet
            // jedoch bereits eine Installation durchgeführt
            // => Seite neu laden
            if ($('#installpage:visible').length > 0) {
                notifyClose();
                location.reload();
            }

            if (data !== null) { 
                // Daten aus der Konfigurationsdatei übertragen
                $inputHost.val(data.host); 
                $inputPort.val(data.port);
                $inputDb.val(data.dbname);
                $inputUser.val(data.user);
                $inputPw.val(data.pw);
                $inputAdmin.val(data.admin);
                $inputAdminForename.val(data.adminForename);
                $inputAdminSurname.val(data.adminSurname);
                $onlyConfig.attr('selected', true);
                $inputSubmit.html('Speichern');

                // Information das die Konfigurationsdatei geladen wurde
                notifySuccess("Konfigurationdatei wurde gefunden und automatisch eingetragen.");
                $labelOverwrite.html('Datenbank übernehmen:');

                // Überprüfen der Daten auf Gültigkeit
                dbCheck(true);
                allAdminCheck();
            } else {
            	$onlyConfig.attr('disabled', true);
            }
        });

        /*
         * Zugangsdaten prüfen (MySQL Datenbank)
         * Bei Veränderungen der Datenbank-Felder diese erneut überprüfen
         */
        $("input.database").change(function(e) {
            dbCheck();
        });

        /*
         * Alle Felder auf Null zurücksetzen
         */
        $inputReset.click(function(e) {
            e.preventDefault();

            $('input').val('');
            $('select').val('');

            // Zurücksetzen der Fehler auf den Feldern
            $(".database.form-group").removeClass("has-success has-warning has-error");
            $(".db-name.form-group").removeClass("has-success has-warning has-error");
            $(".admin.form-group").removeClass("has-success has-warning has-error");
        });

        /*
         * Installation oder Speichern der Konfigurationsdatei
         */
        $inputSpecial.change(function() {
            if ($(this).val() == 'onlyconfig') {
                $labelOverwrite.html('Datenbank übernehmen:');
                $inputSubmit.html('Speichern');
            } else {
                $labelOverwrite.html('Datenbank löschen:');
                $inputSubmit.html('Installation starten');
            }
        });

        /*
         * Auswahl ob die eingegebene Datenbank gelöscht werden darf (optisch)
         */
        $inputOverwrite.change(function() {
            infoOverwrite();
        });

        /*
         * Installation starten 
         * => Daten an den Server weiterleiten
         * => Zu Installationsstatus wechseln
         */
        $inputSubmit.click(function(event) {
        	event.preventDefault();

            if ( ($('.has-success').length >= 10) && ($('.has-success').length <= 11) && ($('.has-warning').length === 0) && ($('.has-error').length === 0) ) {
                // Daten zusammenstellen
                var lgData = new loginData(       
                    $inputHost.val(), 
                    $inputPort.val(),
                    $inputUser.val(),
                    $inputPw.val(),
                    $inputDb.val(),
                    $inputAdmin.val(),
                    $inputAdminPw.val(),
                    $inputAdminForename.val(),
                    $inputAdminSurname.val(),
                    $inputSpecial.val()
                );

                // SOCKET EMIT: Installation an den Server weitergeben
                socket.emit('install', lgData, function() {});
                // Fehlermeldungen ausblenden
                notifyClose();
                // Deaktivieren des Buttons 'Installation starten'
                $inputSubmit.prop('disabled', true);
                // Wechsel in die Installation
                changePage();

            } else {
                dbCheck();
                allAdminCheck();
                notifyError("Bitte die rot markierten Felder korrigieren.");
            }
        });

        /* 
         * SOCKET ON: onlyConfigFile
         * Konfigurationsdatei wurde gespeichert
         */
        socket.on('onlyConfigFile', function(applicationopen) {
            // Erfolgsmeldung anpassen
            successMsg = 'Konfiguration gespeichert';
            // Erfolgreich gespeichert
            infinite = 'success';
            // Ladebalken auf 100% setzen
            $(".statusbar").css("width", "100%");
            // Anwendung nicht mehr startbar 
            if (applicationopen === false) {
                $('.btn-completed').remove();
            }
        });

        /*
         * SOCKET ON: installFailed
         * Installation gescheitert, optische Darstellung
         */
        socket.on('installFailed', function(result) {
            errorMsg = result;
            infinite = 'failed';
        });

        /*
         * SOCKET ON: installStatus
         * Aktualisieren des aktuellen Prozentzahl (Fortschritt der Installation)
         */
        socket.on('installStatus', function(result) {
            $(".statusbar").css('width', result + "%");
            $(".statuspercent").html(result + "%");
        });

        /*
         * SOCKET ON: installState
         * Aktuellen Installationsschritt anzeigen (Datenstruktur, Testdaten usw.)
         */
        socket.on('installState', function(result) {
            $(".status").html(result);
        });

        /*
         * SOCKET ON: installFinish
         * Installation erfolgreich abgeschlossen
         */
        socket.on('installFinish', function(applicationopen) {
        	infinite = 'success';
            $(".status").addClass('success').html('Installation abgeschlossen');
            // Anwendung nicht mehr startbar
            if (applicationopen === false) {
                $('.btn-completed').remove();
            }
        });

        
        $(".btn-completed").click(function(e) {
            e.preventDefault();
            socket.emit('startApp', function(result) {
                window.location.href = 'http://' + result.ip + ':' + result.port + '/';
            });    
        });


        /* 
         * Funktion um die Zugangsdaten MySQL zu überprüfen
         */
        function dbCheck(noNotify) {
            var noNotify = noNotify || false;

            // Alle Felder ausgefüllt
			if ($inputHost.val() != '' && 
				$inputPort.val() != '' &&
				$inputUser.val() != '' &&
                $inputPw.val() != '' &&
				$inputDb.val() != '') {
					
				var dbData = new databaseData(
					$inputHost.val().trim(),
		    		$inputPort.val().trim(),
		    		$inputUser.val().trim(),
		    		$inputPw.val(),
		    		$inputDb.val().trim()
		    	);

				socket.emit('dbcheck', dbData, function(result) {
                    // Zurücksetzen der Fehler auf den Feldern Datenbank
                    $(".database.form-group").removeClass("has-success has-warning has-error");
                    $(".db-name.form-group").removeClass("has-success has-warning has-error");

                    // Ergebnisse anzeigen
					if (result === 'empty-database') {
					 	// Zugangsdaten OK und DB existiert und ist leer
					 	$(".database.form-group").addClass("has-success");
                        databaseOK = true;

    				} else if (result === 'used-database') {
					 	// Zugangsdaten OK, aber Datenbank enthält Daten
						$(".database.form-group").addClass("has-success");
						infoOverwrite(noNotify);

                    } else if (result === 'new-database') {
                        // Zugangsdaten OK, aber Datenbank muss angelegt werden
                        $(".database.form-group").addClass("has-success");
                        databaseOK = true;
						
					} else if (result === 'no-connection') {
						// Zugangsdaten fehlerhaft
						$(".database.form-group").addClass("has-error");
                        notifyError("Verbindung zur MySQL Datenbank konnte nicht hergestellt werden.<br>Bitte die Zugangsdaten überprüfen und sicherstellen das der MySQL Server erreichbar ist.");
                        $databaseOK = false;
					} else {
                        // Verbindung gescheitert
                        $(".database.form-group").addClass("has-error");
                        notifyError("Verbindung zur MySQL Datenbank konnte nicht hergestellt werden.");
                        $databaseOK = false;
                    }
				});

			} else {
				// Freie Felder mit Fehler versehen
				allDbCheck();
                // Entfernen dbOverwrite und zurücksetzen auf 'Nein'
                $(".db-overwrite.form-group").removeClass("has-success has-warning has-error");
                $('.noOverwrite').attr('selected', true);
                $databaseOK = false;
			}

		}

        /*
         * Funktionen um die leere Eingabefelder mit Fehlermeldung darzustellen
         */
		function fieldCheck(field) {
			field.parent().parent().removeClass("has-success has-error");

			if (field.val() == '') {
				field.parent().parent().addClass("has-error");
                return false;
			} else {
				field.parent().parent().addClass("has-success");
                return true;
			}  
		}

        /*
         * Überprüfen der Admin-Eingabefelder (bei Änderungen)
         */
        $("input.admin").change(function() {
            fieldCheck($(this));
        });

        /*
         * Überprüfen aller Admin-Eingabefelder
         */
		function allAdminCheck() {
            var error = 0;

			if (fieldCheck($inputAdmin) == false) { error += 1; }
	        if (fieldCheck($inputAdminPw) === false) { error += 1; }
            if (fieldCheck($inputAdminPwAgain) === false) { error += 1; }
	        if (fieldCheck($inputAdminForename) === false) { error += 1; }
	        if (fieldCheck($inputAdminSurname) === false) { error += 1; }
            if (adminPasswordLength() === false) { error += 1; }

            return (error > 0 ? false : true);
		}

        /*
         * Überprüfen aller Datenbank-Eingabefelder
         */
		function allDbCheck() {
	        fieldCheck($inputHost);
		    fieldCheck($inputPort);
		    fieldCheck($inputUser);
            fieldCheck($inputPw);
		    fieldCheck($inputDb);
		}

        /*
         * Überprüfen der Passworteingabe
         */ 
        $('#config-inner').on('change', '#inputAdminPw, #inputAdminPwAgain', function() {
            if (adminPasswordLength() === false) {
                if ($inputAdminPw.val().length < 8) {
                    notifyError("Passwort muss min. 8 Zeichen lang sein");
                } else if ($inputAdminPw.val().length > 0 && $inputAdminPwAgain.val().length > 0) {
                    notifyError("Das Passwort für den Administrators stimmt nicht überein");
                }
                return false;
            } else {
                return true;
            }
        });

        /*
         * Funktion um die Passwortlänge zu überprüfen (min. 8 Zeichen)
         */ 
        function adminPasswordLength() {
            $inputAdminPw.parent().parent().removeClass("has-success has-error");
            $inputAdminPwAgain.parent().parent().removeClass("has-success has-error");

            if ($inputAdminPw.val().length < 8) {
                $inputAdminPw.parent().parent().addClass("has-error");
                $inputAdminPwAgain.parent().parent().addClass("has-error");
                return false;  

            } else if ($inputAdminPw.val() !== $inputAdminPwAgain.val()) {
                $inputAdminPw.parent().parent().addClass("has-error");
                $inputAdminPwAgain.parent().parent().addClass("has-error");
                return false;

            } else {
                $inputAdminPw.parent().parent().addClass("has-success");
                $inputAdminPwAgain.parent().parent().addClass("has-success");
                return true;
            }
        }

});

// Datenübergabe an den Server (Alle benötigten Dateien)
function loginData(host, port, user, pw, dbname, admin, adminpw, adminForename, adminSurname, special) {
    this.host = host;
    this.port = port;
    this.user = user;
    this.pw = pw;
    this.dbname = dbname;
    this.admin = admin;
    this.adminpw = adminpw;
    this.adminForename = adminForename;
    this.adminSurname = adminSurname;
    this.special = special;
}

// Datenübergabe an den Server (Datenbank Zugangsdaten)
function databaseData(host, port, user, pw, dbname) {
    this.host = host;
    this.port = port;
    this.user = user;
    this.pw = pw;
    this.dbname = dbname;
}

// Information Datenbank überschreiben
function infoOverwrite(noNotify) {
    var noNotify = noNotify || false;

    if ($inputOverwrite.val() == 'no') {
        $(".db-name.form-group").removeClass("has-success").addClass("has-warning");
        if (!noNotify) { 
            if ($labelOverwrite.html() === 'Datenbank übernehmen:') {
                notifyWarning("Datenbank existiert bereits und enthält Tabellen.<br>Bitte Datenbank übernehmen mit 'Ja' bestätigen oder einen anderen Datenbanknamen eintragen.");
            } else {
                notifyWarning("Datenbank existiert bereits und enthält Tabellen.<br>Bitte Datenbank löschen mit 'Ja' bestätigen oder einen anderen Datenbanknamen eintragen.");
            }
        }
        $databaseOK = false;
    } else {
        $(".db-name.form-group").removeClass("has-warning").addClass("has-success");
        databaseOK = true;
    }
}

/**
 * notifyDefaults Standardeinstellungen für alle Hinweisfenster
 *
 * @functionfor notifyWarning, notifyError, notifySuccess
 */
$.notifyDefaults({
    placement: {
      from: "top",
      align: "center"
    },
    allow_dismiss: true,
    newest_on_top: true,
    animate:{
      enter: "animated bounceIn",
      exit: "animated bounceOut"
    },
    type: 'danger',
    timer: 3000
});

/**
 * notifyWarning Hinweisfenster für Warnungen
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param  {string} msg Meldung die angezeigt werden soll
 */
function notifyWarning(msg) {
    notifyClose();

    $.notify({
        icon: 'glyphicon glyphicon-warning-sign',
        title: "<strong>Warnung</strong><br>",
        message: msg
    },{
        type: 'warning'
    }); 
}

/**
 * notifyError Hinweisfenster für Fehlermeldungen
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param  {string} msg Meldung die angezeigt werden soll
 */
function notifyError(msg) {
    notifyClose();

    $.notify({
        icon: 'glyphicon glyphicon-warning-sign',
        title: "<strong>Fehler</strong><br>",
        message: msg
    });
}

/**
 * notifySuccess Hinweisfenster wenn etwas erfolgreich war
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 * @param  {string} msg Meldung die angezeigt werden soll
 */
function notifySuccess(msg) {
    notifyClose();

    $.notify({
        icon: 'glyphicon glyphicon-saved',
        title: "<strong>Information</strong><br>",
        message: msg
    },{
        type: 'success'
    });
}


/**
 * notifyClose Sicherstellen das nur ein Notify Fenster angezeigt wird
 *
 * @author Rafal Welk <rafal@project.hatua.de>
 */
function notifyClose() {
  $('*[data-notify="container"').hide();
}