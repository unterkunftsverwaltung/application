SET FOREIGN_KEY_CHECKS=0;

INSERT INTO `roles` (name, permissions) VALUES ('Administrator', '11111111111111111111111111');
INSERT INTO `roles` (name, permissions) VALUES ('Zuschauer', '00000000000000000000000000');

INSERT INTO `permissions` (`short`, `description`, `reach`, `version`) VALUES ('admin', 'Rollen hinzufügen, bearbeiten und entfernen', 'global', '0.4');
INSERT INTO `permissions` (`short`, `description`, `reach`, `version`) VALUES ('users', 'Benutzer hinzufügen, bearbeiten und entfernen', 'global', '0.4');
INSERT INTO `permissions` (`short`, `description`, `reach`, `version`) VALUES ('units', 'Einheiten hinzufügen, bearbeiten und entfernen', 'global', '0.2');
INSERT INTO `permissions` (`short`, `description`, `reach`, `version`) VALUES ('ranks', 'Dienstgrad hinzufügen, bearbeiten und entfernen', 'global', '0.3');
INSERT INTO `permissions` (`short`, `description`, `reach`, `version`) VALUES ('utilisation', 'Nutzungsart hinzufügen, bearbeiten und entfernen', 'global', '');
INSERT INTO `permissions` (`short`, `description`, `reach`, `version`) VALUES ('reset', 'Passwort zurücksetzen', 'global', '0.4');
INSERT INTO `permissions` (`short`, `description`, `reach`, `version`) VALUES ('buildingAdd', 'Gebäude hinzufügen', 'global', '0.5');
INSERT INTO `permissions` (`short`, `description`, `reach`, `version`) VALUES ('buildingDel', 'Gebäude entfernen', 'global', '0.5');
INSERT INTO `permissions` (`short`, `description`, `reach`, `version`) VALUES ('buildingEdit', 'Gebäude bearbeiten', 'global', '0.5');
INSERT INTO `permissions` (`short`, `description`, `reach`, `version`) VALUES ('roomAdd', 'Räume hinzufügen', 'global', '0.5');
INSERT INTO `permissions` (`short`, `description`, `reach`, `version`) VALUES ('roomDel', 'Räume entfernen', 'global', '0.5');
INSERT INTO `permissions` (`short`, `description`, `reach`, `version`) VALUES ('roomEdit', 'Räume bearbeiten', 'global', '0.5');
INSERT INTO `permissions` (`short`, `description`, `reach`, `version`) VALUES ('roomBook', 'Räume belegen', 'unit', '0.6');
INSERT INTO `permissions` (`short`, `description`, `reach`, `version`) VALUES ('roomClear', 'Räume freigeben', 'unit', '0.6');
INSERT INTO `permissions` (`short`, `description`, `reach`, `version`) VALUES ('roomChange', 'Räume tauschen', 'unit', '');
INSERT INTO `permissions` (`short`, `description`, `reach`, `version`) VALUES ('roomInventory', 'Rauminventar anpassen', 'unit', '');
INSERT INTO `permissions` (`short`, `description`, `reach`, `version`) VALUES ('roomKeys', 'Schlüssel zuordnen', 'unit', '');
INSERT INTO `permissions` (`short`, `description`, `reach`, `version`) VALUES ('roomUse', 'Nutzungsart zuweisen', 'unit', '');
INSERT INTO `permissions` (`short`, `description`, `reach`, `version`) VALUES ('residentAdd', 'Bewohner hinzufügen', 'unit', '0.6');
INSERT INTO `permissions` (`short`, `description`, `reach`, `version`) VALUES ('residentDel', 'Bewohner entfernen', 'unit', '0.6');
INSERT INTO `permissions` (`short`, `description`, `reach`, `version`) VALUES ('residentEdit', 'Bewohner bearbeiten', 'unit', '0.6');
INSERT INTO `permissions` (`short`, `description`, `reach`, `version`) VALUES ('printDoorplate', 'Türschilder drucken', 'unit', '');
INSERT INTO `permissions` (`short`, `description`, `reach`, `version`) VALUES ('printInventory', 'Inventarliste drucken', 'unit', '');
INSERT INTO `permissions` (`short`, `description`, `reach`, `version`) VALUES ('printBooking', 'Belegungsplan drucken', 'unit', '');
INSERT INTO `permissions` (`short`, `description`, `reach`, `version`) VALUES ('inventory', 'Inventar hinzufügen, bearbeiten und entfernen', 'global', '');
INSERT INTO `permissions` (`short`, `description`, `reach`, `version`) VALUES ('keys', 'Schlüssel hinzufügen und entfernen', 'unit', '');

INSERT INTO `ranks` (`rankID`, `label`, `short`) VALUES (1, 'Administrator', 'ADMIN');

INSERT INTO `user_units` (`userID`, `unitID`, `roleID`) VALUES (1, NULL, 1);

INSERT INTO `utilisation` (`label`, `usable`) VALUES ('Unterkunft', 0);