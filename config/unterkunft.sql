SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";

-- Gebäude
	
DROP TABLE IF EXISTS `buildings`;
		
CREATE TABLE `buildings` (
  `buildingID` INTEGER NOT NULL AUTO_INCREMENT,
  `number` VARCHAR(4) NOT NULL COMMENT 'Gebäudenummer',
  `alias` VARCHAR(30) NULL DEFAULT NULL COMMENT 'Gebäudebezeichnung',
  `notes` VARCHAR(300) NULL DEFAULT NULL COMMENT 'Hinweise / Anmerkungen zu diesem Gebäude',
  PRIMARY KEY (`buildingID`)
) COMMENT 'Gebäude';

-- Unterkünfte / Büros

DROP TABLE IF EXISTS `room`;
		
CREATE TABLE `room` (
  `roomID` INTEGER NOT NULL AUTO_INCREMENT,
  `number` VARCHAR(5) NOT NULL COMMENT 'Raumnummer',
  `maxoccupancy` TINYINT(3) UNSIGNED NULL DEFAULT NULL COMMENT 'Maximale Belegungsanzahl',
  `notes` VARCHAR(300) NULL DEFAULT NULL COMMENT 'Hinweise / Anmerkungen zu diesem Raum',
  `buildingID` INTEGER NOT NULL COMMENT 'liegt im Gebäude',
  `unitID` INTEGER NOT NULL COMMENT 'gehört zur Einheit',
  `useID` INTEGER NULL COMMENT 'besonderer Status',
  PRIMARY KEY (`roomID`)
) COMMENT 'Unterkünfte / Büros';

-- Bewohner, die Räumen zugewiesen werden können

DROP TABLE IF EXISTS `residents`;
		
CREATE TABLE `residents` (
  `residentID` INTEGER NOT NULL AUTO_INCREMENT,
  `forename` VARCHAR(30) NOT NULL COMMENT 'Vorname',
  `surname` VARCHAR(30) NOT NULL COMMENT 'Nachname',
  `rankID` INTEGER NOT NULL COMMENT 'Dienstgrad',
  `sex` ENUM('male','female') NOT NULL COMMENT 'Geschlecht',
  `notes` VARCHAR(100) NULL COMMENT 'Bemerkungen',
  `unitID` INTEGER NOT NULL COMMENT 'gehört zur Einheit',
  PRIMARY KEY (`residentID`)
) COMMENT 'Bewohner die den Räumen zugewiesen werden können';

-- Dienstgrade

DROP TABLE IF EXISTS `ranks`;
		
CREATE TABLE `ranks` (
  `rankID` INTEGER NOT NULL AUTO_INCREMENT,
  `label` VARCHAR(50) NOT NULL COMMENT 'Dienstgradbezeichnung',
  `short` VARCHAR(8) NOT NULL COMMENT 'Kurzschreibweise',
  PRIMARY KEY (`rankID`)
) COMMENT 'Dienstgrade';

-- Verknüpfung der Bewohner mit ihren Räumen

DROP TABLE IF EXISTS `room_resident`;
		
CREATE TABLE `room_resident` (
  `rrID` INTEGER NOT NULL AUTO_INCREMENT,
  `roomID` INTEGER NOT NULL COMMENT 'Raum im Gebäude',
  `residentID` INTEGER NOT NULL COMMENT 'Bewohner',
  `until` DATE NULL DEFAULT NULL COMMENT 'voraussichtliche Belegungsdauer',
  PRIMARY KEY (`rrID`)
) COMMENT 'Verknüpfung der Bewohner mit ihren bezogenen Räumen';

-- Einheiten und Teileinheiten

DROP TABLE IF EXISTS `units`;
		
CREATE TABLE `units` (
  `unitID` INTEGER NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(55) NOT NULL COMMENT 'Zugehörigkeit',
  `short` VARCHAR(12) NOT NULL COMMENT 'Kurzschreibweise',
  `member` INTEGER NULL DEFAULT NULL COMMENT 'Gehört zu',
  PRIMARY KEY (`unitID`)
) COMMENT 'Einheiten und Teileinheiten';

-- Benutzer der Anwenundung

DROP TABLE IF EXISTS `user`;
		
CREATE TABLE `user` (
  `userID` INTEGER NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(14) NOT NULL COMMENT 'Benutzername',
  `password` VARCHAR(32) NOT NULL COMMENT 'Passwort mit MD5 verschlüsselt',
  `forename` VARCHAR(30) NOT NULL COMMENT 'Vorname',
  `surname` VARCHAR(30) NOT NULL COMMENT 'Nachname',
  `rankID` INTEGER NOT NULL COMMENT 'Dienstgrad',
  PRIMARY KEY (`userID`)
) COMMENT 'Benutzer';

-- Inventar

DROP TABLE IF EXISTS `inventory`;
		
CREATE TABLE `inventory` (
  `inventoryID` INTEGER NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(255) NOT NULL COMMENT 'Bezeichnung des Inventars',
  PRIMARY KEY (`inventoryID`)
) COMMENT 'Inventar';

-- Inventar auf dem entsprechenden Raum

DROP TABLE IF EXISTS `room_inventory`;
		
CREATE TABLE `room_inventory` (
  `roomID` INTEGER NOT NULL COMMENT 'Raum',
  `inventoryID` INTEGER NOT NULL COMMENT 'Inventar',
  `quantity` TINYINT NOT NULL DEFAULT 1 COMMENT 'Anzahl',
  PRIMARY KEY (`roomID`, `inventoryID`)
) COMMENT 'Inventur auf der entsprechenden Stube';

-- Schlüsselübersicht

DROP TABLE IF EXISTS `keys`;
		
CREATE TABLE `keys` (
  `keysID` INTEGER NOT NULL AUTO_INCREMENT,
  `roomID` INTEGER NOT NULL COMMENT 'Unterkunft',
  `residentID` INTEGER NULL DEFAULT NULL COMMENT 'Ausgegeben an Bewohner',
  `location` VARCHAR(255) NULL DEFAULT NULL COMMENT 'Nicht ausgegeben: Standort des Schlüssels (z.B. Geschäftszimmer)',
  PRIMARY KEY (`keysID`)
) COMMENT 'Schlüsselübersicht';

-- Nutzungsart wie zB Heimschläfer, Projektraum, Büro

DROP TABLE IF EXISTS `utilisation`;
		
CREATE TABLE `utilisation` (
  `useID` INTEGER NOT NULL AUTO_INCREMENT,
  `label` VARCHAR(255) NOT NULL COMMENT 'Nutzungsart wie z.B. Heimschläfer, Projektraum, Büro',
  `usable` BINARY NULL DEFAULT NULL COMMENT 'Raum blocken (0: nutzbar, 1: nicht nutzbar',
  PRIMARY KEY (`useID`)
) COMMENT 'Nutzungsart';

-- Bedingung(en) für die Inventurauswahl

DROP TABLE IF EXISTS `utilisation_inventory`;
		
CREATE TABLE `utilisation_inventory` (
  `useinventoryID` INTEGER NOT NULL AUTO_INCREMENT,
  `useID` INTEGER NOT NULL COMMENT 'Nutzungsart',
  `inventoryID` INTEGER NOT NULL COMMENT 'Inventar',
  `quantity` TINYINT NOT NULL DEFAULT 0,
  PRIMARY KEY (`useinventoryID`)
) COMMENT 'Bedingung(en) für die Inventurauswahl bei der Nutzungsart';

-- Rechtematrix / -konzept

DROP TABLE IF EXISTS `permissions`;
		
CREATE TABLE `permissions` (
  `permissionID` INTEGER NOT NULL AUTO_INCREMENT,
  `short` VARCHAR(16) NOT NULL COMMENT 'Kurzname',
  `description` VARCHAR(255) NOT NULL COMMENT 'Beschreibung',
  `reach` ENUM('global', 'unit') NOT NULL COMMENT 'Globale oder Einheitenspezifische Rechte',
  `version` VARCHAR(5) NULL COMMENT 'Aktivierte Berechtigungen je nach Version',
  PRIMARY KEY (`permissionID`)
) COMMENT 'Rechtematrix';

-- Rechtezuweisung an die Benutzer

DROP TABLE IF EXISTS `user_units`;
		
CREATE TABLE `user_units` (
  `uuID` INTEGER NOT NULL AUTO_INCREMENT,
  `userID` INTEGER NOT NULL COMMENT 'Anwender',
  `unitID` INTEGER NULL COMMENT 'Zugehörigkeit',
  `roleID` INTEGER NOT NULL COMMENT 'Rolle',
  PRIMARY KEY (`uuID`)
) COMMENT 'Rollen je Benutzer je Einheit';

-- Rollen

DROP TABLE IF EXISTS `roles`;
		
CREATE TABLE `roles` (
  `roleID` INTEGER NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(20) NOT NULL COMMENT 'Rollenname',
  `permissions` VARCHAR(1024) NULL DEFAULT NULL COMMENT 'Rechte',
  PRIMARY KEY (`roleID`)
) COMMENT 'Rollen';

-- Foreign Keys

ALTER TABLE `units` ADD FOREIGN KEY (member) REFERENCES `units` (`unitID`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `room` ADD FOREIGN KEY (unitID) REFERENCES `units` (`unitID`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `residents` ADD FOREIGN KEY (unitID) REFERENCES `units` (`unitID`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `user_units` ADD FOREIGN KEY (unitID) REFERENCES `units` (`unitID`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `residents` ADD FOREIGN KEY (rankID) REFERENCES `ranks` (`rankID`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `user` ADD FOREIGN KEY (rankID) REFERENCES `ranks` (`rankID`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `user_units` ADD FOREIGN KEY (userID) REFERENCES `user` (`userID`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `user_units` ADD FOREIGN KEY (roleID) REFERENCES `roles` (`roleID`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `room` ADD FOREIGN KEY (buildingID) REFERENCES `buildings` (`buildingID`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `room` ADD FOREIGN KEY (useID) REFERENCES `utilisation` (`useID`) ON DELETE RESTRICT ON UPDATE CASCADE;
ALTER TABLE `room_resident` ADD FOREIGN KEY (roomID) REFERENCES `room` (`roomID`) ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE `room_resident` ADD FOREIGN KEY (residentID) REFERENCES `residents` (`residentID`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `room_inventory` ADD FOREIGN KEY (inventoryID) REFERENCES `inventory` (`inventoryID`);
ALTER TABLE `keys` ADD FOREIGN KEY (residentID) REFERENCES `residents` (`residentID`);
ALTER TABLE `utilisation_inventory` ADD FOREIGN KEY (useID) REFERENCES `utilisation` (`useID`);
ALTER TABLE `utilisation_inventory` ADD FOREIGN KEY (inventoryID) REFERENCES `inventory` (`inventoryID`);

-- Um Testdaten zu löschen müssen diese auf CASCADE gestellt sein

ALTER TABLE `keys` ADD FOREIGN KEY (roomID) REFERENCES `room` (`roomID`) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE `room_inventory` ADD FOREIGN KEY (roomID) REFERENCES `room` (`roomID`) ON DELETE CASCADE ON UPDATE CASCADE;