# Verbesserungen

## Feedback Review Sprint 5

- Sichtbarkeit der Legende erhöhen
- Beim Erstellen von Räumen Gebäude anzeigen
- Mehrere Bewohner einem Raum zuweisen 
- Erläuterung für die Darstellung männlich / weiblich
- Gebäude / Unterkünfte per Dropdown ein- / ausblenden
- Rollen Hinweis welche Benutzer diese Rolle besitzt
- Erläuterung der Suche


## Feedback Spieß

- Zweite Darstellung für Unterkünfte (ähnlich Gebäudeplan)


## Feedback IT-Projekt (OTL Eckl)

- Exportfunktion der Daten nach Excel


## Bugfix

- Einstellungen > Gebäude: Bemerkungen können verschoben dargestellt werden im IE